
import { Injectable } from '@angular/core';

export const AppBase = Object.freeze({
  // HttpRequest: 'http://45.117.162.156:9180/api',
  Host:'http://210.211.99.229:8800',
  HttpRequest: 'http://210.211.99.229:8800/api',
  HttpRequestVideo: 'http://210.211.99.228:8800/api',

  ApiKey: 'cda11v2OkqSI1rhQm37PBXKnpisMtlaDzoc4w0U6uNATgZRbJG',

  //Setting Page
  Login: '/login',
  Register: '/register',
  GetPrivacyPolicy: '/get_privacy_policy',
  UpdateProfile: '/update_profile',
  ResetPassword: '/forgot_password',

  //Home Page
  GetSearchResult: '/get_search_results',
  GetRecentPost: '/get_recent_posts',

  //Video Page
  GetVideoPost: '/get_video_posts',
  GetVideoSearch: '/get_search_videos',

  //Category Page
  GetCategoryIndex: '/get_category_index',
  GetCategoryPost: '/get_category_posts',

  //Market Page
  GetMarketItems: '/get_market_items',
  GetMarketItemsUser: '/get_market_items_user',
  GetMarketItemsType: '/get_market_items_type',
  GetMarketType: '/get_market_type',
  AddMarketItems: '/add_market_item',
  GetProvince: '/get_province',
  GetSearchMarkets: '/get_search_markets',
  EditMarketItem: '/edit_market_item',
  RemoveMarketItem: '/remove_market_item',

  //Post Detail
  GetPostDetail: '/get_post_detail',

  // Comment
  GetComment: '/get_comments',
  AddComement: '/add_comment',
  EditComment: '/edit_comment',
  DeleteComment: '/delete_comment',
  GetCommentV2: '/get_comments_v2',
  AddComementV2: '/add_comment_v2',

  // Answer
  GetAnswer: '/get_answers',
  GetAnswerByUser: '/get_answers_by_user',
  AddAnswer: '/add_answer',
  EditAnswer: '/edit_answer',
  DeleteAnswer: '/delete_answer',
  GetCategoryAnswer: '/get_category_answers',
  GetSearchAnswer: '/get_search_answers',

  // File
  FileUpload: '//file_upload',

  // Video
  GetUrlUpload: '/get_url_upload',
  GetPathFile: '/get_path_file',
  GetCategoryForVideo: '/get_categories_for_video',
  GetCategoryVideo: '/get_category_video',
  
  GetVideoDetailV2: '/get_video_detail_v2',
  GetVideoUser: '/get_videos_user',
  GetVideoV2: '/get_videos_v2',
  GetVideoV3: '/get_videos_v3',
  AddVideoV2: '/add_video_v2',
  EditVideoV2: '/edit_video_v2',
  RemoveVideoV2: '/remove_video_v2',
  
  GetNotification: '/get_notifications',
  UpdateNotification: '/update_notification'

});


@Injectable()
export class Global {
  public isLogin: boolean = false;
  public userInfo: {
    user_id: number,
    user_name: string,
    full_name: string,
    email: string,
    phone: string,
    address: string,
    gender: string,
    avatar: string,
    birth_date: string,
  }

  public fireBase_token: string = "";
  public dataSender: any = {};
  public current_cat_id: number = -1;
  public avalink: string = "http://inongdan.vn/"
  public avatar: string = "https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1";
}

export enum OfflineMode {
  NORMAL = 0,
  NO_DATA = 1,
  NO_INTERNET = 2
}
