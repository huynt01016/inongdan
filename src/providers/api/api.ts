import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, } from '@angular/http';
import { AppBase } from '../../providers/appbase/appbase';
import { Global } from '../../providers/appbase/appbase';
import { AlertController } from '@ionic/angular';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

@Injectable()
export class ApiProvider {
    constructor(
        public global: Global,
        public http: Http) { }

    //Settting Page
    login(username: string, password: string) {
        console.log(username, password);
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let postData = "";
        postData += "username=" + username;
        postData += "&password=" + password;
        if (this.global.fireBase_token != "")
            postData += "&token=" + this.global.fireBase_token;
        console.log(postData);

        return this.http.post(AppBase.HttpRequest + AppBase.Login
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    register(email: string, phone: string, display_name: string, password: string, address: string, gender: string, fbid: string = "") {
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            email: email,
            phone: phone,
            display_name: display_name,
            password: password,
            address: address,
            gender: gender,
            fbid: fbid,
        };

        if (Data.fbid == "")
            delete Data.fbid;

        let postData = this.jsonToURLEncoded(Data);
        // console.log(JSON.stringify(Data));
        return this.http.post(AppBase.HttpRequest + AppBase.Register
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getPrivacyPolicy() {
        return this.http.get(AppBase.HttpRequest + AppBase.GetPrivacyPolicy, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    updateProfile(username: string, display_name: string, email: string, address: string, birth_date: string, avatar: string, avatar_source: string) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            username: username,
            display_name: display_name,
            email: email,
            address: address,
            birth_date: birth_date,
            avatar: avatar,
            avatar_source: avatar_source,
        };

        if (avatar_source == null) {
            delete Data.avatar_source;
            delete Data.avatar;
        }


        let postData = this.jsonToURLEncoded(Data);
        // console.log(JSON.stringify(Data));
        return this.http.post(AppBase.HttpRequest + AppBase.UpdateProfile
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    resetPassword(username: string) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            username: username,
        };

        let postData = this.jsonToURLEncoded(Data);
        // console.log(JSON.stringify(Data));
        return this.http.post(AppBase.HttpRequest + AppBase.ResetPassword
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    //Home Page
    getSearchResult(search: string, page: number = 1, count: number = 1): Promise<any> {
        var url = AppBase.HttpRequest + AppBase.GetSearchResult
        + "?search=" + search
        + "&page=" + page
        + "&count=" + count;
        console.log("getSearchResult -> url: " + url);
        return this.http.get(url, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getRecentPost(page: number = 1, count: number = 1): Promise<any> {
        var url = AppBase.HttpRequest + AppBase.GetRecentPost
        + "?page=" + page
        + "&count=" + count;
        console.log("getRecentPost: " + url);
        return this.http.get(url, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    //Video Page
    getVideoPost(page: number = 1, count: number = 1): Promise<any> {
        var url = AppBase.HttpRequest + AppBase.GetVideoPost
        + "?page=" + page
        + "&count=" + count;
        
        console.log(`getVideoPost: ${url}`);
        return this.http.get(url, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getSearchVideo(search: string, page: number = 1, count: number = 1): Promise<any> {
        var url = AppBase.HttpRequest + AppBase.GetVideoSearch
        + "?search=" + search
        + "&page=" + page
        + "&count=" + count;
        console.log(`search video: ${url}`);
        return this.http.get(url, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    //Category Page
    getCategoryIndex(): Promise<any> {
        var url = AppBase.HttpRequest + AppBase.GetCategoryIndex
            + "?api_key=" + AppBase.ApiKey;
        console.log(url);
        return this.http.get(url, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getCategoryPost(id: string, page: number = 1, count: number = 1): Promise<any> {
        let url = AppBase.HttpRequest + AppBase.GetCategoryPost
        + "?id=" + id
        + "&page=" + page
        + "&count=" + count;
        // console.log("getCategoryPost: " + url);
        return this.http.get(url, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    // Market Page
    getMarketItems(page: number = 1, count: number = 10): Promise<any> {

        var url = AppBase.HttpRequest + AppBase.GetMarketItems
        + "?page=" + page
        + "&count=" + count;
        console.log(`getMarketItems: ${url}`);
        return this.http.get(url, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getMarketItemsUser(phone: string, page: number = 1, count: number = 1): Promise<any> {
        console.log(phone, page, count);
        return this.http.get(AppBase.HttpRequest + AppBase.GetMarketItemsUser
            + "?phone=" + phone
            + "&page=" + page
            + "&count=" + count, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getMarketItemsType(market_type_id: number, page: number = 1, count: number = 1): Promise<any> {
        return this.http.get(AppBase.HttpRequest + AppBase.GetMarketItemsType
            + "?market_type_id=" + market_type_id
            + "&page=" + page
            + "&count=" + count, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getMarketType(): Promise<any> {

        return this.http.get(AppBase.HttpRequest + AppBase.GetMarketType, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    addMarketItems(user_id: number, market_type_id: number, province_id: number, title: string, content: string, type: number, price: string, phone: string, contact: string, image: string, image_source: string): Promise<any> {
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            user_id: user_id,
            market_type_id: market_type_id,
            province_id: province_id,
            title: title,
            content: content,
            type: type,
            price: price,
            phone: phone,
            contact: contact,
            image: image,
            image_source: image_source,
        };
        if (Data.type == 0) {
            if (Data.image == null)
                delete Data.image;
            if (Data.image_source == null)
                delete Data.image_source;
        }

        // console.log(JSON.stringify(Data));
        let postData = this.jsonToURLEncoded(Data);
        console.log(postData);

        return this.http.post(AppBase.HttpRequest + AppBase.AddMarketItems
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    editMarketItems(market_type_id: number, province_id: number, title: string, content: string, type: number, price: string,
        phone: string, contact: string, market_id: string,
        image: string, image_source: string): Promise<any> {
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            market_id: market_id,
            market_type_id: market_type_id,
            province_id: province_id,
            title: title,
            content: content,
            type: type,
            price: price,
            phone: phone,
            contact: contact,
            image: image,
            image_source: image_source
        };

        if(image_source == null || image_source == "")
            Data.image = "";

        // console.log(JSON.stringify(Data));
        let postData = this.jsonToURLEncoded(Data);
        console.log(postData);

        return this.http.post(AppBase.HttpRequest + AppBase.EditMarketItem
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    deleteMarketItems(market_id: string): Promise<any> {
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            market_id: market_id
        };

        // console.log(JSON.stringify(Data));
        let postData = this.jsonToURLEncoded(Data);
        console.log(postData);

        return this.http.post(AppBase.HttpRequest + AppBase.RemoveMarketItem
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getProvince(): Promise<any> {

        return this.http.get(AppBase.HttpRequest + AppBase.GetProvince, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getSearchMarkets(search: string, page: number = 1, count: number = 1): Promise<any> {
        console.log(AppBase.HttpRequest + AppBase.GetSearchMarkets
            + "?search=" + search
            + "&page=" + page
            + "&count=" + count);
        return this.http.get(AppBase.HttpRequest + AppBase.GetSearchMarkets
            + "?search=" + search
            + "&page=" + page
            + "&count=" + count, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    //Post Detail
    getPostDetail(id: string): Promise<any> {
        return this.http.get(AppBase.HttpRequest + AppBase.GetPostDetail
            + "?id=" + id, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getComment(nid: string): Promise<any> {
        return this.http.get(AppBase.HttpRequest + AppBase.GetComment
            + "?nid=" + nid, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getCommentv2(nid: string): Promise<any> {
        console.log(AppBase.HttpRequest + AppBase.GetCommentV2
            + "?nid=" + nid);
        return this.http.get(AppBase.HttpRequest + AppBase.GetCommentV2
            + "?nid=" + nid, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    addComment(nid: number, uid: number, message: string): Promise<any> {
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            nid: nid,
            uid: uid,
            message: message
        };
        let postData = this.jsonToURLEncoded(Data);

        return this.http.post(AppBase.HttpRequest + AppBase.AddComement
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    addCommentV2(nid: number, uid: number, message: string): Promise<any> {
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            nid: nid,
            uid: uid,
            message: message
        };
        // console.log("addCommentV2: " + JSON.stringify(Data));
        let postData = this.jsonToURLEncoded(Data);

        return this.http.post(AppBase.HttpRequest + AppBase.AddComementV2
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    editComment(comment_id: string, message: string): Promise<any> {
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            comment_id: comment_id,
            message: message,
        };
        let postData = this.jsonToURLEncoded(Data);

        return this.http.post(AppBase.HttpRequest + AppBase.EditComment
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    deleteComment(comment_id: string): Promise<any> {
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            comment_id: comment_id,
        };
        let postData = this.jsonToURLEncoded(Data);

        return this.http.post(AppBase.HttpRequest + AppBase.DeleteComment
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    // Help
    getAnswer(page: number = 1, count: number = 1): Promise<any> {

        var url = AppBase.HttpRequest + AppBase.GetAnswer
        + "?page=" + page
        + "&count=" + count;
        console.log(`getAnswer: ${url}`);
        return this.http.get(url, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getAnswerByUser(user_name: string, page: number = 1, count: number = 1): Promise<any> {

        var url = AppBase.HttpRequest + AppBase.GetAnswerByUser
        + "?user_name=" + user_name
        + "&page=" + page
        + "&count=" + count;
        console.log(`getAnswerByUser: ${url}`);
        return this.http.get(url, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    addAnswer(title: string, question: string, creator: string,
        email: string, phone: string, province_id: string, category_id: string,
        image: string = null, image_source: string = null): Promise<any> {

        console.log(title, question, creator, email, phone, province_id, category_id);
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            title: title,
            question: question,
            category_id: category_id,
            creator: creator,
            email: email,
            phone: phone,
            province_id: province_id,
            image: image,
            image_source: image_source

        };
        let postData = this.jsonToURLEncoded(Data);

        return this.http.post(AppBase.HttpRequest + AppBase.AddAnswer
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    editAnswer(title: string, question: string, province_id: string, category_id: string, ans_id: string,
        image: string = null, image_source: string = null) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            title: title,
            question: question,
            category_id: category_id,
            province_id: province_id,
            ans_id: ans_id,
            image: image,
            image_source: image_source,
        };
        if(image_source == null || image_source == "")
            Data.image = "";
        let postData = this.jsonToURLEncoded(Data);
        // console.log(JSON.stringify(Data));

        return this.http.post(AppBase.HttpRequest + AppBase.EditAnswer
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    deleteAnswer(ans_id: string) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            ans_id: ans_id,
        };
        let postData = this.jsonToURLEncoded(Data);

        return this.http.post(AppBase.HttpRequest + AppBase.DeleteAnswer
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getCategoryAnswer() {
        return this.http.get(AppBase.HttpRequest + AppBase.GetCategoryAnswer,
            {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getSearchAnswer(search: string, page: number = 1, count: number = 1): Promise<any> {
        return this.http.get(AppBase.HttpRequest + AppBase.GetSearchAnswer
            + "?search=" + search
            + "&page=" + page
            + "&count=" + count, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    //File upload
    fileUpload(file_uri: string): Promise<any> {
        var headers = new Headers();
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            file: file_uri,
        };
        let postData = this.jsonToURLEncoded(Data);

        return this.http.post(AppBase.HttpRequest + AppBase.FileUpload
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    //Video
    getUrlUpload(fileName: string, fileSize: number, contentType: string): Promise<any> {

        var link = AppBase.HttpRequestVideo + AppBase.GetUrlUpload
            + "?objectKey=" + fileName
            + "&fileSize=" + fileSize
            + "&contentType=" + contentType;

        console.log(link);
        return this.http.get(link, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getPathFile(fileName: string): Promise<any> {
        return this.http.get(AppBase.HttpRequestVideo + AppBase.GetPathFile
            + "?objectKey=" + fileName, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getVideoV2(page: number = 1, count: number = 1): Promise<any> {
        
        var url = AppBase.HttpRequestVideo + AppBase.GetVideoV2
        + "?page=" + page
        + "&count=" + count;
        console.log(`getVideoV2: ${url}`)
        return this.http.get(url, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getVideoV3(page: number = 1, count: number = 1): Promise<any> {
        
        var url = AppBase.HttpRequestVideo + AppBase.GetVideoV3
        + "?page=" + page
        + "&count=" + count;
        console.log(`getVideoV3: ${url}`)
        return this.http.get(url, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getCategoryForVideo(): Promise<any> {
        var url = AppBase.HttpRequest + AppBase.GetCategoryForVideo
            + "?api_key=" + AppBase.ApiKey;
        console.log(url);
        return this.http.get(url, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getCategoryVideo(id: number, page: number = 1, count: number = 1): Promise<any> {
        return this.http.get(AppBase.HttpRequestVideo + AppBase.GetCategoryVideo
            + "?id=" + id
            + "&page=" + page
            + "&count=" + count, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getVideoDetailV2(id: number): Promise<any> {
        return this.http.get(AppBase.HttpRequestVideo + AppBase.GetVideoDetailV2
            + "?id=" + id, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getVideoUser(phone: string, page: number = 1, count: number = 1): Promise<any> {
        console.log(AppBase.HttpRequestVideo + AppBase.GetVideoUser
            + "?phone=" + phone
            + "&page=" + page
            + "&count=" + count);
            
        return this.http.get(AppBase.HttpRequestVideo + AppBase.GetVideoUser
            + "?phone=" + phone
            + "&page=" + page
            + "&count=" + count, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    addVideo(category_id: number, title: string, summary: string, phone: string, path: string, objectKey: string,
        province_id: number) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            category_id: category_id,
            title: title,
            summary: summary,
            phone: phone,
            path: path,
            province_id: province_id,
            objectKey: objectKey
        };

        let postData = this.jsonToURLEncoded(Data);
        console.log(JSON.stringify(Data));
        return this.http.post(AppBase.HttpRequestVideo + AppBase.AddVideoV2
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    editVideo(video_id: string, category_id: string, title: string, summary: string, phone: string, path: string,
        province_id: number) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            video_id: video_id,
            category_id: category_id,
            province_id: province_id,
            title: title,
            summary: summary,
            phone: phone,
            path: path
        };

        let postData = this.jsonToURLEncoded(Data);
        console.log(JSON.stringify(Data));
        return this.http.post(AppBase.HttpRequestVideo + AppBase.EditVideoV2
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    removeVideo(video_id: string) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            video_id: video_id,
        };

        let postData = this.jsonToURLEncoded(Data);
        // console.log(JSON.stringify(Data));
        return this.http.post(AppBase.HttpRequestVideo + AppBase.RemoveVideoV2
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    uploadFile(url, file, type) {
        var headers = new Headers();
        headers.append('Content-Type', type);
        const requestOptions = new RequestOptions({ headers: headers });

        return this.http.put(url, file, requestOptions);
    }

    getNotification(uid: number, page: number = 1, count: number = 1): Promise<any> {
        let url = AppBase.HttpRequestVideo + AppBase.GetNotification
        + "?uid=" + uid
        + "&page=" + page
        + "&count=" + count;
        console.log(url);
            
        return this.http.get(url, {}).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    updateNotification(notification_id){
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const requestOptions = new RequestOptions({ headers: headers });

        let Data = {
            notification_id: notification_id,
        };

        let postData = this.jsonToURLEncoded(Data);
        // console.log(JSON.stringify(Data));
        return this.http.post(AppBase.HttpRequestVideo + AppBase.UpdateNotification
            , postData, requestOptions).toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private handleError(res: Response | any) {
        console.error('Entering handleError');
        console.dir(res);
        return Promise.reject(res.message || res);
    }

    //convert a json object to the url encoded format of key=value&anotherkye=anothervalue
    jsonToURLEncoded(jsonString) {
        return Object.keys(jsonString).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(jsonString[key]);
        }).join('&');
    }
}