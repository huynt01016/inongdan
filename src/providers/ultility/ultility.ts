import { Injectable } from '@angular/core';
import { ToastController, LoadingController, PopoverController, ModalController } from '@ionic/angular';
import * as moment from 'moment';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Global } from '../appbase/appbase';
import { ApiProvider } from '../api/api';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions , CaptureVideoOptions } from '@ionic-native/media-capture/ngx';
import { PopupNotificationPage } from 'src/app/popup-notification/popup-notification.page';
// import { VideoCapturePlus, VideoCapturePlusOptions, MediaFile } from '@ionic-native/video-capture-plus/ngx';
/*
  Generated class for the UltilityProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UltilityProvider {
  loaderToShow: any;
  constructor(
    public toastController: ToastController,
    public iab: InAppBrowser,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public global: Global,
    public api: ApiProvider,
    private mediaCapture: MediaCapture) {
  }

  numberWithDot(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }
  
  capitalizeFirstLetter(s){
    return s.charAt(0).toUpperCase() + s.slice(1);
  }

  convertToDate(time: string, showDay: boolean = false, showYear: boolean = true) {
    var result = "";
    moment.locale("vi");
    var x = moment(time, 'DD/MM/YYYY HH:mm:ss');
    // console.log(time);
    // console.log(x.date(), x.month(), x.year(), x.hour(), x.minutes(), x.second());
    var dayName = this.getDayInWeek(x.day());

    var curDate = new Date();
    if (curDate.getFullYear() == x.year() && curDate.getMonth() == x.month() && curDate.getDate() == x.date()) {
      let deltaHour = Math.max(curDate.getHours() - x.hour(), 0);
      let deltaMinute = Math.max(curDate.getMinutes() - x.minute(), 0);
      result = deltaHour > 0 ? `${deltaHour} giờ trước` : `${deltaMinute} phút trước`
    }
    else if (showDay) {
      result = (dayName + ", " + x.date() + "/" + (x.month() + 1) + "/" + x.year());
    }
    else {
      if (showYear)
        result = x.format('D MMMM, YYYY')
      else
        result = x.format('D MMMM')
    }

    return result;
  }

  getDayInWeek(day: number) {
    var dayName = "";
    if (day == 0)
      dayName = "Chủ nhật";
    else if (day == 1)
      dayName = "Thứ hai";
    else if (day == 2)
      dayName = "Thứ ba";
    else if (day == 3)
      dayName = "Thứ tư";
    else if (day == 4)
      dayName = "Thứ năm";
    else if (day == 5)
      dayName = "Thứ sáu";
    else if (day == 6)
      dayName = "Thứ bảy";

    return dayName;
  }

  async presentNoti(data: any, duration: number = 3000){
    const modal = await this.modalCtrl.create({
      component: PopupNotificationPage,    
      cssClass: 'notification-popup',
      id:'notification-popup',
      componentProps: {
        duration: duration,
        title: data.title,
        content: data.content
      }
    });
    return await modal.present();
  }
  async presentToast(message: any, type: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      position: 'bottom',
      cssClass: 'mihn-toast-' + type
    });
    toast.present();
  }

  openUrl(link) {
    const browser = this.iab.create(link);
    // browser.executeScript(...);
    // browser.insertCSS(...);

    browser.on('loadstop').subscribe(event => {
      browser.insertCSS({ code: "body{color: red;" });
    });

    browser.close();
  }

  xoa_dau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;
  }

  getfistlastname(str, maxLength) {
    var fullName = str.split(' '),
      lastName = fullName[0],
      firstName = fullName[fullName.length - 1];
    var shortName = lastName + ' ' + firstName;
    if (str.length > maxLength)
      return shortName;
    else return str;
  }

  cropString(str, symbol, minLength, maxLength) {
    if (str.length <= minLength)
      return str;
    var indexOfSymbol = str.indexOf(symbol, minLength);
    if (indexOfSymbol < 0) {
      return str.substring(0, maxLength);
    }
    else {
      return str.substring(0, indexOfSymbol);
    }
  }

  async loginWithData(username, password, OnDone) {
    const loading = await this.loadingCtrl.create({
      message: 'Vui lòng chờ',
      duration: 20000
    });
    await loading.present();

    this.api.login(username, password).then(data => {
      // console.log(JSON.stringify(data.status));
      // console.log(JSON.stringify(data.user_info));
      if (data.status == "nok") {
        if (data.message.includes("user") && data.message.includes("password")) {
          this.presentNoti({
            title:'Đăng nhập thất bại', 
            content:"'Tên đăng nhập' hoặc 'Mật khẩu' không đúng."
          });
        }
        else if (data.message.includes("User") && data.message.includes("exists")) {
          this.presentNoti({
            title:'Đăng nhập thất bại', 
            content:"'Tài khoản' không tồn tại."
          });
        }
        else{
          this.presentNoti({
            title:'Đăng nhập thất bại', 
            content:"Có lỗi xảy ra. (" + data.message + ")"
          });
        }
      }
      else if (data.status == "ok") {

        this.global.isLogin = true;
        this.global.userInfo = data.user_info[0];
        this.presentNoti({
          title:'Đăng nhập thành công', 
          content:`Chào mừng bạn '${this.global.userInfo.full_name}'`
        });
        if (OnDone) {
          OnDone(username, password);
        }
      }

      loading.dismiss();

    }).catch(err => {
      loading.dismiss();
    });
  }

  generateFromImage(img, MAX_WIDTH: number = 700, MAX_HEIGHT: number = 700, quality: number = 1, callback) {
    var canvas: any = document.createElement("canvas");
    var image = new Image();

    image.onload = () => {
      var width = image.width;
      var height = image.height;

      if (width > height) {
        if (width > MAX_WIDTH) {
          height *= MAX_WIDTH / width;
          width = MAX_WIDTH;
        }
      } else {
        if (height > MAX_HEIGHT) {
          width *= MAX_HEIGHT / height;
          height = MAX_HEIGHT;
        }
      }
      canvas.width = width;
      canvas.height = height;
      var ctx = canvas.getContext("2d");

      ctx.drawImage(image, 0, 0, width, height);

      // IMPORTANT: 'jpeg' NOT 'jpg'
      var dataUrl = canvas.toDataURL('image/jpeg', quality);

      callback(dataUrl)
    }
    image.src = img;
  }

  async captureAndUpload(sourceType: number, onCapture, onDone, onError) {
    const options: CameraOptions = {
      quality: 100,
      correctOrientation: true,
      destinationType: Camera.DestinationType.FILE_URI,
      encodingType: Camera.EncodingType.JPEG,
      mediaType: Camera.MediaType.PICTURE,
      sourceType: sourceType
    }

    console.log('Bắt đầu lấy ảnh');
    Camera.getPicture(options).then((imageData) => {
      console.log("-->getPicture: " + imageData);
      if(!imageData){
        onError("File Type is not valid.");
        return;
      }
      else if(!imageData.includes('file:///'))
        imageData = 'file:///' + imageData;
      // Resolve the picture URI to a file
      File.resolveLocalFilesystemUrl(imageData).then(oneFile => {
        if(oneFile.name.includes('.png') || oneFile.name.includes('.jpg') || oneFile.name.includes('.mp4')){        
          this.convertSourceFromNativeUrl(imageData, onCapture);
        }
        else{
          onError("File Type is not valid.");
          return;
        }
        console.log('Lấy ảnh thành công ultility');
        console.log('File' + oneFile.name);
        console.log('File' + oneFile.nativeURL);
        onDone(oneFile);
        // var defaultImage = 'data:image/jpeg;base64,' + imageData;
        // this.createThumbnail(defaultImage, onDone);
      }, (err) => {
        onError(err);
      });
    });
  }

  
  async captureVideoAndImage(type, onCapture, onDone, onError){
    
    let optionsVideo: CaptureVideoOptions  = {
      limit: 1,
      duration: 30
    }
    let optionsImage: CaptureImageOptions  = {
      limit: 1,
    }
    let capture = null;
    try{
      if(type == 0)
        capture = await this.mediaCapture.captureVideo(optionsVideo);
      else
        capture = await this.mediaCapture.captureImage(optionsImage);

      console.log("-->videoCapture: " + capture[0].fullPath);
      let imageData = capture[0].fullPath;
      if(!imageData){
        onError("File Type is not valid.");
        return;
      }else if(imageData.includes('/private')){
        //ios
        imageData = imageData.replace('/private','file://');
      }
      else if(!imageData.includes('file:///')){
        imageData = 'file:///' + imageData;
      }
      
      console.log("-->getPicture: " + imageData);

      // Resolve the picture URI to a file
      File.resolveLocalFilesystemUrl(imageData).then(oneFile => {
        if(oneFile.name.includes('.png') || oneFile.name.includes('.jpg') 
        || oneFile.name.includes('.mp4') || oneFile.name.includes('.MOV') ){        
          this.convertSourceFromNativeUrl(imageData, onCapture);
        }
        else{
          onError("File Type is not valid.");
          return;
        }
        console.log('-->File Success: ' + oneFile.name);
        console.log('-->File Success: ' + oneFile.nativeURL);
        onDone(oneFile);
      }, (err) => {
        onError(err);
      });
    }
    catch(err){
      onError(err);
    }
  }

  async capturePhotoOrTakeVideo(sourceType: number, onCapture, onDone, onError) {
    const options: CameraOptions = {
      quality: 100,
      correctOrientation: true,
      destinationType: Camera.DestinationType.FILE_URI,
      encodingType: Camera.EncodingType.JPEG,
      mediaType: Camera.MediaType.VIDEO,
      sourceType: sourceType
    }

    console.log('Bắt đầu lấy ảnh');
    Camera.getPicture(options).then((imageData) => {
      if(!imageData){
        onError("File Type is not valid.");
        return;
      }else if(imageData.includes('file:///private')){
        //ios
        console.log("-->replace: " + imageData);
        imageData = imageData.replace('file:///private','file://');
      }
      else if(!imageData.includes('file:///'))
        imageData = 'file:///' + imageData;

      console.log("-->getPicture: " + imageData);
      // Resolve the picture URI to a file
      File.resolveLocalFilesystemUrl(imageData).then(oneFile => {
        if(oneFile.name.includes('.png') || oneFile.name.includes('.jpg') 
          || oneFile.name.includes('.mp4')){        
          this.convertSourceFromNativeUrl(imageData, onCapture);
        }
        else{
          onError("File Type is not valid.");
          return;
        }
        console.log('Lấy ảnh thành công ultility');
        console.log('File' + oneFile.name);
        console.log('File' + oneFile.nativeURL);
        onDone(oneFile);
        // var defaultImage = 'data:image/jpeg;base64,' + imageData;
        // this.createThumbnail(defaultImage, onDone);
      }, (err) => {
        onError(err);
      });
    }).catch(err=> onError(err));
  }

  convertSourceFromNativeUrl(imageData, onDone){    
    console.log("Capture: " + JSON.stringify(imageData));
    var img = (<any>window).Ionic.WebView.convertFileSrc(imageData);
    console.log("Capture after convert: " + JSON.stringify(img));

    onDone(img);
  }

  async captureAndUploadBase64(sourceType: number, onDone, onError) {
    const options: CameraOptions = {
      quality: 25,
      targetWidth: 720,
      correctOrientation: true,
      destinationType: Camera.DestinationType.DATA_URL,
      encodingType: Camera.EncodingType.JPEG,
      mediaType: Camera.MediaType.PICTURE,
      sourceType: sourceType
    }

    Camera.getPicture(options).then((imageData) => {
      var defaultImage = 'data:image/jpeg;base64,' + imageData;
      this.createThumbnail(defaultImage, onDone);
    }, (err) => {
      onError(err);
    });
  }

  createThumbnail(data, onDone) {
    this.generateFromImage(data, 512, 512, 0.5, data => {
      if (onDone)
        onDone(data);
    });
  }

  getImageBase64Sameple() {
    return "iVBORw0KGgoAAAANSUhEUgAAAPEAAADRCAMAAAAquaQNAAAAkFBMVEX///8MNZMALpEAMZIAJY43UZ8AIo3x8/hXbK28xNzt8PcAJ45EXKSYo8lfcq8ALJAAG4sAGIvi5O97irt0hLjL0eSmsNGiqsuIlsION5TQ1+gnRpsAH40AFIoAD4lld7HDyuDZ3uySncaGk8CvuNUjQ5lCW6QaPZcrSZwyTp5QZand4u5qfLQAB4i2v9nv8vg8kZGeAAAKBklEQVR4nO1daWOiOhQtBKFKFdBqtUVL1YrVefb//7s3040T2siS5CbMeD5aSjiQ5ebcJVdX5vE0eXFUIj8MA9OczmK9ipQSdhzmRU+mWZ3BLFHM941z+mqalxBBqoGw40SPpokJcfK1MHZc08SEWId6GMdj08xEmF8YXxhfGP9VjJksusaY9W4k8dwxxqm0aTiJOsb4XvZmF8ZW4sJYChfGVuLCWAoXxlbiwlgKF8ZW4sJYChfG1BhM+5XIHt2vhwyH4uumgzotmmV83K1SvxoF4d+UxZely32NHmCU8TRGLgrAlqfKRk0yDnx25unbYVnZs00yXnvKCTvRbVWrJhn31H9ih+VVrZpkvFBPuIb37O9jXOX6vzCmxIXxv8U4lEIHGedDGdztO8eY7eXudGCEjAfVe583/GDjA+OHNk0XuKFjPO0lNfY+fxA7d+V/7iLj/rKBoRiXw6c6yHiwqs/3TwMz/t87yPi2WbAkW/D/3kHGecPNT8xPXx1k3HS3F665f+8g4+eGlNmO+/cOMm44jn83wT1OBxmPm83VjuNzwluhgZQ+fmNcg0JYdS2Etaa15F4Os1Wzfs2rUIXwHm4aN83h7ksxcytjpqfx13tu07OyPPG9KgBlTpMZ/Pf5c9z8ZXMIvgKw/6uO99p/fqVk26qxp+2sCkA5OeL/zt5NNrZq1zQg++hsy3X1teMPlTwdybYqBMwrHm9cn1jqeXHel29j+hx7YcxqvbrjTeKFvlfj5bTFsJgr3EPpb9lwOFXTyvRumNW99mk2POlM/rmPi24d2p1lpAowdaWZ6YchAXhDI8l1qCPYFtk77MX0w5BgABlasktvR/BQrE++9NrbCWxgfbI3dU4lptCtfdMPQwPILY1tToJVB9zOufZica3A4H3HTEMAhA648U6RUXjUkTKtBeGNGsaNJTFziBV17MaSmDG412oY9zWliasHe1bD+LUjU5c6xle7rgxkV5UYNNSUQ6wc3jevb0ugEGI11NmE3NQlnTetFvhoqSrCKIQ4ec8uQBa2qsXpN7bFbC0b6KIcYC14Q2V3HYChmVqWkoHDWDoRo8CLtULIGOVlhffdFF3HMiEE6gZ9cyHIAIWQSp8nKWAYh+qG8dVVAJsJlaNFHiA8qn2wg6vnVcoCxeVIqZfoDtYnyaAApYBhzFTpAe9AIWRpkccNIkNCxS5WGMi+MglNHrBspopcu5+AOTGaqL21BF5RWub/NKiFM90VhZCF+DJiwFOxHvwebPKkFryXmejeAdg26VF0FTXAMgrnxc9jN6wrYrBUaLdgRIjwvVBjD8M4K35eNBFtvF+Cm0P0mMJdmRyw4yXFkBw2kyJXgr0RCiGV4YREyAr7A62EhkmVwk0mXKN6IWgLSNnH0MG8EWHx2jMCQ3MuuIYY8DH9rPi5oRNFGN+CQogdESEBaKw+lP791cyJ4ouS21EIqU4HpwAOY1yN75dNCLNI2EBumxACywffM2+bFFReZcIGUAgRrWGkABOhZOtvll5UD36UiRtAIUTcEwgBz1MOMB8Mb2thcr6zwsIuX/tSHvAFlLnYSri2SwjBYaxpO4dCiFrBoRVAiRIuMJI4gqHpmTc0KVbLXDg5GsC9/mFsmRACXu3qajFtgZKDttdaFwTDuLQfNe1xg+2CxqICPXuEkKfi7evsbxYJIbBURhptXhRC1IUgtAKYQ57OfQ3sPA0LITCMtTrxH20RQmAYO0xnQyiE9Kov1weIgda7dcUgBN+kEAKdTfOqgREh2tb9GoCda6LXRQJCiM5FoQq4qdF81FNmh8cNh7HuUBzMcTMnhNANY1siQnLC926FEDKmjLbCiJBEd2MiQMYsgX0PBefQ2UMK8LO0iSIfT8+jNE5sEELAl9ZcR97maQVixsUNncwLIWMYWY3DT2+TGq5HH6eoVxTqzQghW4nw0229FD0fu+/OuBAiE35a17e8AnkahRB9tVfOIW8ffvpUN8sFdw33pj1uA/APx9WXc8jqOlq5zgOuefkTE1pAJoq8diYTN2AxIkRjhR0hpMJP60Y+cTFPplNjMIq8sdi2rkc54mYoHEdL+sOmB/DMLWoN7epQDhf8jR+MCiF9ySjyiV9Vic2PH0tv0qwQspGNIg9Od+ex/WZY4RRPL4T09EWRC4GhY+RCCJ4FT+e3ByFEYbJgPWSCKHK9ACFEaSZZHUChIUK3yNFIz3pH7+coct2ApYu6GB0aA4QvmxNCAkr0DRWFA2PecSsrq6oELBOkhf+CRgG9mkAbYaXjgKemiEnnzLX5GiHEDmwLaoRQa8fmezX1tm2k+PTB5mhRplwKW9MVncjll3GT7AsdSMm1csPF6DzqHQwnRdCXlQyXBsqSYESIe02L0cZIiQ70DJgOPaaBHREhlLAiIoQUGBFiQWoMBfKf03//YtgQEUILjAixrRidHqDL6x9ZnyxKjSGC+YgQaqAQ8o9U5QfGdhWj0wbTESH0mFlXI0Q3MEDSaGoMHfaWpMbQYYI1QoJ6ld7awJ6NCkaE6PS4haFzWFtQkYMvlqoZbpjkFggPQcNaWHJgHlNVab4ttj61hOv3jC4JtwaOjHFNHgn4aOYwEVWnvzTHL1OnpywNZbcPzXmQPSNj+cngsU+uEbHYaCBIbMCi7Zf6NHO/vQHW2o/2w51Kd9daLOFn8J/YCw+jwyLlfmOHUUs8PnPswnQ3Gu1j7j2k5PM1N4pd990Syl4wbiBsbx7hm2Px5m2eehphr6IfyXMMnStKzk5gwWovYeP2hLlfsvAWKZPnD0LJCC5dE8uRJm3XEPTFe1AIYobHexJX7AzwRHeU9LjCrG3HGiZGcrsl8N9Su32gOEXpsIWNgjM8IKebz+6B7k4dyA4RESVeUMSy7WcIhNk9gTkhEYKcS6WioAxO28SgV7hFqYBe8fFZ3vLRWwK6l1bG5SJFC2OMwf1Sys6YFl+/bYXDQFgfymCvhoC9UtPSCWdXXHkTfpfU13SYUx3g6UcZ/I7LVuvVCao08z0Ili3SjIE/4MxqWEGuVVggc0FKN56aQO4PwGM23f0XtUf4xO1D3LkTw7wvwYOTIMh9PkfMDGHe/O0zzxaoXkuk93HbpPjXm6GZ9VBkIh/Gv8ca91Rh4vTyxON2ixJfYc4lYURx1NunvE7MTR40yMoqFyvt2WWq7w7KilL55tSr8RtuKrIGVjLp9vOKAP0kU0WjAcbnpcxYzl3ycFZFi8yc+Xh3jnIoWTXrrFLKHEPO1TMKvSsdwrddiRkvjblVhV6Y6Fn+I9yJvjIzUmfmA5Of8zX9nYped4p/HMsRM+o4P/nfPebuSlGu+3j/vQ+x+GA4QCJ4jHnObrxTV5R1GPFGjZvmFoTXHCdR+nHyHYv8eJSpvHkwfEi8iH3e/GAB3zdM19cPC8d5OMz76rvccbu5fl7kL6PNqZZN8z8qLuPflO7IMwAAAABJRU5ErkJggg==";
  }

  getImageQuestionDefault() {
    return "iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAACXBIWXMAAAsTAAALEwEAmpwYAAAc3klEQVR4nO3deVwV1f8G8OdedkE2QUEEcQFXXNDMBcM191wyc89My1wyLc2FJDXLXX8uWZaKWaYllvuSC+7KV1xITQE3EBGURQQRBOb3h0nSDMhyuXPnzvP+Jztn7nl9UOa5M2dmzmgEQRBARGokaOWugIjkwwAgUjEGAJGKMQCIVIwBQKRiDAAiFWMAEKkYA4BIxRgARCrGACBSMQYAkYoxAIhUjAFApGIMACIVYwAQqRgDgEjFGABEKsYAIFIxBgCRijEAiFSMAUCkYgwAIhVjABCpGAOASMUYAEQqxgAgUjEGAJGKMQCIVIwBQKRiDAAiFWMAEKkYA4BIxRgARCrGACBSMQYAkYoxAIhUjAFApGIMACIVYwAQqRgDgEjFTOUuoKzkZucgJjwCSTHxSLl7Hylx95GTlS13WYZLA5R3coC9W0U4uDnDrX5NWDvYyl0VlTGjCoCcp9kI330c534/hAs7jiI96aHcJSmW1tQEtds0hW/vdmjatwNsKzrKXRKVAY0gCILcRZSWIAgICz6I4GnLER8ZLXc5RsfC2gqdPhmCzp8OhWV5a7nLId0RFB8A8ZHR+H5IAG6c+UvuUoxeeWcHDFr+GZq93UnuUkg3lB0AVw6cwap+k5GenCp3KarSI2Akes0cBY2Wc8gKp9wAOL5uG4JGzkJuTm6h2zm6u8C+sjNsKzrCxNyopjx0KudpNrLSM5B0JwEPbt1FdmZWods3ebM9Rv0yFyZm/DtVMGUGwKV9J7Gk6zgIudI7f1XfOvAf2Qc+Xf1QwcNFz9UpX1ZGJi7vP4XQTXsRunk/CvoVafvhWxi8cio0Go2eKyQdUV4A3L1yA3NaDEVGarqoz8GtIvov/gRN+3bg4amO3PkrEpsmLsKVA2ck+wcu+wwdxvXXc1WkI8oKgNycXMz07Y+Y8EhRn1erRhgTvBC2lSrIUNnLPU55hKgTFxB94RrSEh8iPTkVOU+z4VilEipUdYVGq8Erb3WEtaOd3KWKCLm52BqwEru+Xivq02i1mHVxM9zq15ShMiolZQXAsTV/YN2ImaL2mi0bYvKh1TC1MJehqoJlpj3GifU7cPSH3xFzMaLAQ+kXNR/UFf4j+6CWfxM9VFg8e+YH4bfP/k/UXr9TS0zcu1KGiqiUlBMAmWmPMcW7Jx7GPcjX7uBWEYFhGw3qmz/r8RPs/GoNDq7YhIyHaSUao067ZugzZyxqNPfRcXUlJwgCVg+ahjO/7BX1TdizEj6dW8pQFZWCoJgT5TOb9ol2fgDov/gTg9r5rx0Jw4yG/bBzzg8l3vkB4O9Dofiq5TvYGrASudk5Oqyw5DQaDQavnIpy9uVFffsW/ShDRVRaigmAs1sOiNqq+tZB074dZKhG2t4F6zGvzQgkRMXoZDxBELBzzg+Y6/8e0hIN47ZmawdbdJk8TNR+NeQsb71WIEUEQHpyKq4cFM9C+4/sYxCz/YIgYMuUZfh18tJCtzO3skCN5j54pd/raDm0O3x7tYV7Q++XXkuPOnkRC9q/j7QHKbosu8ReG9FbdOkvNzsHF3YclakiKilF3MURcfSc5GGwT1c/GaoR2z5rNXbPW1dgv0/nlmg/tj/qdmwOU3MzUX9megYu/3kah7/5FZf/PC05RszFCCzsOArTTgTBvJylrkovkfLODqjWrL7o9usrB86g1Ts9ZKqKSkL+r88iSLwdJ2pzdHcxiJt8rhwMxfaZ30n2uXhXxbQTQZiwZyUadGstufMDzx628e3VFp/sX4Xpp36Ee0Nvye2iL1zDpomLdFV6qXj5NRK1JUaL/53IsCniCCAp5p6ozb6yswyV5Jf2IAWrB02TvLzXsPtr+HDzvHzf1lmPn+DslgO4EXoJjxKSYGZpDpdanqj3egt4Nq0LjUaDGs19EHB6AzaOn48jq4NF44Z8twX1O7eEb6+2ZfqzvYzU339STLwMlVBpKCIAkmMTRG2G8Hz63oU/IjU+UdTeuGcbjP5tQd65fWpCEvYv+QlHVm+VnCjbGrASddo1w8Blk+FWrwbMLM0x9NvpMDE1waFvfhVt/+ukJWjU/TVoTU10/0MVkZ2Lk6gt+Q4DQGkUcQqQnfVU1Cb3gz2P7ifj4IpNonanam4YsX523s4fEx6JWU0HYffcdYXOkv99KBSBDfvhj8BVEAQBGo0GA5d9JnlDUEJUDE78uEN3P0wJSJ3O5DzliktKo4gAMET7l/yEzPQMUfvwtV/Ays4GwD/X8lsNkzyFkZKbk4vts1bn3XKrNdFixI9fSk767V/ycymqJ3qGAVBCUrP1tfyboHabpgCAjIdp+H5IADLTHhd77K3TV+D0xj0AgAoeLmj74VuibWIvReFexO1ij030IgZACaXcvS9q6zBuQN6ft0xdJrkN8OzZhRaDu8GrlXgm/bngacvzDqnbjhIHAACc23qoOCUTiShiEtAQWdqUE7Vd3HUMDXu8huunwhHy7RZRv7WjHcb9sRjerX3z2iJPXMBPY75GzMWIfNsm3o7DmU370HJIN1Ss6Y7Kdavj7pUb+ba5efayjn4aUiseAZRQVd/aorbj67bhfYtmmNdmhOSlweFrv8i38wPPHmOecmQN7FzFs+qhm/fl/blas/qi/vgILoBKpcMAKKEWg7sVa/sqPl5o9Ia/ZJ+VnY3kHXT3b9zJ+7NzNTdRf8J13TxzQOrFACghn65+qNvh1SJtqzXRYsiqaYUunVW1sfiI4sUbayzLi085oIwnucmAMQBKSKPRYMyWhfDya1zodiZmphi5YU6hE34A8Fji0WH7F04LpK6xc0FOKi0GQClY2dlg8qHVeGveeMk7E2v5N0HA6R/x6oDOhY4jCAJObdglaq/kXTXvz1J3Q1ra2pSgaqJ/8SuklEzMTNFl8jB0mjgEN89ewf3rMTAxN4Nnkzpwrl4FgiDgyOpgXDt6Ds7V3dB+zNv5FjDJzc7B5klLEHHsnGjs2i/cBfjfqwQA4N7Aq2x+KFINBoCOaE1NUKO5j2gJr91z1yF42vK8/z+0YjPaju4Hj0a1kBL3AMd+2Cq5yKmZpTn8hvcCAGSkpuP6qXDRNlV96+j4pyC1YQCUsf8ulZWenIqdc3546ef8R/aBTYVnKwSHbtor+TxE7bZNdVMkqRbnAMpYSZbyqvZKPbw1fwIAIDszC7vnBYm2qeDhknfbMVFJMQAMTBUfL4wJXggzy2dLnO/8ak2++wGeazm0h0Esh0bKxlMAA6E1NUHnT4ei1xej8t5vcHbLAWyftVq0rZWdDTp+PEjfJZIRYgDIxM6lAso7O8LBzRmNe7VF0zc7wMbJPq8/LPggVg+eLvnZHtNH5M0PEJUGA0AGo39bUOBy5plpj7F99vfYMz9Isr+qbx20f+GpQ6LSYAAYiEf3k3F64x7snrdO8gUowLOnCV+cHyAqLQaAzJ4+ycKqfpNxYceRQrczL2eJMcEL4eRZWU+VkRowAGS2aeLCl+785ezLY8LuFajRooGeqiK14HUkmZ1YX/jinh6Na2PaiSDu/FQmeAQgs6zHTyTbTc3N0G3qcHSfPoJP/VGZ4W+WAXKtUw2fHf7eoN56TMaJpwAGqPes0dz5SS8YAEQqxgAoY1LLgGVnZj37r8QTfgV9hqgscA6gjFWo6ooHt+7ma9u/5CcIgoDI4xcK/AyRPvAIoIw16NZa1HYr7G98PyQAId+J3x1gX9kZHhILhBKVBQZAGesZ+EGx3mQ8eMUUaE34z0L6wd+0Mlbe2QHDfphRpG1bvdMDvr3blXFFRP9iAOhBox7++HjXcti5SF/a02i16BEwEu+s/lzPlZHacRJQTxp09cPsS8HY8eX3CN91DEkx92BlawMvv8boMvkdVH/V5+WDEOkYA0CPLG2s4ORZGU6elaHRamFlZwMnz8rFmiMg0iUGgJ5EX7iG74cEIPZSVL72m6GXELI6GAOWfIrW7/VS/D0AgiAo/mdQE84B6EHUyYuY3WywaOd/LjPtMYJGzsr3/gClEnL5vkIlYQCUscy0x/h+SIDku/3+a8+8IEQcFb8hSEk0Wn77KwkDoIzt+nqt5LLeUgRBQNDIWRAU8Nbf57cz/xcP/5WFAVDGzgYfFLVVqOqKHp+PRP1OLUV99yJuF3iqYEge3ksUtVk7cqVipeEkYBmLj4wWtfUM/AB+7/ZEZnoGPrQRh0B8ZDSq+Bj2iz9T7t4XtXk0qiVDJVQaPAIoY+ZWFqK2xNtx+f77X2aW4s8YmmtHzoranKpxwVKl4RFAGfNsWhfXjoTla9s28zscX7cND+OTRNtrNBp4NjHst/6m3L2PW2F/i9obSjz4RIaNRwBlrEMBL/FIjL4nOZHWrH8ng18N6PC3v4nazMtZSs5pkGFjAJSxJm+2x6sDOhdpWztXJwxaPqWMKyqd1IQk7F/8k6i9YffXYF7OUoaKqDQYAHoweOVU1Ghe+L3+NhXsMOqXuQb9zj8hNxfr35+NzPSMfO3PHmYaIVNVVBoMAD2wdrDF1GPr0Hv2aGhNTUT9Pp1bYvZfW1DLv4kM1RXd759/g/PbQkTtrYf3NPirFiSNk4B6ojU1QY+AkXhtRB9cOXAaSTHxsLKzQc2WDeHe0Nugb6DJznqKjR/Nl1zByMrOBr1njZahKtIFBoCe2blUQIvB3eQuo8gij5/Hpk8W42boJVGf1kSL0b8tgJ2rkwyVkS4wAEgkNT4RF3cdQ+jm/bi8/1SB2/VfMgn1OjbXY2Wka6oOgNjL1/HTmK/zXafXaDSARgONBv/8V5OvTaPRQKPVvtAnvV3R2vK3Q6OBVqsp8Wdf1qZ9oW48/1n+2Q4A0hNT8PBeIlITxPcn/Ffv2aPRfuzbuv4nIT1TbQDEXr6Oef7vIS3xYb52QRAAQYDhP44jD3MrC4zcMAdN3mwvdymkA6oMgNhLUZjf7n3Rzk+F8/JrjMErpsC9obfcpZCOqC4AYsIjsaD9+0h7kCJ3KYrh4l0VfeeNR+OebQz6agUVn6oCIOZixLOdn9/8L1Wxpjua9GmPpm+2h2fTus/mPcjoqCYAos9fxYIOo5CeJL3ze/k1RsfxA/VclWGxcbKHY5VKsHerKPkUIxkfVQTA7XN/Y2GHUUhPTpXsf3vRRHSaOETPVRHJz+gD4NbZK1jYcRQepzyS7B+wdJLqv/lJvYw6AG7+7zIWdhyFjIdpkv2DV0xBuzG8lk3qZbQBcOPMX1j0+ofISE2X7B+6ajrajOqr56qIDItRBsD1U+FY1Gk0njwS7/wajQbvrP4cr43oLUNl8snIzEJsfBJiE5LwIPmRIlYelsvVm3eh0QDv9PSHi5M9TE3ET3AaC6MLgKiTF7G485gCd/531wTC792eMlSmX4Ig4OzlG9h6IBRbD5xBxC3p9QepYAHLNsNEq0XLxt7o+3pz9OnQDFUMfLWm4jKqAIg8fh6Lu4xFZtpjUZ9Go8F7QbPQcmh3GSrTH0EQsCMkDFOX/oIr14v2PgIqWE5uLo6FXcWxsKsY/3UQerdvhrkTBsLb01Xu0nTCaAIg4ug5LOk6VrRaDfBsxZqRP85G80FdZahMf8IjojF2zhocC7sqdylG6/eDodgREoYP+3fEl+P6w9bGSu6SSsUobu+6diQMi7uMkdz5tSZafLDxK6Pf+YP/PIPmA6Zz59eD7JwcLP95L1oODsDN2AS5yykVxQfA1cP/w5KuY5H1+ImoT2tqglGb5qHZ251kqEw/BEHA7G+D0XfCYmQU8LouKhuXo+6gWf9pOHH+mtyllJiiTwGuHAzFsh4fISsjU9SnNTXB6F/nw7d3Oxkq05/lG/dixopfC+y3symHbv6+6O7vCx8vD7g42cPRzubZugMkKTdXQOLDR4i7n4KL125hR0gYdh89j3SJ37MHyY/QZdTXOPPLHNSp7iZDtaWjERRwPWhl308R9p937FnZWiM76ymePhF/65mYmWLMloVo9Ia/vkqUxZ+nwtH5g6+QK/FK7vLWVvh0WHdMGNoN5a2VfZ5qCJIepmH+2u1Y9tMeySOtmh4uCN30FRxsrWWorsQExZ4CZKSmS+78puZmGPf7YqPf+ePuJ6PfxKWSO38Dbw+Eb12AGR/25c6vI452Npg7YSDOB8+DV1XxFYCo6HsYOnWFDJWVjmIDQIqphTnG/bEEDVTwiqrAlb8hReJeh3av1seJn2bD081ZhqqMXy3Pyjjzyxw086kp6tt55Bz2nwyXoaqSM5oAMLM0x/jtS+HTpZXcpZS5y1F3sGbrIVG7t6crtiyZCBu+oadMOdha449ln8KtoqOob9LCDcjJzZWhqpIxigAwt7LA+J3LUe/1FnKXohdffhcsOvTXaDTYvPBjpZ2DKparswM2zB0rag+PiMbvB0JlqKhkFB8A5uUs8fHuFajbvpncpehFRmYWdoSEidoHdG2FRrU99V+QirVtVg+d/RqJ2rfsPyNDNSWj6ACwsLbCxL0rUbtNU7lL0Zs/T4ZLXo6aPPwNGaqhz94TP1ey+9h5ZGY9laGa4lNEANhKnGtZlrfGxH3fwLu1rwwVyWfnkXOiNk83ZzTw9pChGvLzrQX78vlPux6lZ+Bo2N8yVVQ8igiADuMG5Hv1tImZKT7Z9w28WokPv4xdVPQ9UVunVg25Wq9MTE1M0O7VeqJ2qX8nQ6SIOwFd61TDtBNBOBG0HU8ePUbPLz6Ao7uL3GXJIlbirT2elXnJT06ebhVFbbHxyTJUUnyKCAAA8GhUCx5LJ8ldhuzuJoh/sVyc7GWohJ5zcbITtUkFtSFSxCkA/StN4qEnXveXl9Tf/yOJJ1MNEQOASMUYAESlZPiP0xWMAUCkYgwAolJSwBP1BWIAEKkYA4ColHgEQESKpJgbgUi/7sQnYv22I9h34iIuR91B0j/vV6zv5Y7WvnUwqLsfWjWupdrxX6TgAwAGAOX3KD0Dc1b/jqUbdks+0XYpMgaXImOwavN+dPZrhEWThqBujSqqGd/Y8BSA8kTejkOz/tMxb822Ij3Ouvf4BTTpNwWb9pxUxfgFEaDcQwAeARCAZztP84EBeYfKRfUk8ykGTPo/pKY9xvtvdTDa8Y0VjwAIj9Iz8MbYBcXeeV405su1CPnfFaMc35jxCIAwZ/XvuHozVrKveUMv9O3YHFVcHJGaloHj567i553HRQtfZufk4IOZq3Hpj0UwM83/Om2lj/8ySr4MyABQuTvxiVi6Ybeo3drKAkunDMN7fdrmW2xkZN/2GD+4K96ZvhKXImPyfSbiVhzWbD2EUf06lmr8jwZ3wbDp3xjE+MaOpwAqt37bEckJs6VThmHEm+0kVxryrVsNu76ZIvnSkVWb9pd6/CZ1qxvM+EWh4AMABoDa7TtxUdTWoqE33uvTttDPebg6YdbYfqL28Iho3HuQYjTjGzsGgMpdjrojanuz46tFWmOwTwfppdhfHFPp4xeFkucAGAAqJzVzXsVFvAqzFLdK0tslp/47ptLHN3YMAJWzsjAXtaWmFW05q4K2s3xhTKWPXxQ8AiDFquFRSdR2/NzVIn325IVr0mO6/zum0sc3dgwAlWvtW0fU9vPO4wi7cqPQz2U9zcaUJRtF7c4OtqjlWdloxi8KBR8AMADUblB3P1FbTm4uhk3/BtFxDyQ/k/U0Gx/PXS+6jg4AA7v5Qav9dwJO6eMbOwaAyrVqXEvyBZeXImNQv9enWLphN6LjHiAnNxfJqenYffQ8Xnl7KlZtFl8vt7aywJQR+d+VV9zxdx09hyb9phjM+MaOdwISFk0agpD/XcaTzPw31DxKz8CEeesxYd76Io0T8EEfyZeUKH38l1Hy04A8AiDUrVEFa2d/WKox3uz4KiYPl/72VPr4xowBQACAAV1b4bvAkTA1Kd6DMMCznefHr8cUeu6s9PELw8uAZBTef6sD/vwhAN6erkXa3trKAl9/PAC/LpqAcpYWRj++MeIcAOXT5pW6uPTHIqzZegirNu1HeES0aBtnB1sM7OaHKSN6FvucWenjS1HwAQADgMTMTE0wql9HjOrXEfcepOBy1B0kp6bB0sIcNdwroZZn5VJdKlP6+MaEAUCFcnGyL9PXjyt9fIBzAESkUAwAolLiEQARKRIDgEjFGABEpaTgMwAGAJGaMQAURmqtu6yn2TJUQs/xYSDSG2cHW1FbQlKqDJWQMWAAKIxbJQdR292EZBkqoed4GZD0xq2ieCXboq6BR/RfDACFaVqvhqjtdHgkEpIeylANAbwKQHrUp6P4ZRaCIGBN8GEZqiGlYwAoTP2a7qjp4SJqn792O5JT02WoiDgHQHqj0WgwvLf4vXcpj9Ix7qu1iv5lJP1jACjQ+CFdJCcDf955HPPXbpehIlIqBoAClbO0wJcfvS3ZN2XJRkxd+gtycnP1XJV6KfmoiwGgUEPeeA0dWzSQ7Jv7wx/o/MFXki++IHoRA0ChTLRabF70MbyqSi+AeeDUX2jQZxIGfbYc2w6dRdrjJ3quUD0UfADAJcGUzMHWGttXTELLQZ9LXgEQBAEbdx3Hxl3HYW5mCk83Z7g42aOCXXnVr4knCAKa1KuOjwZ1gU05S7nLkQ0DQOFqV3PD6Y1z0GPsPETciitwu6yn2Yi4FVfoNmqz9UAoNu0+iZCgQDja2ZR4HD4MRLLy9nTF6Y1zCpwToIL9FRmNDiNmI+lhmtylyIIBYCQcbK2x97tp+HneOFSt7Cx3OYpy/u9baP/ebCSmPCrR55U8B8AAMCJarQYDu/nh6o4lWDx5KBrV9pS7JMW4cPUW2g2fhQfJJQsBpeIcgBGytDDDhKHdMGFoN9y4E4+tB0Jx4eotxMYnITYhGfeTUhV97bo0BAhITcuQ7AuPiEa74bNwcO3nkusuFDimgv8uGQBGrnqVSvh0WA+5yzAoV2/Gou27s3DvQYqo76/IaLR9dyYOrZ2Bio52MlSnXzwFINWpXc0NIUGBcHUWL64CAJej7qDtu7MQn1i0R6yVfATAACBVquVZGUeCAiWfqQCAK9fvoM2wmYi7b9yrLTEASLW8qroiJCgQVSpVkOx/fqpgzEuuMQBI1Wp6uCAkKBDuLtIhcO3WXbR5dyZiE5IKHEPBZwAMAKIa7pUQEhQID1cnyf7I23FoM2wm7sQn6rmysscAIMKzqyVHgr4o8CaqqOh7aDNsJmLuiUOAk4BERsDTzRlHggLh6SYdAtdj4tFm2ExExz3Qc2VlhwFA9IKqlZ1xJOgLVK9SSbL/xp14+A/7Ardi7+e18WEgIiPi4eqEkKBA1HCXDoFbsffR5t2ZuBmboOfKdI8BQCTB3aUCQoICC1xw5fbd+2gzbCZu3InnVQAiY1SlUgUcXjcD3p7SIRAd9wBths3E9Zh7eq5MdxgARIVwq+iIkHWBqOVZWbI/5l4iftt3Ws9V6Q4DgOglXJ0dEBIUiNrV3OQuRecYAERF4OJkj5CgQNStUUXuUnSKAUBURJUq2OHwuhmoV9N4QoABQFQMFR3tcHhdIHy8POQuRScYAETF5Oxgi0NrZ6CBd8EhoJTbgxkARCXg5FAeh9bOKHDdxb8U8lYmBgBRCVWwL4+Daz5H4zqeor4hPVrrv6ASYAAQlYKjnQ0OrpmB5g298tp8vDzw0eAuMlZVdBpBKScrRAZMEAQcDr2M3FwBfr61YWlhJndJRSEwAIjUS+ApAJGKMQCIVIwBQKRiDAAiFWMAEKkYA4BIxRgARCrGACBSMQYAkYoxAIhUjAFApGIMACIVYwAQqRgDgEjFGABEKsYAIFIxBgCRijEAiFSMAUCkYgwAIhVjABCpGAOASMUYAEQqxgAgUjEGAJGKMQCIVIwBQKRiDAAiFWMAEKkYA4BIxRgARCrGACBSMQYAkYoxAIhUjAFApGIMACIVYwAQqRgDgEjFGABEKsYAIFIxBgCRijEAiFTs/wH0NLtdfhXtnAAAAABJRU5ErkJggg==";
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
}
