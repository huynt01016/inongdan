import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [{ path: '', loadChildren: '../home/home.module#HomePageModule'}]
      },
      {
        path: 'video',
        children: [{ path: '', loadChildren: '../video/video.module#VideoPageModule'}]
      },
      {
        path: 'market',
        children: [
          { path: '', loadChildren: '../market/market-category/market-category.module#MarketCategoryPageModule'},
          { path: 'category', loadChildren: '../market/market/market.module#MarketPageModule'},

        ]
      },
      {
        path: 'help',
        children: [{ path: '', loadChildren: '../help/help/help.module#HelpPageModule'}]
      },     
      {
        path: 'category',
        children: [{ path: '', loadChildren: '../notification/notification.module#NotificationPageModule'}]
      },
      {
        path: 'settings',
        children: [{ path: '', loadChildren: '../settings/settings.module#SettingsPageModule'}]
      },
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
