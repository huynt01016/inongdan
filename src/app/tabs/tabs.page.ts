import { Component } from '@angular/core';
import { Platform, Events, ModalController, PopoverController } from '@ionic/angular';
import { UltilityProvider } from '../../providers/ultility/ultility';
import { Global } from '../../providers/appbase/appbase';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(
    public events: Events,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    public global: Global,
    public platform: Platform,
    public ultility: UltilityProvider,) {
    this.platform.ready().then(() => {

      var self = this;
      document.addEventListener("backbutton", function (e) {
        self.closeAllModal();
      }, false);

    }
    );

    var self = this;
    this.events.subscribe('back', () => {
      self.closeModal();
    });
  }


  async closeAllModal() {
    this.events.publish('back');
    // this.showAlert();
  }

  async closeModal() {
    const modal = await this.modalCtrl.getTop();
    const alert = await this.alertCtrl.getTop();
    const popover = await this.popoverCtrl.getTop();

    if (modal) {
      modal.dismiss();
    }
    if (alert) {
      alert.dismiss();
    }
    if (popover) {
      popover.dismiss();
    }

  }

  async showAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Thông báo!',
      message: this.global.fireBase_token,
      buttons: [
        {
          text: 'Thoát',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }
      ]
    });

    await alert.present();
  }
}

