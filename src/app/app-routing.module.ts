import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'detail', loadChildren: './detail/detail.module#DetailPageModule' },
  { path: 'search', loadChildren: './search/search.module#SearchPageModule' },
  { path: 'category', loadChildren: './category/category.module#CategoryPageModule' },
  { path: 'video', loadChildren: './video/video.module#VideoPageModule' },
  { path: 'market/:id', loadChildren: './market/market/market.module#MarketPageModule' },
  { path: 'notifications', loadChildren: './notifications/notifications.module#NotificationsPageModule' },
  { path: 'settings', loadChildren: './settings/settings.module#SettingsPageModule' },
  { path: 'comment', loadChildren: './comment/comment.module#CommentPageModule' },
  { path: 'category-posts/:id', loadChildren: './category-posts/category-posts.module#CategoryPostsPageModule' },
  { path: 'login', loadChildren: './personal/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './personal/register/register.module#RegisterPageModule' },
  { path: 'forgot-password', loadChildren: './personal/forgot-password/forgot-password.module#ForgotPasswordPageModule' },
  { path: 'market-detail', loadChildren: './market/market-detail/market-detail.module#MarketDetailPageModule' },
  { path: 'market-add', loadChildren: './market/market-add/market-add.module#MarketAddPageModule' },
  { path: 'help', loadChildren: './help/help/help.module#HelpPageModule' },
  { path: 'help-detail', loadChildren: './help/help-detail/help-detail.module#HelpDetailPageModule' },
  { path: 'help-add', loadChildren: './help/help-add/help-add.module#HelpAddPageModule' },
  { path: 'dropdownlist', loadChildren: './dropdownlist/dropdownlist.module#DropdownlistPageModule' },
  { path: 'market-category', loadChildren: './market/market-category/market-category.module#MarketCategoryPageModule' },  { path: 'update-profile', loadChildren: './personal/update-profile/update-profile.module#UpdateProfilePageModule' },
  { path: 'video-add', loadChildren: './video/video-add/video-add.module#VideoAddPageModule' },
  { path: 'home-menu', loadChildren: './home-menu/home-menu.module#HomeMenuPageModule' },
  { path: 'video-detail', loadChildren: './video/video-detail/video-detail.module#VideoDetailPageModule' },
  { path: 'popup-selecter', loadChildren: './popup-selecter/popup-selecter.module#PopupSelecterPageModule' },
  { path: 'popup-notification', loadChildren: './popup-notification/popup-notification.module#PopupNotificationPageModule' },
  { path: 'notification', loadChildren: './notification/notification.module#NotificationPageModule' }





];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
