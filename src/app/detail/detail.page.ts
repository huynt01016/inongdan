import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ApiProvider } from '../../providers/api/api';
import { Global } from '../../providers/appbase/appbase';
import { ModalController, NavParams, AlertController, ToastController, ActionSheetController, NavController } from '@ionic/angular';

import { UltilityProvider } from '../../providers/ultility/ultility';
import * as Enums from '../../providers/appbase/appbase';

import { CommentPage } from '../comment/comment.page';
import { LoginPage } from '../personal/login/login.page';
import * as moment from 'moment';
import { CategoryPostsPage } from '../category-posts/category-posts.page';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})

export class DetailPage {
  commentSucess = false;
  nid = this.navParams.get('nid');
  post: any;
  isHidden: boolean;
  comment: string = "";
  comments: any = [];
  constructor(
    public navCtrl: NavController,
    public actionSheetController: ActionSheetController,
    public apiProvider: ApiProvider,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public global: Global,
    public ultility: UltilityProvider,
    public sanitizer: DomSanitizer,
    public alertController: AlertController,
    public toastController: ToastController,
  ) {
    this.post = {
      cat_id: 1,
      category_name: "",
      nid: 1,
      news_title: "",
      news_image: "https://wallpapercave.com/wp/Hvtxo6m.jpg",
      news_date: "",
      video_id: "",
      video_url: "",
      news_description: "",
      content_type: "",
      comments_count: 0
    };
    this.comments = [];
    this.isHidden = true;
    this.getPostDetail(this.nid);
    this.getComment();
  }
  ionViewWillLeave() {
    console.log('detail ionViewWillLeave');
  }

  close() {
    this.modalCtrl.dismiss({
      'dismissed': true,
      'commentSucess': this.commentSucess,
    });
  }

  getPostDetail(nid) {
    this.apiProvider.getPostDetail(nid)
      .then(data => {
        // console.log(JSON.stringify(data));
        this.post = data.post[0];
        this.post.title = this.post.news_title;
        this.post.date = this.ultility.convertToDate(this.post.news_date, true);
        this.post.news_description = this.post.news_description;

        console.log("description: " + this.post.news_description);

        if (this.post.content_type == "VIDEO") {
          this.post.video = true;
          this.post.video_url = this.post.news_description.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");

          this.post.news_description = '';
        }
        else if (this.post.news_description.includes('youtube')) {

          var removePTag = this.post.news_description.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
          var httpIndexOf = removePTag.indexOf('youtube');
          if (httpIndexOf < 0) {
            this.post.video = false;
          }
          else {
            this.post.video = true;
            var url = removePTag.substring(httpIndexOf, removePTag.indexOf('"', httpIndexOf + 1));
            this.post.video_url = 'https://www.' + url;
          }

          console.log("youtube: " + url);
        }
        else
          this.post.video = false;

        this.post.news_description = this.post.news_description
          .replace(new RegExp(`http://inongdan.vn/upload/`, 'g'), `/upload/`)
          .replace(new RegExp(`/upload/`, 'g'), `http://inongdan.vn/upload/`)

        console.log("description: " + this.post.news_description);
      }).catch(err => {

      });

  }

  getComment() {
    console.log("getComment " + this.nid);
    this.apiProvider.getComment(this.nid)
      .then(data => {
        console.log(JSON.stringify(data))
        this.comments = data.comments;
        let cmtCount = this.comments.length;
        this.post.comments_count = cmtCount;
        if (cmtCount <= 0) {
          this.comments.push({
            message: "Hiện tại chưa có bình luận cho bài viết này",
            avatar: "https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1",
            user_name: "Quản trị viên",
            full_name: "Quản trị viên",
            created: "",
            date: moment().format('D MMMM, YYYY'),
            user: 0,
            isMe: false
          });
        }
        for (let i = 0; i < cmtCount; i++) {
          if (!this.comments[i].avatar || this.comments[i].avatar.length < 1)
            this.comments[i].avatar = "https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1";
          this.comments[i].date = this.ultility.convertToDate(this.comments[i].created);

          if (this.global.userInfo && this.comments[i].user == this.global.userInfo.user_id)
            this.comments[i].isMe = true;
          else
            this.comments[i].isMe = false;
        }
      }).catch(err => {
      });
  }

  trustUrl(dangerousUrl) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(dangerousUrl);
  }

  sendComment() {
    if (this.global.userInfo && this.global.userInfo.user_id) {
      if (this.comment == undefined || this.comment.trim().length <= 0){     
        this.ultility.presentToast('Vui lòng nhập nội dung bình luận.', 'w');
        return;
      }
      else {
        this.apiProvider.addComment(this.nid, this.global.userInfo.user_id, this.comment.trim())
        .then(data => {
          if (data.status == "ok") {
            this.ultility.presentToast('Bình luận thành công', 's');
            this.comment = "";
            this.getComment();
          }
          else {
            this.ultility.presentToast('Bình luận thất bại', 'w');
          }
        }).catch(err => {
          this.ultility.presentToast('Bình luận thất bại', 'w');
        });
      }

    }
    else {
      this.showAlertLogin();
    }
  }

  deleteComment(comid: string) {
    this.apiProvider.deleteComment(comid)
      .then(data => {
        if (data.status == "ok") {
          this.ultility.presentToast('Xoá bình luận thành công.', 's');
          this.getPostDetail(this.nid);
          this.getComment();
          this.commentSucess = true;
        }
        else {
          this.ultility.presentToast('Xoá bình luận thất bại.', 'w');
        }
      }).catch(err => {

      });
  }

  editComment(comid: string, message: string) {
    if (message == undefined || message.trim().length <= 0)
    {
      this.ultility.presentToast('Vui lòng nhập bình luận.', 'w');
    }
    else {
      this.apiProvider.editComment(comid, message)
        .then(data => {
          if (data.status == "ok") {  
            this.ultility.presentToast('Sửa bình luận thành công.', 's');
            this.getComment();
          }
          else {
            this.ultility.presentToast('Sửa bình luận thất bại.', 'w');
          }
        }).catch(err => {

        });
    }
  }

  async showAlertLogin() {

    const toast = await this.toastController.create({
      message: 'Đăng nhập để bình luận?',
      duration: 2000,
      position: 'bottom',
      cssClass: 'mihn-toast-wb',
      buttons: [
        {
          text: 'Đăng nhập',
          handler: () => {
            this.showLogin();
          }
        }
      ]
    });
    toast.present();
  }

  async showLogin() {
    const modal = await this.modalCtrl.create({
      component: LoginPage,
    });
    return await modal.present();
  }

  async showMore(comid: string, message: string) {

    const actionSheet = await this.actionSheetController.create({
      buttons: [{
        text: 'Xoá',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.showAlertDelete(comid);
        }
      }, {
        text: 'Sửa',
        icon: 'create',
        handler: () => {
          this.showAlertEdit(comid, message);
        }
      }, {
        text: 'Huỷ',
        icon: 'close',
        role: 'cancel',
        handler: () => {
        }
      }]
    });
    await actionSheet.present();
  }

  async showAlertDelete(comid: string) {
    const alert = await this.alertController.create({
      header: 'Thông báo!',
      message: 'Bạn có muốn xoá bình luận này?',
      buttons: [
        {
          text: 'Thoát',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Đồng ý',
          handler: () => {
            this.deleteComment(comid);
          }
        }
      ]
    });

    await alert.present();
  }

  async showAlertEdit(comid: string, message: string) {
    const alert = await this.alertController.create({
      header: 'Sửa bình luận',
      inputs: [
        {
          name: 'message',
          placeholder: 'Nhập bình luận',
          value: message,
        },
      ],
      buttons: [
        {
          text: 'Thoát',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Đồng ý',
          handler: (data) => {
            this.editComment(comid, data.message);
          }
        }
      ]
    });

    await alert.present();
  }

  async showCategoryPost(cid) {
    if (this.global.current_cat_id == -1) {
      const modal = await this.modalCtrl.create({
        component: CategoryPostsPage,
        componentProps: {
          'cid': cid,
        }
      });
      return await modal.present();
    }
    else {
      this.close();
    }
  }
}
