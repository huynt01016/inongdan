import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpDetailPage } from './help-detail.page';

describe('HelpDetailPage', () => {
  let component: HelpDetailPage;
  let fixture: ComponentFixture<HelpDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
