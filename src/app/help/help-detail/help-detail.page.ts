import { Component } from '@angular/core';
import { ModalController, NavParams, AlertController } from '@ionic/angular';
import { Global, AppBase } from '../../../providers/appbase/appbase';
import { ApiProvider } from '../../../providers/api/api';
import { UltilityProvider } from '../../../providers/ultility/ultility';
import { HelpAddPage } from '../help-add/help-add.page';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-help-detail',
  templateUrl: './help-detail.page.html',
  styleUrls: ['./help-detail.page.scss'],
})
export class HelpDetailPage {
  isMe: boolean;
  data = this.navParams.get('data');
  isReload: boolean = false;
  constructor(
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public global: Global,
    public ultility: UltilityProvider,
    public alertController: AlertController,
    public apiProvider: ApiProvider,
    public photoViewer: PhotoViewer
  ) {
    console.log("DATA: " + JSON.stringify(this.data))
    switch (this.data.status) {
      case "9":
        this.data.statusName = "Đã hủy"
        break;
      case "6":
        this.data.statusName = "Tiếp nhận"
        break;
      case "5":
        this.data.statusName = "Chuyên gia không trả lời"
        break;
      case "4":
        this.data.statusName = "Chuyên gia đã trả lời"
        break;
      case "3":
        this.data.statusName = "Chờ chuyên gia"
        break;
      case "2":
        this.data.statusName = "Tạm khóa"
        break;
      case "1":
        this.data.statusName = "Đã trả lời"
        break;
      case "0":
        this.data.statusName = "Chưa trả lời"
        break;
      default:
        this.data.statusName = "Chờ"
    }

    if (this.data.answer == "" || this.data.answer == undefined)
      this.data.answer = `<p><em>Chào ${this.data.display_name || 'Ẩn danh'}</em></p><p>Chuyên gia của chúng tôi sẽ phản hồi bạn trong thời gian sớm nhất. Xin lỗi vì sự chậm trễ này.</>`;

    if (this.global.isLogin && this.data.creator == this.global.userInfo.phone)
      this.isMe = true;
    this.data.name = this.ultility.getfistlastname(this.data.display_name, 15);

    
    var answer = this.data.answer.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
    var beginIndex = answer.indexOf('src=');
    var endIndex = answer.indexOf('/>');
    var img = answer.substring(beginIndex + 4, endIndex).replace('"','').replace('"','');
    console.log(img);
    this.data.validAttachment = this.data.attachment && !this.data.attachment.includes("null");
    
    this.data.answerAttachment = null;
    if(img != null && img != '' && (img.includes('.png') || img.includes('.jpg'))){
      this.data.answerAttachment = AppBase.Host + img;
    }
  }

  close() {
    this.modalCtrl.dismiss({
      'dismissed': true,
      'isReload': this.isReload,
    }, '', 'HelpDetailPage');
  }

  deleteAnswer() {
    this.apiProvider.deleteAnswer(this.data.ans_id)
      .then(data => {
        // console.log(JSON.stringify(data));
        if (data.status == "ok") {
          this.ultility.presentNoti({
            title:'Thông báo hệ thống',
            content:"Xóa câu hỏi thành công."
          });
          this.isReload = true;
        }
        else {
          this.ultility.presentNoti({
            title:'Thông báo hệ thống',
            content:"Xóa câu hỏi thất bại."
          });
        }
        this.close();
      }).catch(err => {
        this.ultility.presentNoti({
          title:'Thông báo hệ thống',
          content:"Xóa câu hỏi thất bại."
        });
        this.close();
      });
  }


  async showAlertDelete(comid: string) {
    const alert = await this.alertController.create({
      header: 'Thông báo!',
      message: 'Bạn có muốn xoá câu hỏi này?',
      buttons: [
        {
          text: 'Thoát',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Đồng ý',
          handler: () => {
            this.deleteAnswer();
          }
        }
      ]
    });

    await alert.present();
  }

  async editAnswer() {
    const modal = await this.modalCtrl.create({
      component: HelpAddPage,
      componentProps: {
        'isAdd': false,
        'data': this.data,
      }
    });

    var me = this;
    modal.onDidDismiss().then((detail) => {
      console.log(JSON.stringify(detail.data));
      if (detail !== null && detail.data.isReload == true) {
        console.log("HERE");
        me.isReload = true;
        if(detail.data.result){
          me.data.title = detail.data.result.title;
          me.data.province = detail.data.result.province;
          me.data.province_name = detail.data.result.province;
          me.data.province_id = detail.data.result.province_id;
          me.data.category = detail.data.result.category;
          me.data.cat_name = detail.data.result.category;
          me.data.category_id = detail.data.result.category_id;
          me.data.question = detail.data.result.question;
          me.data.ans_id = detail.data.result.ans_id;
          if(detail.data.result.attachment != "")
            me.data.attachment = detail.data.result.attachment;
        }
        // me.close();
      }
    });

    return await modal.present();
  }
  
  photoClick(url, title){
    this.photoViewer.show(url, title);
  }
  handleErrorImage(ev: any, data: any, key: string){
    if(key == 'attachment')
      data.validAttachment = false;
    data[key] = null;
  }
}
