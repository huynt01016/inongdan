import { Component, ViewChild } from '@angular/core';
import { ModalController, NavParams, ActionSheetController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { ApiProvider } from 'src/providers/api/api';
import { Global } from 'src/providers/appbase/appbase';
import { UltilityProvider } from 'src/providers/ultility/ultility';

import { PopoverController } from '@ionic/angular';
import { DropdownlistPage } from '../../dropdownlist/dropdownlist.page';
import { PopupSelecterPage } from 'src/app/popup-selecter/popup-selecter.page';

@Component({
  selector: 'app-help-add',
  templateUrl: './help-add.page.html',
  styleUrls: ['./help-add.page.scss'],
})


export class HelpAddPage {
  @ViewChild(NgForm) form: NgForm;

  isAdd = this.navParams.get('isAdd');
  
  isReload: boolean = false;
  market_types: Array<any>;
  provinces: Array<any>;
  categories: Array<any>;

  item: any = {};
  imageUri: any = [];
  defaultImage: any = [];

  constructor(
    public apiProvider: ApiProvider,
    public global: Global,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public ultility: UltilityProvider,
    public actionSheetController: ActionSheetController,
  ) {
    this.getProvince();
    this.getCategoryAnswer();
    
    let data = this.navParams.get('data') || {};
    console.log(JSON.stringify(data));

    this.item = {
      title: data.title || '',
      province_id: data.province_id || 361,
      province: data.province_name || "Toàn Quốc",
      category: data.cat_name || '',
      category_id: data.category_id || '',
      question: data.question || '',
      ans_id: data.ans_id || 0,
      attachment: data.attachment
    };
    
    if (data.attachment && !data.attachment.includes("null")) {
      this.defaultImage.push(data.attachment);
    }

  }


  close() {
    this.modalCtrl.dismiss({
      'dismissed': true,
      'isReload': this.isReload,
      'result': this.item
    });
  }

  getProvince() {
    this.apiProvider.getProvince()
      .then(data => {
        this.provinces = data.provinces;
      }).catch(err => {

      });
  }

  getCategoryAnswer() {
    this.apiProvider.getCategoryAnswer()
      .then(data => {
        // console.log(JSON.stringify(data));
        this.categories = data.categories;
      }).catch(err => {

      });
  }

  addHelp() {
    if (this.item.category_id == "") {
      this.ultility.presentNoti({
        title:'Thông báo hệ thống',
        content:"Vui lòng chọn 'Chủ đề'."
      });
      return;
    }
    if (this.item.province_id == "") {
      
      this.ultility.presentNoti({
        title:'Thông báo hệ thống',
        content:"Vui lòng chọn 'Địa chỉ'."
      });
      return;
    }
    if (this.item.title == "") {
      
      this.ultility.presentNoti({
        title:'Thông báo hệ thống',
        content:"Vui lòng chọn 'Tiêu đề'."
      });
      return;
    }
    if (this.item.question == "") {
      
      this.ultility.presentNoti({
        title:'Thông báo hệ thống',
        content:"Vui lòng chọn 'Nội dung câu hỏi'."
      });
      return;
    }

    if (this.isAdd) {
      this.apiProvider.addAnswer(
        this.item.title, this.item.question, 
        this.global.userInfo.phone, 
        this.global.userInfo.email, this.global.userInfo.phone, 
        this.item.province_id, this.item.category_id,
        this.imageUri.length > 0 ? 'img.png' : '', 
        this.imageUri[0] || "").then(data => {
        if (data.status == "ok") {
          
          this.ultility.presentNoti({
            title:'Thông báo hệ thống',
            content:"Thêm câu hỏi thành công."
          });
          this.isReload = true;
        }
        else {
          this.ultility.presentNoti({
            title:'Thông báo hệ thống',
            content:"Thêm câu hỏi thất bại."
          });
        }
        this.close();
      }).catch(err => {
        this.ultility.presentNoti({
          title:'Thông báo hệ thống',
          content:"Thêm câu hỏi thất bại."
        });
        this.close();
      });
    }
    else {
      this.apiProvider.editAnswer(this.item.title, this.item.question, this.item.province_id, this.item.category_id, this.item.ans_id,
        'img.png',
        this.imageUri[0] || "").then(data => {       
        if (data.status == "ok") {
          this.ultility.presentNoti({
            title:'Thông báo hệ thống',
            content:"Sửa câu hỏi thành công."
          });
          if(this.imageUri.length > 0){
            this.item.attachment = `data:image/jpeg;base64,${this.imageUri[0]}`;
          }
          else
            this.item.attachment = "";

          this.isReload = true;
        }
        else {
          this.ultility.presentNoti({
            title:'Thông báo hệ thống',
            content:"Sửa câu hỏi thất bại."
          });
        }
        this.close();
      }).catch(err => {
        this.ultility.presentNoti({
          title:'Thông báo hệ thống',
          content:"Sửa câu hỏi thất bại."
        });
        this.close();
      });
    }
  }

  async onSelectCategory(){
      var allItems: any = [];
    
      for(var i = 0; i < this.categories.length; i++){
        var r = this.categories[i];
        var v = {
          id: r.category_id,
          name: this.ultility.capitalizeFirstLetter(r.category_name),
        };
        allItems.push(v);
      }
  
      const rowModal = await this.modalCtrl.create({
        component: PopupSelecterPage,
        id: 'popup-selecter',
        cssClass: 'popup-selecter',
        componentProps: { 
          placeholder: 'Nhập chủ đề',
          allItems:allItems,
        }
      });
      await rowModal.present();
      var selected = await rowModal.onDidDismiss();
      if(selected && selected.data && selected.data.selectedItem != null){
        this.item.category_id = selected.data.selectedItem.id;
        this.item.category= selected.data.selectedItem.name;
      }
  }
  async onSelectProvince(){
    var allItems: any = [];
    
      for(var i = 0; i < this.provinces.length; i++){
        var r = this.provinces[i];
        var v = {
          id: r.province_id,
          name: this.ultility.capitalizeFirstLetter(r.name),
        };
        allItems.push(v);
      }
  
      const rowModal = await this.modalCtrl.create({
        component: PopupSelecterPage,
        id: 'popup-selecter',
        cssClass: 'popup-selecter',
        componentProps: { 
          placeholder: 'Nhập địa điểm',
          allItems:allItems,
        }
      });
      await rowModal.present();
      var selected = await rowModal.onDidDismiss();
      if(selected && selected.data && selected.data.selectedItem != null){
        this.item.province_id = selected.data.selectedItem.id;
        this.item.province = selected.data.selectedItem.name;
      }
  }
  async showGetImg(isShow) {
    if(!isShow){
      return;
    }
    const actionSheet = await this.actionSheetController.create({
      buttons: [{
        text: 'Camera',
        role: 'destructive',
        icon: 'camera',
        handler: () => {
          this.captureAndUpload(1);
        }
      }, {
        text: 'Thư viện',
        icon: 'image',
        handler: () => {
          this.captureAndUpload(0);
        }
      }, {
        text: 'Huỷ',
        icon: 'close',
        role: 'cancel',
        handler: () => {
        }
      }]
    });
    await actionSheet.present();
  }

  async captureAndUpload(sourceType: number) { 
    let me = this;
    await this.ultility.captureAndUploadBase64(sourceType, 
      (data)=>{
        if(data){
          if (me.imageUri.length == 0)
            me.defaultImage = [];
          me.defaultImage.push(data);
          me.imageUri.push(data.replace('data:image/jpeg;base64,', ''));
        }
      }, 
      (err)=>{
        if(this.global.fireBase_token == ''){
          let imageData = this.ultility.getImageQuestionDefault();
          var defaultImage = 'data:image/jpeg;base64,' + imageData;
          this.ultility.createThumbnail(defaultImage, (data)=>{       
            me.defaultImage.push(data);
            me.imageUri.push(data.replace('data:image/jpeg;base64,', ''));
          });
        }
      });
  }
  clear() {
    setTimeout(() => {
      this.defaultImage.pop();
      this.imageUri.pop();
    }, 100);
  }
}
