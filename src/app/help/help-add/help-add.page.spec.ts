import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpAddPage } from './help-add.page';

describe('HelpAddPage', () => {
  let component: HelpAddPage;
  let fixture: ComponentFixture<HelpAddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpAddPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpAddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
