import { Component } from '@angular/core';
import { ApiProvider } from '../../../providers/api/api';
import * as Enums from '../../../providers/appbase/appbase';
import { UltilityProvider } from '../../../providers/ultility/ultility';
import { ModalController, ToastController, AlertController } from '@ionic/angular';
import { HelpDetailPage } from '../help-detail/help-detail.page';
import { HelpAddPage } from '../help-add/help-add.page';
import { LoginPage } from 'src/app/personal/login/login.page';
import { SearchPage } from '../../search/search.page';
import { Global } from '../../../providers/appbase/appbase';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-help',
  templateUrl: './help.page.html',
  styleUrls: ['./help.page.scss'],
})
export class HelpPage {
  get_duration_interval: any;
  deltaTime: 0;

  tab: string;
  offlineMode: number = 0;
  text_noconnection: string = "";
  currentPage: number = 1;
  answers: Array<any>;
  reloads: Array<any>;
  offlineMode1: number = 0;
  text_noconnection1: string = "";
  currentPage1: number = 1;
  answer_user: Array<any>;
  constructor(
    public apiProvider: ApiProvider,
    public ultility: UltilityProvider,
    public modalCtrl: ModalController,
    public toastController: ToastController,
    public alertController: AlertController,
    public global: Global,
    public storage: Storage) {
    this.answers = [];
    this.answer_user = [];
    this.getAnswer();
    this.getAnswerByUser();
    this.tab = "1";

  }

  ionViewWillEnter() {
    this.getAnswerByUser();
    this.get_duration_interval = setInterval(() => { 
      this.getAnswer(undefined, true);
    }, 5000);
  }
  ionViewWillLeave(){
    clearInterval(this.get_duration_interval);
  }


  getAnswer(infiniteScroll: any = undefined, autoReload: boolean = false) {
    
    var pageNum = this.currentPage;
    var count = 10;
    if(autoReload){
      this.currentPage--;
      if(this.currentPage <= 0)
        this.currentPage = 1;
      pageNum = 1;
      count = this.currentPage * 10;
      console.log("help -> autoReload at: " + pageNum + " count: " + count)
    }
    this.apiProvider.getAnswer(pageNum, count)
      .then(data => {
        if (!data || !data.answers || data.answers.length <= 0) {
          console.log('getAnswer -> data is empty');
          if (this.currentPage == 1) {
            this.text_noconnection = "CHƯA CÓ BÀI VIẾT";
            this.offlineMode = Enums.OfflineMode.NO_DATA;
          }
        }
        else {
          this.offlineMode = Enums.OfflineMode.NORMAL;
          
          if(autoReload){
            this.reloadData(data.answers, count);
          }
          else{
            for (let index = 0; index < data.answers.length; index++) {
              var r = this.convertAnswerData(data.answers[index]);
              if(r.status == 1)
                this.answers.push(data.answers[index]);
            }
          }
          this.currentPage++;
        }
      }).catch(err => {
        if (this.answers.length <= 0) {
          this.text_noconnection = "KHÔNG TẢI ĐƯỢC DỮ LIỆU";
          this.offlineMode = Enums.OfflineMode.NO_INTERNET;
        }
      });
      
      if (infiniteScroll != undefined)
        infiniteScroll.complete();
  }

  getAnswerByUser(infiniteScroll: any = undefined) {
    if (this.global.isLogin == false)
      return;

    this.apiProvider.getAnswerByUser(this.global.userInfo.phone, this.currentPage1, 10)
      .then(data => {
        // data = null; test for no-data
        if (!data || !data.answers || data.answers.length <= 0) {
          console.log('getAnswerByUser -> data is empty');
          if (this.currentPage1 == 1) {
            this.text_noconnection1 = "CHƯA CÓ BÀI VIẾT";
            this.offlineMode1 = Enums.OfflineMode.NO_DATA;
          }
        }
        else {
          // console.log(JSON.stringify(data));
          this.offlineMode1 = Enums.OfflineMode.NORMAL;
          if (this.currentPage1 == 1) {
            this.answer_user = [];
          }
          
          for (let index = 0; index < data.answers.length; index++) {
            var r = this.convertAnswerData(data.answers[index]);
            this.answer_user.push(r);
          }
          this.currentPage1++;
        }

        if (infiniteScroll != undefined)
          infiniteScroll.complete();
      }).catch(err => {
        if (this.answer_user.length <= 0) {
          this.text_noconnection1 = "KHÔNG TẢI ĐƯỢC DỮ LIỆU";
          this.offlineMode1 = Enums.OfflineMode.NO_INTERNET;
          if (infiniteScroll != undefined)
            infiniteScroll.complete();
        }
      });
  }
  reloadData(answers, count){
    var updated = false;
    this.reloads = [];
    for (let index = 0; index < answers.length; index++) {
      var r = this.convertAnswerData(answers[index]);
      if(r.status == 1){
        if(this.reloads.length == 0){
          if(this.answers.length == 0)
            updated = true;
          else{
            var oldFirst = this.answers[0];
            if(oldFirst.ans_id != r.ans_id){
              updated =true;
            }
            else
              break;
          }
        }
        this.reloads.push(answers[index]);
      }
    }
    if(updated && this.reloads.length >= count)
      this.answers = this.reloads;
  }
  convertAnswerData(answer){
    var result = answer;
    result.date = this.ultility.convertToDate(result.created, false);
    result.title = this.ultility.cropString(result.title, " ", 100, 100);
    if (result.full_name == undefined || result.full_name == "")
      result.display_name = result.creator;
    else
      result.display_name = result.full_name;

    let status = result.status;
    result.div_status = status == 0 ? "div-status-yellow" : status == 1 ? "div-status-green" : "div-status-gray";
    result.text_status = status == 0 ? "Chưa trả lời" : status == 1 ? "Đã trả lời" : "";

    if (result.attachment && !result.attachment.includes(null))
      result.image = result.attachment;
    else
      result.image = '../../assets/question-img.png';

    return result;
  }

  doInfinite(event: any) {
    console.log("doinfinite")
    this.getAnswer(event.target);
  }

  doInfinite2(event: any) {
    console.log("doinfinite2")
    this.getAnswerByUser(event.target);
  }

  async showAnswerDetail(data: any) {
    const modal = await this.modalCtrl.create({
      component: HelpDetailPage,
      componentProps: {
        'data': data,
      },
      id: 'HelpDetailPage'
    });

    let me = this;
    modal.onDidDismiss().then((detail) => {
      // console.log(JSON.stringify(detail.data));
      if (detail !== null && detail.data.isReload == true) {
        me.currentPage = 1;
        me.currentPage1 = 1;
        me.answers = [];
        me.answer_user = [];
        me.getAnswer();
        me.getAnswerByUser();
      }
    });

    return await modal.present();
  }

  async showHelpAdd() {
    // if (this.global.userInfo) {
    const modal = await this.modalCtrl.create({
      component: HelpAddPage,
      componentProps: {
        'isAdd': true,
      }
    });

    modal.onDidDismiss().then((detail) => {
      if (detail !== null && detail.data.isReload == true) {
        this.currentPage = 1;
        this.currentPage1 = 1;
        this.answers = [];
        this.answer_user = [];
        this.getAnswer();
        this.getAnswerByUser();
      }
    });

    return await modal.present();
    // }
    // else {
    //   this.showAlertLogin();
    // }
  }

  async showAlertLogin() {

    const toast = await this.toastController.create({
      message: 'Đăng nhập để thêm câu hỏi?',
      duration: 2000,
      position: 'top',
      color: 'dark',
      buttons: [
        {
          text: 'Bỏ qua',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Đăng nhập',
          handler: () => {
            this.showLogin();
          }
        }
      ]
    });
    toast.present();
  }

  async showLogin() {
    const alert = await this.alertController.create({
      header: 'Đăng nhập',
      message: 'Vui lòng nhập thông tin',
      inputs: [
        {
          name: 'username',
          placeholder: 'Email/Số điện thoại'
        },
        {
          name: 'password',
          placeholder: 'Mật khẩu',
          type: 'password'
        }
      ],
      cssClass: 'mihn-alert',
      buttons: [
        {
          text: 'Bỏ qua',
          cssClass: 'dark',
          handler: (blah) => {
          }
        },
        {
          text: 'Đồng ý',
          cssClass: 'dark',
          handler: (data) => {
            this.ultility.loginWithData(data.username, data.password, (u, p) => {
              this.storage.set('credentials', { username: u, password: p });
              this.getAnswerByUser();
            });
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }

  async showSearch() {
    const modal = await this.modalCtrl.create({
      component: SearchPage,
      componentProps: {
        recentPost: this.answers.length > 7 ? this.answers.slice(0, 7) : this.answers,
        category: 3,
      }
    });
    return await modal.present();
  }
}
