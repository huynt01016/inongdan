import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ApiProvider } from '../../../providers/api/api';
import { UltilityProvider } from '../../../providers/ultility/ultility';
import { ModalController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage {
  username: string;
  password: string;
  isLogin: boolean = false;
  constructor(
    public alertController: AlertController,
    public ultility: UltilityProvider,
    public modalCtrl: ModalController,
    public apiProvider: ApiProvider, ) { }

  close() {
    if (this.isLogin) {
      this.modalCtrl.dismiss({
        'dismissed': true,
        'username': this.username,
        'password': this.password,
      });
    }
    else {
      this.modalCtrl.dismiss({
        'dismissed': true,
      });
    }
  }

  forgotpassword(form: NgForm) {

    this.apiProvider.resetPassword(form.value.username).then(data => {
      console.log(JSON.stringify(data));
      if (data.status == "nok") {
        if (data.message.includes("User") && data.message.includes("exists")) {
          this.ultility.presentToast("'Tài khoản' không tồn tại.", "e");
        }
      }
      else if (data.status == "ok") {
        this.showAlertEdit(form.value.username, data.password);
      }

    }).catch(err => {

    });
  }

  async showAlertEdit(username: string, password: string) {
    const alert = await this.alertController.create({
      header: 'Quên mật khẩu',
      message: "Yêu cầu mật khẩu thành công, Hãy kiểm tra email/sdt đăng ký để xác nhận",
      buttons: [
        {
          text: 'Đồng ý',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            this.close();
          }
        }
      ]
    });

    await alert.present();
  }

  loginWithData(username, password) {
    this.username = username;
    this.password = password
    this.isLogin = true;
    this.close();
  }

}
