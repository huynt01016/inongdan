
  import { Component } from '@angular/core';
  import { ApiProvider } from '../../../providers/api/api';
  import { Global } from '../../../providers/appbase/appbase';
  import { ModalController, AlertController } from '@ionic/angular';
  import { NavParams } from '@ionic/angular';
  import { UltilityProvider } from '../../../providers/ultility/ultility';
  import { Router } from  "@angular/router";
  import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage  {
  
    gender: string = "M";
    result: string = "";
    isShowPassword: boolean = false;
    passwordType: string = "password";

    username: string;
    password: string;
    constructor(
      public apiProvider: ApiProvider,
      public modalCtrl: ModalController,
      public navParams: NavParams,
      public global: Global,
      public ultility: UltilityProvider,
      public router: Router,
      public alertController: AlertController
      ) { 
    }
  
    close(isSuccess: boolean = false) {
      if (isSuccess)
      {
        this.modalCtrl.dismiss({
          'dismissed': true,
          'username': this.username,
          'password': this.password,
        });
      }
      else
      {
        this.modalCtrl.dismiss({
          'dismissed': true,
        });
      }
    }
  
    register(form: NgForm) {
      console.log(JSON.stringify(form.value))
      if (form.value.password !== form.value.confirmPassword)
      {
        console.log(form.value.password, form.value.confirmPassword);
        this.ultility.presentToast("Nhập lại mật khẩu không khớp", "error");
      }
      else
      {
        this.apiProvider.register(form.value.email, form.value.phone, form.value.display_name, form.value.password, "Việt Nam", this.gender, "").then(data=>{
          console.log(JSON.stringify(data));
          if(data.status =="nok")
          {
            this.ultility.presentToast(data.message, "error");
          }
          else
          {
            this.ultility.presentToast("Đăng ký thành công", "success");
            this.username = form.value.email;
            this.password = form.value.password;
            this.close(true);
          }
    
        }).catch(err=>{
          
         });
      }
  
    }
  
    getPrivacyPolicy()
    {
      console.log(this.gender);
      this.apiProvider.getPrivacyPolicy().then(data=>{
        console.log(JSON.stringify(data));
          this.showPostDetail(data.post[0].nid);
      }).catch(err=>{
        
       });
    }

    showHidePassword()
    {
      this.isShowPassword = !this.isShowPassword;
      if (this.isShowPassword)
        this.passwordType = "text";
      else
        this.passwordType = "password";
    }

    onChangeGender(event: any) {
      this.gender = event.detail.value;
    }

    showPostDetail(nid)
    {
      this.apiProvider.getPostDetail(nid)
      .then(data=>{           
        console.log(data.post[0].news_description);
        this.showAlertPrivacy(data.post[0].news_description);
      }).catch(err=>{

      });

    }

    
  async showAlertPrivacy(message: string) {
    const alert = await this.alertController.create({
      header: 'Điều khoản sử dụng',
      message: message,
      buttons: [
        {
          text: 'Thoát',
          role: 'cancel',
        }
      ]
    });

    await alert.present();
  }
    
  }
  