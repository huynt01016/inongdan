import { Component } from '@angular/core';
import { ApiProvider } from '../../../providers/api/api';
import { Global } from '../../../providers/appbase/appbase';
import { NavParams, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

import { UltilityProvider } from '../../../providers/ultility/ultility';
import { Router } from "@angular/router";
import { NgForm } from '@angular/forms';

import { ForgotPasswordPage } from '../forgot-password/forgot-password.page';
import { RegisterPage } from '../register/register.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  result: string = "";
  isShowPassword: boolean = false;
  passwordType: string = "password";
  constructor(
    public apiProvider: ApiProvider,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public global: Global,
    public ultility: UltilityProvider,
    public router: Router,
    private storage: Storage
  ) {
  }

  close() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  login(form: NgForm) {
    this.loginWithData(form.value.username, form.value.password);
  }

  loginWithData(username, password) {
    this.apiProvider.login(username, password).then(data => {
      console.log(JSON.stringify(data.status));
      console.log(JSON.stringify(data.user_info));
      if (data.status == "nok") {
        if (data.message.includes("user") && data.message.includes("password")) {
          this.ultility.presentToast("'Tên đăng nhập' hoặc 'Mật khẩu' không đúng.", "e");
        }
        else if (data.message.includes("User") && data.message.includes("exists")) {
          this.ultility.presentToast("'Tài khoản' không tồn tại.", "e");
        }
        else
          this.ultility.presentToast("Có lỗi xảy ra (" + data.message + ")", "e");
      }
      else if (data.status == "ok") {
        // console.log(JSON.stringify(data.user_info[0]));
        // set a key/value
        this.storage.set('credentials', { username: username, password: password });

        this.global.isLogin = true;
        this.global.userInfo = data.user_info[0];
        this.ultility.presentToast("Đăng nhập thành công tài khoản " + this.global.userInfo.full_name, "s");
        if (this.global.userInfo.avatar && this.global.userInfo.avatar.length > 1)
          this.global.avatar = this.global.avalink + this.global.userInfo.avatar;
        this.close();
      }

    }).catch(err => {

    });
  }

  showHidePassword() {
    this.isShowPassword = !this.isShowPassword;
    if (this.isShowPassword)
      this.passwordType = "text";
    else
      this.passwordType = "password";
  }

  async showForgotPassword() {
    const modal = await this.modalCtrl.create({
      component: ForgotPasswordPage,
    });

    modal.onDidDismiss().then((detail) => {
      if (detail !== null && detail.data.username !== undefined && detail.data.password !== undefined) {
        console.log('The result:', detail.data);
        this.loginWithData(detail.data.username, detail.data.password);
      }
    });

    return await modal.present();
  }

  async showRegister() {
    const modal = await this.modalCtrl.create({
      component: RegisterPage,
    });

    modal.onDidDismiss().then((detail) => {
      if (detail !== null && detail.data.username !== undefined && detail.data.password !== undefined) {
        console.log('The result:', detail.data);
        this.loginWithData(detail.data.username, detail.data.password);
      }
    });
    return await modal.present();
  }
}
