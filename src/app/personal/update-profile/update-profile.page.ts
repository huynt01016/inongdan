import { Component, ViewChild } from '@angular/core';
import { ApiProvider } from '../../../providers/api/api';
import { Global } from '../../../providers/appbase/appbase';
import { NavParams, ModalController, ActionSheetController, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { UltilityProvider } from '../../../providers/ultility/ultility';
import { Router } from "@angular/router";
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.page.html',
  styleUrls: ['./update-profile.page.scss'],
})
export class UpdateProfilePage {
  @ViewChild(NgForm) form: NgForm;

  result: string = "";
  isShowPassword: boolean = false;
  passwordType: string = "password";
  defaultImage: string = "";
  imageUri: string = "";

  display_name: string = "";
  phone: string = "";
  email: string = "";
  address: string = "";
  birth_date: string = "";

  constructor(
    public actionSheetController: ActionSheetController,
    public alertController: AlertController,
    public apiProvider: ApiProvider,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public global: Global,
    public ultility: UltilityProvider,
    public router: Router,
    private storage: Storage
  ) {

    if (this.global.userInfo.avatar && this.global.userInfo.avatar.length > 1)
      this.defaultImage = this.global.avatar

    this.display_name = this.global.userInfo.full_name;
    this.email = this.global.userInfo.email;
    this.phone = this.global.userInfo.phone;
    this.address = this.global.userInfo.address;
    this.birth_date = this.global.userInfo.birth_date;

    if(this.phone != ""){
      this.phone = this.phone.substring(0,3) + "*******";
    }
  }

  close() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  showHidePassword() {
    this.isShowPassword = !this.isShowPassword;
    if (this.isShowPassword)
      this.passwordType = "text";
    else
      this.passwordType = "password";
  }

  async showMore(isShow) {
    if (!isShow) {
      return;
    }
    const actionSheet = await this.actionSheetController.create({
      buttons: [{
        text: 'Camera',
        role: 'destructive',
        icon: 'camera',
        handler: () => {
          this.captureAndUpload(1);
        }
      }, {
        text: 'Thư viện',
        icon: 'image',
        handler: () => {
          this.captureAndUpload(0);
        }
      }, {
        text: 'Huỷ',
        icon: 'close',
        role: 'cancel',
        handler: () => {
        }
      }]
    });
    await actionSheet.present();
  }

  async captureAndUpload(sourceType: number) {
    let me = this;
    await this.ultility.captureAndUploadBase64(sourceType, 
      (data)=>{
        if(data){
          me.defaultImage = data;
          me.imageUri = data.replace('data:image/jpeg;base64,', '');
        }
      }, 
      (err)=>{
        if(this.global.fireBase_token == ''){
          let imageData = this.ultility.getImageBase64Sameple();
          var defaultImage = 'data:image/jpeg;base64,' + imageData;
          this.ultility.createThumbnail(defaultImage, (data)=>{       
            me.defaultImage = data;
            me.imageUri = data.replace('data:image/jpeg;base64,', '');
          });
        }
      });
  }


  async showAlertUpdate() {
    const alert = await this.alertController.create({
      header: 'Thông báo!',
      message: 'Bạn có muốn cập nhập thông tin cá nhân?',
      buttons: [
        {
          text: 'Thoát',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Đồng ý',
          handler: () => {
            this.updateProfile();
          }
        }
      ]
    });

    await alert.present();
  }

  updateProfile() {

    if (this.display_name == "") {
      this.ultility.presentToast("Vui lòng nhập 'Tên hiển thị'.", "w");
      return;
    }
    else if (this.email == "") {
      this.ultility.presentToast("Vui lòng nhập 'Email'.", "w");
      return;
    }
    else if (!this.ultility.validateEmail(this.email)) {
      this.ultility.presentToast("'Email' không đúng định dạng.", "w");
      return;
    }
    else if (this.address == "") {
      this.ultility.presentToast("Vui lòng nhập 'Địa chỉ'.", "w");
      return;
    }
    else if (this.birth_date == "" || this.birth_date == undefined) {
      this.ultility.presentToast("Vui lòng nhập 'Ngày sinh'.", "w");
      return;
    }
    console.log(this.birth_date);
    var d = new Date(this.birth_date);
    var birth_date = d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getFullYear();
    console.log(birth_date);
    this.apiProvider.updateProfile(this.global.userInfo.user_name, this.display_name, this.email, this.address, birth_date, "img.png", this.imageUri).then(data => {
      console.log(JSON.stringify(data));
      if (data.status = "ok") {
        this.ultility.presentToast("Cập nhật thông tin thành công", "s");
        if (this.display_name != "")
          this.global.userInfo.full_name = this.display_name;
        if (this.email != "")
          this.global.userInfo.email = this.email;
        if (this.display_name != "")
          this.global.userInfo.address = this.address;
        if (this.birth_date != "")
          this.global.userInfo.birth_date = this.birth_date;
        if (data.path.length >= 1)
          this.global.avatar = this.global.avalink + data.path;
        this.close();
      }
      else {
        this.ultility.presentToast("Cập nhật thông tin thất bại. Vui lòng thử lại.", "e");
        console.log('updateProfile: ' + data.message);
      }

    }).catch(err => {
      this.ultility.presentToast("Cập nhật thông tin thất bại. Vui lòng thử lại.", "e");
      console.log('updateProfile: ' + err);
    });
  }

  clear() {
    setTimeout(() => {
      this.defaultImage = null;
      this.imageUri = null;
    }, 100);
  }
}
