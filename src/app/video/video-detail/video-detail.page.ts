import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent, ModalController, NavParams } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';

import * as moment from 'moment';
import { Global } from 'src/providers/appbase/appbase';
import { UltilityProvider } from 'src/providers/ultility/ultility';
import { ApiProvider } from 'src/providers/api/api';
import { jsonpFactory } from '@angular/http/src/http_module';

@Component({
  selector: 'app-video-detail',
  templateUrl: './video-detail.page.html',
  styleUrls: ['./video-detail.page.scss'],
})
export class VideoDetailPage implements OnInit {
  @ViewChild(IonContent, { }) content: IonContent;
  
  post: any = null;
  currentInfoBtnValue: number = 0;
  comment: string = "";
  comments: any = [];
  isLogin: boolean = false;
  constructor(
    public modalController: ModalController,
    public navParams: NavParams,
    protected sanitizer: DomSanitizer,   
    public global: Global,
    public ultility: UltilityProvider,
    public apiProvider: ApiProvider) {
      
      this.comments = [];
      this.isLogin = this.global.isLogin;

      this.post = this.navParams.get('post') || {};
      this.currentInfoBtnValue = this.navParams.get('currentInfoBtnValue') || {};
      console.log(this.currentInfoBtnValue);
      if(this.currentInfoBtnValue == 1 || this.currentInfoBtnValue == 3){
        this.post.counter_view++;
        this.getCommentV2();
        this.getVideoDetailV2();
      }
      else{
        this.post.counter_view++;
        this.getComment();
        this.getVideoDetail();
      }
  }

  ngOnInit() {
  }

  close(){
    this.modalController.dismiss({
      'dismissed': true,
    }, "", "video-detail");
  }
  getVideoDetail(){
    var nid = this.post.nid || this.post.vid || '-1';
    this.apiProvider.getPostDetail(nid);
  }
  getComment(){
    var nid = this.post.nid || this.post.vid || '-1';
    this.apiProvider.getComment(nid)
      .then(data => {
        console.log(nid + "  " + JSON.stringify(data));
        this.comments = data.comments;
        let cmtCount = this.comments.length;
        if (cmtCount <= 0) {
          this.comments.push({
            message: "Hiện tại chưa có bình luận cho bài viết này",
            avatar: "https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1",
            user_name: "Quản trị viên",
            full_name: "Quản trị viên",
            created: "",
            date: moment().format('D MMMM, YYYY'),
            user: 0,
            isMe: false
          });
        }
        for (let i = 0; i < cmtCount; i++) {
          if (!this.comments[i].avatar || this.comments[i].avatar.length < 1)
            this.comments[i].avatar = "https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1";
          this.comments[i].date = this.ultility.convertToDate(this.comments[i].created);
        }
        this.comments.sort(function(a, b){
          var da = moment(a.created, 'DD/MM/YYYY HH:mm:ss').valueOf();
          var db = moment(b.created, 'DD/MM/YYYY HH:mm:ss').valueOf();
          return db-da;
        });
        this.post.comments_count = this.comments.length;
        
      }).catch(err => {
        this.comments.push({
          message: "Hiện tại chưa có bình luận cho bài viết này",
          avatar: "https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1",
          user_name: "Quản trị viên",
          full_name: "Quản trị viên",
          created: "",
          date: moment().format('D MMMM, YYYY'),
          user: 0,
          isMe: false
        });
      });
  }
  getVideoDetailV2(){
    var nid = this.post.nid || this.post.vid || '-1';
    this.apiProvider.getVideoDetailV2(nid);
  }
  getCommentV2(){
    var nid = this.post.nid || this.post.vid || '-1';
    this.apiProvider.getCommentv2(nid)
      .then(data => {
        console.log(nid + "  " + JSON.stringify(data));
        this.comments = data.comments;
        let cmtCount = this.comments.length;
        if (cmtCount <= 0) {
          this.comments.push({
            message: "Hiện tại chưa có bình luận cho bài viết này",
            avatar: "https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1",
            user_name: "Quản trị viên",
            full_name: "Quản trị viên",
            created: "",
            date: moment().format('D MMMM, YYYY'),
            user: 0,
            isMe: false
          });
        }
        for (let i = 0; i < cmtCount; i++) {
          if (!this.comments[i].avatar || this.comments[i].avatar.length < 1)
            this.comments[i].avatar = "https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1";
          this.comments[i].date = this.ultility.convertToDate(this.comments[i].created);
        }
        this.comments.sort(function(a, b){
          var da = moment(a.created, 'DD/MM/YYYY HH:mm:ss').valueOf();
          var db = moment(b.created, 'DD/MM/YYYY HH:mm:ss').valueOf();
          return db-da;
        });
        
        this.post.comments_count = this.comments.length;
      }).catch(err => {
        this.comments.push({
          message: "Hiện tại chưa có bình luận cho bài viết này",
          avatar: "https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1",
          user_name: "Quản trị viên",
          full_name: "Quản trị viên",
          created: "",
          date: moment().format('D MMMM, YYYY'),
          user: 0,
          isMe: false
        });
      });
  }

  onVideoPlay(id) {
    let listaFrames = document.getElementsByTagName("iframe");
    for (var index = 0; index < listaFrames.length; index++) {
      let vid = listaFrames[index].id;
      let iframe = listaFrames[index].contentWindow;
      if (id == vid){
        this.post.isPlay = true;
        iframe.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
      }
      else{
        iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
      }
    }
    // for (let i = 0; i < this.posts.length; i++) {
    //   if (id == this.posts[i].video_url)
    //     this.posts[i].isPlay = true;
    //   else
    //     this.posts[i].isPlay = false;

    // }
  }

  sendComment() {
    if (this.comment == undefined || this.comment.trim().length <= 0){     
      this.ultility.presentToast('Vui lòng nhập nội dung bình luận.', 'w');
      return;
    }

    var nid = this.post.nid || this.post.vid || '-1';
    this.apiProvider.addComment(nid, this.global.userInfo.user_id, this.comment.trim())
      .then(data => {
        if (data.status == "ok") {
          this.ultility.presentToast('Bình luận thành công', 's');
          this.comment = "";
          this.content.scrollToTop(400);
          this.getComment();
        }
        else {
          this.ultility.presentToast('Bình luận thất bại', 'w');
        }
      }).catch(err => {
        this.ultility.presentToast('Bình luận thất bại', 'w');
      });
  }
  sendCommentV2() {
    if (this.comment == undefined || this.comment.trim().length <= 0){     
      this.ultility.presentToast('Vui lòng nhập nội dung bình luận.', 'w');
      return;
    }


    var nid = this.post.nid || this.post.vid || '-1';
    this.apiProvider.addCommentV2(nid, this.global.userInfo.user_id, this.comment.trim())
      .then(data => {
        console.log(JSON.stringify(data));
        if (data.status == "ok") {   
          this.ultility.presentToast('Bình luận thành công', 's');
          this.comment = "";
          this.content.scrollToTop(400);
          this.getCommentV2();
        }
        else {
          this.ultility.presentToast('Bình luận thất bại', 'w');
        }
      }).catch(err => {

        this.ultility.presentToast('Bình luận thất bại', 'w');
      });
  }
  send(){
    if(this.currentInfoBtnValue == 1 || this.currentInfoBtnValue == 3)
        this.sendCommentV2();
      else
        this.sendComment();
  }
  handleImgCommentError(ev: any, item : any){
    item.avatar = "https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1";
  }
}
