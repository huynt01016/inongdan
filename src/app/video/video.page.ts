import { Component, ViewChild } from '@angular/core';
import { NavController, ModalController, AlertController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { ApiProvider } from '../../providers/api/api';
import { Url } from 'url';
import { Global } from '../../providers/appbase/appbase';

import { DetailPage } from '../detail/detail.page';
import { SearchPage } from '../search/search.page';
import { VideoAddPage } from '../video/video-add/video-add.page';
import { UltilityProvider } from '../../providers/ultility/ultility';
import * as Enums from '../../providers/appbase/appbase';
import * as moment from 'moment';


import { File } from '@ionic-native/file';
import { VideoDetailPage } from './video-detail/video-detail.page';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-tab1',
  templateUrl: 'video.page.html',
  styleUrls: ['video.page.scss']
})
export class VideoPage {
  posts: Array<{
    cat_id: number; 
    category_name: string; 
    nid: number; 
    news_title: string; 
    news_date: string; 
    news_image: Url; 
    news_description: string,
    video_url: string, 
    isPlay: boolean
  }>;

  reloads: Array<{
    cat_id: number; 
    category_name: string; 
    nid: number; 
    news_title: string; 
    news_date: string; 
    news_image: Url; 
    news_description: string,
    video_url: string, 
    isPlay: boolean
  }>;
  test_url: string;
  timeouts: Array<any>;
  
  inforButton: Array<any>;
  currentInfoBtnValue = 0
  get_duration_interval: any;
  currentPage: number = 1;
  postPerPage: number = 3;
  isLogin: boolean = false;
  searchValue: string = '';
  video_categories:Array<any> = [];
  constructor(
    public modalController: ModalController,
    public alertController: AlertController,
    public toastController: ToastController,
    public apiProvider: ApiProvider,
    public navCtrl: NavController,
    public global: Global,
    public ultility: UltilityProvider,
    public sanitizer: DomSanitizer,
    public loadingCtrl: LoadingController,) {
    this.test_url = 'https://inongdanaws3.s3.ap-southeast-1.amazonaws.com/290/1589096884081VID20190803131833.mp4'
 
    if (this.posts == undefined)
      this.posts = [];
    this.currentPage = 1;
    this.getVideoCategory();
    this.getVideoPost();
  }
  async ionViewWillEnter(){

    this.searchValue ='';
    this.isLogin = this.global.userInfo && this.global.userInfo.phone.length > 0;
    this.initHeader();
    this.btnTabClick(null)
  }
  ionViewWillLeave() {
    this.pauseAlliFrame();
    for (let index = 0; index < this.timeouts.length; index++) {
      const element = this.timeouts[index];
      clearTimeout(element);    
    }
    clearInterval(this.get_duration_interval);
  }
  getVideoCategory() {
    this.apiProvider.getCategoryForVideo()
      .then(data => {
        this.video_categories = data.categories;
        console.log("getVideoCategory: " + JSON.stringify(this.video_categories));
      }).catch(err => {

      });
  }
  async getVideoPost(infiniteScroll: any = undefined, autoReload: boolean = false) {
    var pageNum = this.currentPage;
    var count = this.postPerPage;
      
    if (infiniteScroll != undefined){
      infiniteScroll.complete();
    }
    try{
      let data = null;
      if(this.searchValue.length > 0){
        data = await this.apiProvider.getSearchVideo(this.searchValue, pageNum, count)
        if (!data || !data.posts || data.posts.length <= 0) {
          console.log('getVideoPost -> data is empty');
        }
        else {
          this.fillData(data.posts);
        }
      }
      else if(this.currentInfoBtnValue == 0){
        //mới nhất

        data = await this.apiProvider.getVideoV2(pageNum, count);
        if (!data || !data.videos || data.videos.length <= 0) {
          console.log('getVideoV2 -> data is empty');
        }
        else {
          this.fillData(data.videos);
        }
      }
      else if(this.currentInfoBtnValue == 1){
        //nổi bật

        data = await this.apiProvider.getVideoV3(pageNum, count);
        if (!data || !data.videos || data.videos.length <= 0) {
          console.log('getVideoV2 -> data is empty');
        }
        else {
          this.fillData(data.videos);
        }
      }
      else if(this.currentInfoBtnValue == 2){
        //hỏi đáp
        
        data = await this.apiProvider.getVideoPost(pageNum, count);
        if (!data || !data.posts || data.posts.length <= 0) {
          console.log('getVideoPost -> data is empty');
        }
        else {
          this.fillData(data.posts);
        }
      }
      else if(this.currentInfoBtnValue == 3){
        data = await this.apiProvider.getVideoUser(this.global.userInfo.phone, pageNum, count);
        if (!data || !data.videos || data.videos.length <= 0) {
          console.log('getVideoV2 -> data is empty');
        }
        else {
          this.fillData(data.videos);
        }
      }

      
      this.currentPage++;
    }
    catch(err){
      console.log("get video post -> " + this.currentInfoBtnValue + "   " + err);
    }
      
  }
  initHeader(){
    this.inforButton = [
      {
        text: "Mới nhất",
        value: 0
      },
      {
        text: "Nổi bật",
        value: 1
      },
      {
        text: "Hỏi đáp",
        value: 2
      },
    ];
    if(this.isLogin){
      this.inforButton.push(
        {
          text: "Của tôi",
          value: 3
        });
    }
  }
  btnTabClick(btn){
    this.pauseAlliFrame();
    this.currentInfoBtnValue = !btn ? 0 : btn.value;
    this.currentPage = 1;
    this.getVideoPost(undefined);
  }


  fillData(dataPosts){
    this.timeouts = [];
    if(this.currentPage == 1)
      this.posts = [];
    for (let index = 0; index < dataPosts.length; index++) {
      dataPosts[index].date = this.ultility.convertToDate(dataPosts[index].news_date);
      dataPosts[index].title = dataPosts[index].news_title || dataPosts[index].video_title;
      dataPosts[index].summary = dataPosts[index].news_summary || dataPosts[index].video_summary;
      dataPosts[index].isPlay = false;
      dataPosts[index].comments_count = dataPosts[index].comments_count || 0;

      dataPosts[index].isYoutube = dataPosts[index].video_url.includes('youtube');

      if (dataPosts[index].content_type == "VIDEO") {   
        dataPosts[index].isYoutube = true;          
        dataPosts[index].video_url = null;
        let url = dataPosts[index].news_description.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
        url += "?enablejsapi=1&lazy=1&iframe=1";
        dataPosts[index].video_url = url;
      }
      else{
      }
      if(!dataPosts[index].category_image || dataPosts[index].category_image.length <= 0){
        const contains = this.video_categories.filter(v => v.cid == dataPosts[index].cat_id);
        if(contains.length > 0)
          dataPosts[index].category_image = contains[0].category_image;
      }
      if(dataPosts[index].status != 9)
        this.posts.push(dataPosts[index]);
    }
  }

  tryAgain() {
    this.getVideoPost();
  }
  
  doInfinite(event: any) {
    console.log("doinfinite: " + event.target);
    this.getVideoPost(event.target);
  }

  async showDetail(post){
    this.pauseAlliFrame();
    const modal = await this.modalController.create({
      component: VideoDetailPage,   
      id: 'video-detail',
      componentProps: {
        'post': post,
        'currentInfoBtnValue': this.currentInfoBtnValue
      }
    });
    await modal.present();
    
    var selected = await modal.onDidDismiss();
    if(selected && selected.data && selected.data.done){   
      this.currentPage = 1;
      this.getVideoPost(undefined);
    }
  }

  doSearch(event){
    this.pauseAlliFrame();
    this.currentPage = 1;
    this.getVideoPost(undefined);
  }
  async showSearch() {
    const modal = await this.modalController.create({
      component: SearchPage,
      componentProps: {
        recentPost: this.posts.length > 7 ? this.posts.slice(0, 7) : this.posts,
        category: 4,
      }
    });
    await modal.present();

  }

  async showAdd(post){
    this.pauseAlliFrame();
    var p = post || {
      video_url: '',
      title: '',
      summary: '',
      video_category: ''
    }
    var cats = this.video_categories || [
      {"cid":1,"category_name":"Tổng hợp","category_image" : "http://inongdan.vn//upload/category/1598409156071chude.png"},
      {"cid":2,"category_name":"Xã hội","category_image" : "http://inongdan.vn//upload/category/1598409156071chude.png"}
    ];
    const modal = await this.modalController.create({
      component: VideoAddPage,   
      id: 'video-add',
      componentProps: {
        'cats': cats,
        'post': p,
        'isEdit': post != null
      }
    });
    await modal.present();
    var d = await modal.onDidDismiss();
    if(d && d.data){ 
      if(d.data.done){
        this.btnTabClick({
          text: "Của tôi",
          value: 3
        });
      }
    }
  }
  async removeVideo(post){
    this.apiProvider.removeVideo(post.vid)
        .then(async response => {
            // this.ultility.presentToast(fileUrl, "s");
            if (response.status = "ok") {
              this.ultility.presentNoti({
                title:'Xóa video thành công',
                content:"Danh sách video của bạn đã được cập nhật."
              });
              this.currentPage = 1;
              this.getVideoPost(undefined);
            }
            else {
              this.ultility.presentNoti({
                title:'Xóa video thất bại',
                content:"Có sự cố xảy ra. Vui lòng thử lại."
              });
              console.log('editVideo: ' + JSON.stringify(response));
            }
        }).catch(err=>{

        });
  }
  async presentAlertConfirmDelete(post) {
    const alert = await this.alertController.create({
      header: 'Xóa video',
      message: 'Bạn thật sự muốn xóa video này?',
      buttons: [
        {
          text: 'HỦY BỎ',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ĐỒNG Ý',
          handler: async () => {
            await this.removeVideo(post);
          }
        }
      ]
    });

    await alert.present();
  }

  pauseAlliFrame() {
    let listaFrames = document.getElementsByTagName("iframe");
    for (var index = 0; index < listaFrames.length; index++) {
      let iframe = listaFrames[index].contentWindow;
      iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
    }
  }

  

  handleImgError(ev: any, item : any){
    item.news_image = "../../assets/video-default.jpg";
  }
  handleImgCategoryError(ev: any, item : any){
    item.category_image = "../../assets/video-default.jpg";
  }
}
