import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VideoAddPage } from './video-add.page';
import { SafePipeModule } from 'src/app/pipes/safe.pipe.module';

const routes: Routes = [
  {
    path: '',
    component: VideoAddPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SafePipeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [VideoAddPage]
})
export class VideoAddPageModule {}
