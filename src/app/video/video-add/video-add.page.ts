import { Component } from '@angular/core';
import { ModalController, NavParams, ActionSheetController, PopoverController, Platform, LoadingController } from '@ionic/angular';
import { ApiProvider } from '../../../providers/api/api';
import { Global } from '../../../providers/appbase/appbase';

import { UltilityProvider } from '../../../providers/ultility/ultility';
import { PopupSelecterPage } from 'src/app/popup-selecter/popup-selecter.page';
import { File } from '@ionic-native/file';

@Component({
  selector: 'app-video-add',
  templateUrl: './video-add.page.html',
  styleUrls: ['./video-add.page.scss'],
})
export class VideoAddPage {
  cats: any = [];
  post:any = {};
  oldPost: any = {};
  isEdit: boolean = false;
  fileToUpload: any = null;
  selectedCat: any = {};
  provinces: Array<any>;
  constructor(
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public apiProvider: ApiProvider,
    public popoverCtrl: PopoverController,
    public ultility: UltilityProvider,
    public actionSheetController: ActionSheetController,
    public global: Global,
    public loadingCtrl: LoadingController) 
    {
      this.cats = this.navParams.get('cats') || {};
      this.post = this.navParams.get('post') || {};
      this.getProvince();


      this.isEdit = this.navParams.get('isEdit') || false;
      console.log(JSON.stringify(this.post));

      if((!this.post.category_name || this.post.category_name.length <= 1)
          && this.cats.length > 0){
        this.post.category_name = this.cats[0].category_name;
        this.post.cat_id = this.cats[0].cid;
      }
      this.oldPost = {
        title: this.post.title,
        summary: this.post.summary,
        category_name: this.post.category_name,
        cat_id: this.post.cat_id,
        province_id: this.post.province_id,
        province_name: this.post.province_name,
      }
  }

  close(done) {
    if(!done){
      this.post.title = this.oldPost.title;
      this.post.summary = this.oldPost.summary;
      this.post.category_name =  this.oldPost.category_name;
      this.post.cat_id = this.oldPost.cat_id;
      this.post.province_id = this.oldPost.province_id;
      this.post.province_name = this.oldPost.province_name;
    }
    this.modalCtrl.dismiss({
      'dismissed': true,
      'done': done
    },"", "video-add");
  }

  getProvince() {
    this.apiProvider.getProvince()
      .then(data => {
        // console.log(JSON.stringify(data));
        this.provinces = data.provinces;
        if((!this.post.province_name || this.post.province_name.length <= 1)
            && this.provinces.length > 0){
          this.post.province_name = this.provinces[0].name;
          this.post.province_id = this.provinces[0].province_id;
        }
      }).catch(err => {

      });
  }


  async showMenu(){
    var allItems: any = [];
    var selectedItem: any = null;
    
    for(var i = 0; i < this.cats.length; i++){
      var r = this.cats[i];
      var v = {
        id: r.cid,
        name: this.ultility.capitalizeFirstLetter(r.category_name),
        selectAtStart: true,
        selected: r.cid == this.oldPost.cat_id
      };
      if(r.cid == this.oldPost.cat_id)
        selectedItem = v;
      allItems.push(v);
    }

    const rowModal = await this.modalCtrl.create({
      component: PopupSelecterPage,
      id: 'popup-selecter',
      cssClass: 'popup-selecter',
      componentProps: { 
        placeholder: 'Nhập chủ đề',
        allItems:allItems,
        selectedItem: selectedItem
      }
    });
    await rowModal.present();
    var selected = await rowModal.onDidDismiss();
    if(selected && selected.data && selected.data.selectedItem != null){
      this.post.category_name = selected.data.selectedItem.name;
      this.post.cat_id = selected.data.selectedItem.id;
    }
  }

  async onSelectProvince() {
    var allItems: any = [];
    var selectedItem: any = null;
  
    for(var i = 0; i < this.provinces.length; i++){
      var r = this.provinces[i];
      var v = {
        id: r.province_id,
        name: this.ultility.capitalizeFirstLetter(r.name),
        selected: r.province_id == this.oldPost.province_id
      };
      if(r.province_id == this.oldPost.province_id)
        selectedItem = v;
      allItems.push(v);
    }

    const rowModal = await this.modalCtrl.create({
      component: PopupSelecterPage,
      id: 'popup-selecter',
      cssClass: 'popup-selecter',
      componentProps: { 
        placeholder: 'Nhập địa điểm',
        allItems:allItems,
        selectedItem: selectedItem
      }
    });
    await rowModal.present();
    var selected = await rowModal.onDidDismiss();
    if(selected && selected.data && selected.data.selectedItem != null){
      this.post.province_id = selected.data.selectedItem.id;
      this.post.province_name = selected.data.selectedItem.name;
    }
}


  async showActionSheet(){
    const actionSheet = await this.actionSheetController.create({
        buttons: [{
          text: 'Camera',
          role: 'destructive',
          icon: 'camera',
          handler: () => {
            this.captureVideo(0);
          }
        }, 
        // {
        //   text: 'Thư viện',
        //   icon: 'image',
        //   handler: () => {
        //     this.capturePhotoOrTakeVideo(0);
        //   }
        // }, 
        {
          text: 'Huỷ',
          icon: 'close',
          role: 'cancel',
          handler: () => {
          }
        }]
      });
      await actionSheet.present();
  }
  async captureVideo(type){
    let me = this;
    let loading = this.loadingCtrl.create({
      message: 'Đang đọc dữ liệu. Vui lòng đợi...'
    });
    (await loading).present();
    await this.ultility.captureVideoAndImage(type,
      async (imageData) => {
        if (imageData) {
            this.post.video_url = imageData;
        }
      },
      async (oneFile) => {
        console.log('Lấy ảnh thành công');
        if (oneFile) {
          console.log('One file exist');
          console.log(JSON.stringify(oneFile));
          this.fileToUpload = oneFile;
        }
        (await loading).dismiss();
      },
      async (err) => {
        this.ultility.presentNoti({
          title:'Đọc dữ liệu thất bại',
          content:'Có sự cố xảy ra. Vui lòng thử lại sau.'
        });
        console.log("Lấy ảnh thất bại. Vui lòng thử lại." + err);
        console.log(JSON.stringify(err));
        (await loading).dismiss();

      });
  }
  async capturePhotoOrTakeVideo(sourceType: number){
    let me = this;
    let loading = this.loadingCtrl.create({
      message: 'Đang đọc dữ liệu. Vui lòng đợi...'
    });
    (await loading).present();
    await this.ultility.capturePhotoOrTakeVideo(sourceType,
      async (imageData) => {
        if (imageData) {
            this.post.video_url = imageData;
        }
      },
      async (oneFile) => {
        console.log('Lấy ảnh thành công');
        if (oneFile) {
          console.log('One file exist');
          console.log(JSON.stringify(oneFile));
          this.fileToUpload = oneFile;
        }
        (await loading).dismiss();
      },
      async (err) => {
        this.ultility.presentNoti({
          title:'Đọc dữ liệu thất bại',
          content:'Có sự cố xảy ra. Vui lòng thử lại sau.'
        });
        console.log("Lấy ảnh thất bại. Vui lòng thử lại." + err);
        console.log(JSON.stringify(err));
        (await loading).dismiss();

      });
  }


  async addVideo() {
    console.log("addVideo: " + JSON.stringify(this.post));
    if (this.post.cat_id == "") {    
      this.post.cat_id = 1;
    }
    if (this.post.title == "") {
      this.ultility.presentNoti({
        title:'Thông tin Video',
        content:"Vui lòng nhập 'Tiêu đề' của Video."
      });
      return;
    }
    if (this.post.summary == "") {
      this.ultility.presentNoti({
        title:'Thông tin Video',
        content:"Vui lòng nhập 'Nội dung' của Video."
      });
      return;
    }
    if (this.fileToUpload == null && !this.isEdit) {
      this.ultility.presentNoti({
        title:'Thông tin Video',
        content:"Vui lòng chọn video muốn đăng tải."
      });
      return;
    }

    if (this.fileToUpload) {
      let loading = this.loadingCtrl.create({
        message: 'Đang xử lý dữ liệu. Vui lòng đợi...'
      });
      (await loading).present();
      var nativeUrl = this.fileToUpload.nativeURL;
      const tempFilename = nativeUrl.substr(nativeUrl.lastIndexOf('/') + 1);
      const tempBaseFilesystemPath = nativeUrl.substr(0, nativeUrl.lastIndexOf('/') + 1);
      let type = 'video/mp4';
      if(tempFilename.includes('.mp4') || tempFilename.includes('.mov')){
        type = 'video/mp4';
      }
      else{
        type = 'image/jpeg';
      }
      File.readAsArrayBuffer(tempBaseFilesystemPath, tempFilename).then(realFile => {
        console.log('Img name' + this.fileToUpload.name);
        let newName = this.global.userInfo.user_id + '/' + + new Date().getTime() + this.fileToUpload.name;
        let fileUrl = '';
        let size = Math.floor(realFile.byteLength / 1024);

        // Get the URL for our PUT request
        // get url upload --> upload --> get path file --> add market items
        console.log('Media Info: ' + newName + "   with size: " + size + "kB and type: " + type);

        this.apiProvider.getUrlUpload(newName, size, type).then(async (dataUpload) => {
          console.log("get Url Upload JSON: " + JSON.stringify(dataUpload));
          if (dataUpload.status = "ok") {

            console.log('get Url Upload: ' + dataUpload.url);

            // Finally upload the file (arrayBuffer) to AWS
            this.apiProvider.uploadFile(dataUpload.url, realFile, type).subscribe(result => {
              console.log('upload file: ' + dataUpload.url);

              // Add the resolved URL of the file to our local array
              this.apiProvider.getPathFile(newName).then(dataPath => {
                fileUrl = dataPath.url;
                console.log('get path file: ' + fileUrl);

                if (dataPath.status = "ok") {
                  this.apiProvider.addVideo(this.post.cat_id, this.post.title, this.post.summary, this.global.userInfo.phone, fileUrl, newName, this.post.province_id)
                    .then(async response => {
                      (await loading).dismiss();
                      console.log('addVideo: ' + fileUrl + ' ' + newName);
                      console.log('addVideo result: ' + JSON.stringify(response));
                      if (response.status = "ok") {
                        this.ultility.presentNoti({
                          title:'Thêm video thành công',
                          content:'Video của bạn sẽ được đăng tải sau khi kiểm duyệt.'
                        });
                        this.close(true);
                      }
                      else {
                        this.ultility.presentNoti({
                          title:'Thêm video thất bại',
                          content:'Có sự cố xảy ra. Vui lòng thử lại sau.'
                        });
                        console.log('addVideo: ' + JSON.stringify(response));
                      }
                    }).catch(async err => {
                      this.ultility.presentNoti({
                        title:'Thêm video thất bại',
                        content:'Có sự cố xảy ra. Vui lòng thử lại sau.'
                      });
                      console.log('addVideo: ' + JSON.stringify(err));
                      (await loading).dismiss();
                    });
                }
              }).catch(async err => {
                this.ultility.presentNoti({
                  title:'Thêm video thất bại',
                  content:'Có sự cố xảy ra. Vui lòng thử lại sau.'
                });
                console.log('getPathFile: ' + JSON.stringify(err));
                (await loading).dismiss();
              });
            })
          }
          else {
            this.ultility.presentNoti({
              title:'Thêm video thất bại',
              content:'Có sự cố xảy ra. Vui lòng thử lại sau.'
            });
            console.log('getUrlUpload: ' + JSON.stringify(dataUpload));
            (await loading).dismiss();
          }

        }).catch(async (err) => {
          this.ultility.presentNoti({
            title:'Thêm video thất bại',
            content:'Có sự cố xảy ra. Vui lòng thử lại sau.'
          });
          console.log('getUrlUpload: ' + JSON.stringify(err));
          (await loading).dismiss();
        });


      });
    }
    else if(this.isEdit){
      this.apiProvider.editVideo(this.post.vid, 
        this.post.cat_id || this.post.cat_id || 1, 
        this.post.title, 
        this.post.summary, this.global.userInfo.phone, this.post.video_url,
        this.post.province_id)
        .then(async response => {
          if (response.status = "ok") {
            this.ultility.presentNoti({
              title:'Sửa video thành công',
              content:'Video của bạn đã được cập nhật.'
            });
            this.close(true);
          }
          else {
            this.ultility.presentNoti({
              title:'Sửa video thất bại',
              content:'Có sự cố xảy ra. Vui lòng thử lại sau.'
            });
            console.log('editVideo: ' + JSON.stringify(response));
          }
        }).catch(async err => {
          this.ultility.presentNoti({
            title:'Sửa video thất bại',
            content:'Có sự cố xảy ra. Vui lòng thử lại sau.'
          });
          console.log('editVideo -> catch: ' + JSON.stringify(err));
        });
    }
    else {
      this.ultility.presentNoti({
        title:'Sửa video thất bại',
        content:'Có sự cố xảy ra. Vui lòng thử lại sau.'
      });
    }
  }
}
