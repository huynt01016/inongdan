import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoAddPage } from './video-add.page';

describe('VideoAddPage', () => {
  let component: VideoAddPage;
  let fixture: ComponentFixture<VideoAddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoAddPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoAddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
