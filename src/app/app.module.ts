import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpModule } from '@angular/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

import { NgxMaskIonicModule } from 'ngx-mask-ionic'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ApiProvider } from '../providers/api/api';
import { UltilityProvider } from '../providers/ultility/ultility';
import { Global } from '../providers/appbase/appbase';

import { DetailPageModule } from '../app/detail/detail.module';
import { SearchPageModule } from '../app/search/search.module';
import { CommentPageModule } from '../app/comment/comment.module';
import { LoginPageModule } from '../app/personal/login/login.module';
import { RegisterPageModule } from '../app/personal/register/register.module';
import { ForgotPasswordPageModule } from '../app/personal/forgot-password/forgot-password.module';
import { UpdateProfilePageModule } from '../app/personal/update-profile/update-profile.module';
import { MarketDetailPageModule } from '../app/market/market-detail/market-detail.module';
import { MarketAddPageModule } from '../app/market/market-add/market-add.module';
import { NotificationsPageModule } from '../app/notifications/notifications.module';
import { HelpDetailPageModule } from '../app/help/help-detail/help-detail.module';
import { HelpAddPageModule } from '../app/help/help-add/help-add.module';
import { DropdownlistPageModule } from '../app/dropdownlist/dropdownlist.module';
import { CategoryPostsPageModule } from '../app/category-posts/category-posts.module';
import { VideoAddPageModule } from '../app/video/video-add/video-add.module';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { HomeMenuPageModule } from './home-menu/home-menu.module';
import { VideoDetailPageModule } from './video/video-detail/video-detail.module';
import { PopupSelecterPageModule } from './popup-selecter/popup-selecter.module';
import { MediaCapture } from '@ionic-native/media-capture/ngx';
import { PopupNotificationPageModule } from './popup-notification/popup-notification.module';
import { FCM } from '@ionic-native/fcm/ngx';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, HttpModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    NgxMaskIonicModule.forRoot(),

    AppRoutingModule,
    DetailPageModule,
    HomeMenuPageModule,
    CategoryPostsPageModule,
    SearchPageModule,
    CommentPageModule,
    LoginPageModule,
    RegisterPageModule,
    ForgotPasswordPageModule,
    UpdateProfilePageModule,
    MarketDetailPageModule,
    MarketAddPageModule,
    NotificationsPageModule,
    HelpDetailPageModule,
    HelpAddPageModule,
    DropdownlistPageModule,
    VideoAddPageModule,
    VideoDetailPageModule,
    PopupSelecterPageModule,
    PopupNotificationPageModule],
  providers: [
    StatusBar,
    SplashScreen,
    ApiProvider,
    UltilityProvider,
    Global,
    ImagePicker,
    InAppBrowser,
    AppVersion,
    EmailComposer,
    PhotoViewer,
    MediaCapture,
    FCM,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
