import { Component } from '@angular/core';
import { Global } from '../../providers/appbase/appbase';
import { ModalController } from '@ionic/angular';
import { LoginPage } from '../personal/login/login.page';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage {
  
  notifications: Array<number> = [1,1,1,1,1,1,1,1,1];
  constructor(
    public global: Global,
    public modalCtrl: ModalController,) { 
      if (this.global.userInfo == undefined)
      {
      }
    }

  async showLogin() {
    const modal = await this.modalCtrl.create({
      component: LoginPage, 
    });
    return await modal.present();
  }

  
  close() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }


}
