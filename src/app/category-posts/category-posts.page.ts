import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiProvider } from 'src/providers/api/api';
import { DetailPage } from '../detail/detail.page';
import { ModalController, NavParams } from '@ionic/angular';
import { UltilityProvider } from 'src/providers/ultility/ultility';
import * as Enums from '../../providers/appbase/appbase';
import { Global } from '../../providers/appbase/appbase';

@Component({
  selector: 'app-category-posts',
  templateUrl: './category-posts.page.html',
  styleUrls: ['./category-posts.page.scss'],
})
export class CategoryPostsPage {
  commentSucess = false;
  offlineMode: number = 0;
  text_noconnection: string = "";
  category: any;
  currentPage: number = 1;
  firstPost: any;
  posts: Array<{ cat_id: number; category_name: string; nid: number; news_title: string; news_date: string; news_image: string; news_description: string }>;
  cid: number;
  constructor(public route: ActivatedRoute,
    public navParams: NavParams,
    public api: ApiProvider,
    public modalController: ModalController,
    public global: Global,
    public ultility: UltilityProvider) {
    this.cid = this.navParams.get('cid');

    this.firstPost = {
      news_title: "",
      news_image: "https://wallpapercave.com/wp/Hvtxo6m.jpg",
      news_date: "",
      category_name: ''
    };
    this.category = {
      category_name: '',
    }
    this.posts = [];
    this.getCategoryPost(this.cid);
    this.global.current_cat_id = this.cid;
  }

  ionViewWillLeave(){
    console.log('CategoryPostsPage ionViewWillLeave');
    this.global.current_cat_id = -1;
  }
  close() {
    this.global.current_cat_id = -1;
    this.modalController.dismiss({
      'dismissed': true,
    });
  }

  getCategoryPost(cid, infiniteScroll: any = undefined) {
    this.api.getCategoryPost(cid, this.currentPage, 10)
      .then((data) => {
        if (!data || !data.posts || data.posts.length <= 0) {
          console.log('getRecentPost -> data is empty');
          if (this.currentPage == 1) {
            this.text_noconnection = "CHƯA CÓ BÀI VIẾT";
            this.offlineMode = Enums.OfflineMode.NO_DATA;
          }
        }
        else {
          this.offlineMode = Enums.OfflineMode.NORMAL;
          if (this.currentPage == 1) {
            this.firstPost = data.posts[0];
            this.firstPost.date = this.ultility.convertToDate(this.firstPost.news_date, true);
            this.firstPost.title = this.firstPost.news_title;
            this.category.category_name = this.firstPost.category_name
            for (let index = 1; index < data.posts.length; index++) {
              data.posts[index].date = this.ultility.convertToDate(data.posts[index].news_date);
              data.posts[index].title = this.ultility.cropString(data.posts[index].news_title, " ", 70, 75);
              data.posts[index].isVideo = data.posts[index].content_type === "VIDEO";

              this.posts.push(data.posts[index]);
            }
          }
          else {
            for (let index = 0; index < data.posts.length; index++) {
              data.posts[index].date = this.ultility.convertToDate(data.posts[index].news_date);
              data.posts[index].title = this.ultility.cropString(data.posts[index].news_title, " ", 70, 75);
              data.posts[index].isVideo = data.posts[index].content_type === "VIDEO";

              this.posts.push(data.posts[index]);
            }
          }
          this.currentPage++;
        }
        if (infiniteScroll != undefined)
          infiniteScroll.complete();
      })
      .catch(err => {
        console.log(`getCategoryPost error-> ${err}`);
        if (this.posts.length <= 0) {
          this.text_noconnection = "KHÔNG TẢI ĐƯỢC DỮ LIỆU";
          this.offlineMode = Enums.OfflineMode.NO_INTERNET;
          if (infiniteScroll != undefined)
            infiniteScroll.complete();
        }
      })
  }
  async showPostDetail(nid: number) {
    const modal = await this.modalController.create({
      component: DetailPage,
      componentProps: {
        'nid': nid,
      }
    });

    modal.onDidDismiss().then((detail) => {
      if (detail !== null && detail.data && detail.data.commentSucess !== undefined) {
        this.commentSucess = detail.data.commentSucess
        if (this.commentSucess)
        {
          this.currentPage = 1;
          this.posts = [];
          this.getCategoryPost(this.cid);
        }
      }
    });
    return await modal.present();
  }

  doInfinite(event: any) {
    console.log("doinfinite")
    this.getCategoryPost(this.cid, event.target);
  }

  tryAgain() {
    this.getCategoryPost(this.cid);
  }

}
