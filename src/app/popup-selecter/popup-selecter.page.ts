import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { UltilityProvider } from 'src/providers/ultility/ultility';

@Component({
  selector: 'app-popup-selecter',
  templateUrl: './popup-selecter.page.html',
  styleUrls: ['./popup-selecter.page.scss'],
})
export class PopupSelecterPage implements OnInit {

  allItems: any = []
  allFilterItems: any = []
  selectedItem: any = {}
  placeholder: string = ''
  hasSearchBar: boolean = true
  constructor(private modalCtrl: ModalController,
            private navParams: NavParams,
            private ulti: UltilityProvider) {
    this.allItems = this.navParams.get('allItems') || [];
    this.selectedItem = this.navParams.get('selectedItem') || {};
    this.placeholder = this.navParams.get('placeholder') || 'Nhập tên';
    this.hasSearchBar = this.navParams.get('hasSearchBar') || true;
    this.getItems(null);
  }

  ngOnInit() {
  }
  select(v: any){
    this.selectedItem = v;
    this.selectedItem.select = true;
    this.modalCtrl.dismiss({
      selectedItem: this.selectedItem
    }, "", "popup-selecter");
  }
  getItems(ev: any) {

    // set val to the value of the searchbar
    const val = ev ? ev.target.value : '';

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.allFilterItems = this.allItems.filter((value) => {
        return (this.ulti.xoa_dau(value.name.toLowerCase()).indexOf(this.ulti.xoa_dau(val.toLowerCase())) > -1);
      })
    }
    else {
      this.allFilterItems = this.allItems;
    }
  }

}
