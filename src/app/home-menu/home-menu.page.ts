import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-home-menu',
  templateUrl: './home-menu.page.html',
  styleUrls: ['./home-menu.page.scss'],
})
export class HomeMenuPage implements OnInit {
  categories: any = [];
  selected: any = {};
  constructor(
    public modalCtrl: ModalController,
    public navParams: NavParams) { 
      
      this.selected = null;

      this.categories = this.navParams.get('categories') || [];
      if(this.categories.length > 0 && this.categories[0].cid != -1){
        this.categories.unshift({
          cid: -1,
          category_name: 'Tin tức chung',
          category_image: '../../assets/home-default.jpg'
        });
      }
    }

  ngOnInit() {
  }

  close(){
    this.modalCtrl.dismiss({
      'dismissed': true,
      'selected': this.selected
    }, "", "home-menu");
  }
  select(cat){
    this.selected = cat;
    this.close();
  }

}
