import { Component, OnInit } from '@angular/core';
import { ApiProvider } from 'src/providers/api/api';
import { Global } from 'src/providers/appbase/appbase';
import { UltilityProvider } from 'src/providers/ultility/ultility';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {

  news: Array<any>;
  information: Array<any>;
  constructor(public apiProvider: ApiProvider,
            public global: Global,
            public ultility: UltilityProvider) { 
    
  }

  ngOnInit() {
  }
  
  ionViewWillEnter(){
    this.news = [
    ];
    this.information = [
    ];
    this.getNotification();
  }

  getNotification(){
    if(!this.global.isLogin)
      return;
    if (this.global.userInfo == undefined)
      return;
    this.apiProvider.getNotification(this.global.userInfo.user_id, 1, 20)
      .then(data => {
        if(data.status == "ok" && data.notifications){
          this.fillData(data.notifications);
        }
      }).catch(err => {

      });
  }
  fillData(notifications){
    for(let i = 0; i < notifications.length; i++){
      let curr = notifications[i];   
      curr.src = 'assets/icon/user-activated.png';
      curr.created = this.ultility.convertToDate(curr.created, true);
      if(curr.status == 0){
        this.news.push(curr);
      }
      else{
        this.information.push(curr);
      }
    }
  }
  read(item, index){
    this.news.splice(index, 1);
    this.information.unshift(item);

    this.apiProvider.updateNotification(item.notification_id)
      .then((result)=>{
        console.log("updateNotification: " + JSON.stringify(result));
      })
      .catch(err=>{
        console.log("updateNotification -> failed: " + JSON.stringify(err));
      });
  }

}
