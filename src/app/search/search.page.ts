import { Component } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import { ApiProvider } from '../../providers/api/api';
import { Url } from 'url';
import { Global } from '../../providers/appbase/appbase';
import { ModalController } from '@ionic/angular';
import { DetailPage } from '../detail/detail.page';
import { MarketDetailPage } from '../market/market-detail/market-detail.page';
import { HelpDetailPage } from '../help/help-detail/help-detail.page';

import { UltilityProvider } from '../../providers/ultility/ultility';
import * as Enums from '../../providers/appbase/appbase';
import { LoadingController } from '@ionic/angular';
import { ConstantPool } from '@angular/compiler';
import { VideoDetailPage } from '../video/video-detail/video-detail.page';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage {
  category = this.navParams.get('category');
  // 1: getSearchResult
  // 2: getSearchMarkets
  // 3: getSearchAnswer

  offlineMode: number = 0;
  text_noconnection: string = "";
  myInput: string = "";
  searchContent: string;
  isHidden: boolean = true;
  currentPage: number = 1;
  posts: Array<{ cat_id: number; category_name: string; nid: number; news_title: string; news_date: Date; news_image: Url; news_description: string }>;
  searchWords: Array<any>;

  maxLength: number = 50;
  market_items: any;
  answers: any;
  videos: any;
  length: number;
  constructor(
    public apiProvider: ApiProvider,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public global: Global,
    public ultility: UltilityProvider,
    public loadingController: LoadingController) {
    this.posts = this.navParams.get('recentPost') || [];
    this.market_items = this.navParams.get('recentPost') || [];
    this.answers = this.navParams.get('recentPost') || [];
    this.videos = this.navParams.get('recentPost') || [];

    this.searchWords = [];
    console.log(this.category);
    if (this.category == 1) {
      this.searchWords.push("Không khí lạnh");
      this.searchWords.push("Dịch tả lợn châu phi");
      this.searchWords.push("Cá rô phi đơn tính");
      this.searchWords.push("Tôm biển");
      this.searchWords.push("Nông nghiệp công nghệ cao");
    }
    else if (this.category == 2) {
      this.searchWords.push("Bưởi");
      this.searchWords.push("Thuốc bảo vệ thực vật");
      this.searchWords.push("Cam vinh");
      this.searchWords.push("Chuyên");
      this.searchWords.push("Mít sấy");
    }
    else if (this.category == 3) {
      this.searchWords.push("Chồn hương");
      this.searchWords.push("Dịp tết");
      this.searchWords.push("Cá rô phi đơn tính");
      this.searchWords.push("Trâu");
      this.searchWords.push("Nổi mẩn");
    }
    else if (this.category == 4) {
      this.searchWords.push("Cá");
      this.searchWords.push("Ba ba");
      this.searchWords.push("Dịch tả");
      this.searchWords.push("Gà");
      this.searchWords.push("Nấm rơm");
    }

  }

  getSearch(infiniteScroll: any = undefined) {

    this.text_noconnection = "";
    if (this.category == 1) {
      this.getSearchResult(infiniteScroll);
    } else if (this.category == 2) {
      this.getSearchMarkets(infiniteScroll);
    } else if (this.category == 3) {
      this.getSearchAnswer(infiniteScroll);
    } else if (this.category == 4) {
      this.getSearchVideo(infiniteScroll);
    }
  }

  getSearchResult(infiniteScroll: any = undefined) {
    console.log("getSearchResult: " + this.searchContent);
    this.presentLoading();
    var count = 10;
    this.apiProvider.getSearchResult(this.searchContent, this.currentPage, count)
      .then(data => {
        this.dismissLoading();
        if (!data || !data.posts || data.posts.length <= 0) {
          console.log('getSearchResult -> data is empty');
          if (this.currentPage == 1) {
            this.text_noconnection = "Chưa có bài viết.";
            this.offlineMode = Enums.OfflineMode.NO_DATA;
            this.isHidden = true;
          }
        }
        else {
          //  console.log(JSON.stringify(data.posts))
          this.offlineMode = Enums.OfflineMode.NORMAL;
          for (let index = 0; index < data.posts.length; index++) {
            data.posts[index].date = this.ultility.convertToDate(data.posts[index].news_date);
            data.posts[index].title = this.ultility.cropString(data.posts[index].news_title, " ", 90, 95);
            this.posts.push(data.posts[index]);
            this.length = this.posts.length;
          }
          this.isHidden = false;
        }
        if (infiniteScroll != undefined)
          infiniteScroll.complete();
      }).catch(err => {
        if (this.posts.length <= 0) {
          this.text_noconnection = "Không tải được dữ liệu";
          this.offlineMode = Enums.OfflineMode.NO_INTERNET;
          this.isHidden = true;
          this.dismissLoading();
          if (infiniteScroll != undefined)
            infiniteScroll.complete();
        }
      });
  }

  getSearchMarkets(infiniteScroll: any = undefined) {
    console.log("getSearchMarkets: " + this.searchContent);
    this.presentLoading();
    var count = 10;
    this.apiProvider.getSearchMarkets(this.searchContent, this.currentPage, count)
      .then(data => {
        this.dismissLoading();
        // console.log(JSON.stringify(data));
        if (!data || !data.markets || data.markets.length <= 0) {
          console.log('getMarketItemsType -> data is empty');
          if (this.currentPage == 1) {
            this.text_noconnection = "Chưa có bài viết.";
            this.offlineMode = Enums.OfflineMode.NO_DATA;
            this.isHidden = true;
          }
        }
        else {
          this.offlineMode = Enums.OfflineMode.NORMAL;
          for (let index = 0; index < data.markets.length; index++) {
            data.markets[index].date = this.ultility.convertToDate(data.markets[index].created);
            data.markets[index].title = data.markets[index].title;
            data.markets[index].title_short = this.ultility.cropString(data.markets[index].title, " ", 90, 95);
            if (data.markets[index].title.length > this.maxLength)
              data.markets[index].title_short = data.markets[index].title.substring(0, this.maxLength - 2) + "..";
            this.market_items.push(data.markets[index]);
            this.length = this.market_items.length;
          }
          this.currentPage++;
          this.isHidden = false;
        }
        if (infiniteScroll != undefined)
          infiniteScroll.complete();
      }).catch(err => {

        if (this.market_items.length <= 0) {
          this.text_noconnection = "Không tải được dữ liệu.";
          this.offlineMode = Enums.OfflineMode.NO_INTERNET;
          if (infiniteScroll != undefined)
            infiniteScroll.complete();
        }
      });
  }

  getSearchAnswer(infiniteScroll: any = undefined) {
    console.log("getSearchAnswer: " + this.searchContent);
    this.presentLoading();
    var count = 10;
    this.apiProvider.getSearchAnswer(this.searchContent, this.currentPage, count)
      .then(data => {
        this.dismissLoading();

        if (!data || !data.answers || data.answers.length <= 0) {
          console.log('getAnswer -> data is empty');
          if (this.currentPage == 1) {
            this.text_noconnection = "Chưa có bài viết";
            this.offlineMode = Enums.OfflineMode.NO_DATA;
            this.isHidden = true;
          }
        }
        else {
          // console.log(JSON.stringify(data));
          let maxLength = 80;
          this.offlineMode = Enums.OfflineMode.NORMAL;
          for (let index = 0; index < data.answers.length; index++) {
            data.answers[index].date = this.ultility.convertToDate(data.answers[index].created);
            data.answers[index].title = this.ultility.cropString(data.answers[index].title, " ", 90, 95);
            if (data.answers[index].full_name == undefined || data.answers[index].full_name == "")
              data.answers[index].display_name = data.answers[index].creator;
            else
              data.answers[index].display_name = data.answers[index].full_name;

            var status = data.answers[index].status;
            data.answers[index].div_status = status == 0 ? "div-status-yellow" : status == 1 ? "div-status-green" : "div-status-gray";
            data.answers[index].text_status = status == 0 ? "Chưa trả lời" : status == 1 ? "Đã trả lời" : "";

            if(data.answers[index].attachment && !data.answers[index].attachment.includes(null))
              data.answers[index].image = data.answers[index].attachment;
            else
              data.answers[index].image = '../../../assets/question-img.png';

            this.answers.push(data.answers[index]);
            this.length = this.answers.length;
            this.isHidden = false;
          }
          this.currentPage++;
        }

        if (infiniteScroll != undefined)
          infiniteScroll.complete();
      }).catch(err => {
        if (this.answers.length <= 0) {
          this.text_noconnection = "Không tải được dữ liệu.";
          this.offlineMode = Enums.OfflineMode.NO_INTERNET;
          if (infiniteScroll != undefined)
            infiniteScroll.complete();
        }
      });
  }

  getSearchVideo(infiniteScroll: any = undefined) {
    console.log("getSearchResult: " + this.searchContent);
    this.presentLoading();
    var count = 10;
    this.apiProvider.getSearchVideo(this.searchContent, this.currentPage, count)
      .then(data => {
        this.dismissLoading();
        if (!data || !data.posts || data.posts.length <= 0) {
          console.log('getSearchResult -> data is empty');
          if (this.currentPage == 1) {
            this.text_noconnection = "Chưa có bài viết.";
            this.offlineMode = Enums.OfflineMode.NO_DATA;
            this.isHidden = true;
          }
        }
        else {
          //  console.log(JSON.stringify(data.posts))
          this.offlineMode = Enums.OfflineMode.NORMAL;
          for (let index = 0; index < data.posts.length; index++) {
            data.posts[index].date = this.ultility.convertToDate(data.posts[index].news_date);
            data.posts[index].title = this.ultility.cropString(data.posts[index].news_title, " ", 90, 95);
            this.videos.push(data.posts[index]);
            this.length = this.videos.length;
          }
          this.isHidden = false;
        }
        if (infiniteScroll != undefined)
          infiniteScroll.complete();
      }).catch(err => {
        if (this.videos.length <= 0) {
          this.text_noconnection = "Không tải được dữ liệu";
          this.offlineMode = Enums.OfflineMode.NO_INTERNET;
          this.isHidden = true;
          this.dismissLoading();
          if (infiniteScroll != undefined)
            infiniteScroll.complete();
        }
      });
  }

  searchByKeyword(event: any) {
    let val = event.target.value.trim();
    console.log(val);
    if (val != "") {
      this.currentPage = 1;
      this.posts = [];
      this.market_items = [];
      this.answers = [];
      this.videos = [];
      this.searchContent = val;
      this.getSearch();
    }
  }

  searchByFixword(content) {
    this.currentPage = 1;
    this.posts = [];
    this.market_items = [];
    this.answers = [];
    this.videos = [];

    this.searchContent = content;
    this.getSearch();
  }

  async showPostDetail(nid: number) {
    const modal = await this.modalCtrl.create({
      component: DetailPage,
      componentProps: {
        'nid': nid,
      }
    });
    return await modal.present();
  }
  async showVideoDetail(post) {
    const modal = await this.modalCtrl.create({
      component: VideoDetailPage,   
      id: 'video-detail',
      componentProps: {
        'post': post,
        'currentInfoBtnValue': 0
      }
    });
    await modal.present();
  }

  async showMarketDetail(marketid: number) {
    var data: any;
    for (let i = 0; i < this.market_items.length; i++) {
      if (this.market_items[i].market_id == marketid) {
        data = this.market_items[i];
        break;
      }
    }

    if (data != undefined) {
      const modal = await this.modalCtrl.create({
        component: MarketDetailPage,
        componentProps: {
          'data': data,
        }
      });
      return await modal.present();
    }
  }

  async showAnswerDetail(data: any) {
    const modal = await this.modalCtrl.create({
      component: HelpDetailPage,
      componentProps: {
        'data': data,
      },
      id: 'HelpDetailPage'
    });

    return await modal.present();
  }

  doInfinite(event: any) {
    console.log("doinfinite")
    this.currentPage++;
    this.getSearch(event.target);
  }


  close() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  tryAgain() {
    this.getSearchResult();
    this.presentLoading();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      duration: 3000
    });
    await loading.present();
  }

  dismissLoading() {
    this.loadingController.getTop().then(v => v ? this.loadingController.dismiss() : null);
  }

  handleImgError(ev: any, item : any){
    item.news_image = "../../assets/video-default.jpg";
    console.log("----> handleImgError: " + ev + "  " + JSON.stringify(item));
  }
}
