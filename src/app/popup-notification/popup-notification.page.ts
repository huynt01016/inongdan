import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-popup-notification',
  templateUrl: './popup-notification.page.html',
  styleUrls: ['./popup-notification.page.scss'],
})
export class PopupNotificationPage implements OnInit {

  duration: number = 3000;
  title: string ='';
  content: string ='';
  dismissTimeOut: any = null;
  constructor(private modalCtrl: ModalController, 
    private navParams: NavParams) {    
      this.duration = this.navParams.get('duration') || 3000;
      this.title = this.navParams.get('title') || 'Thông báo từ hệ thống';
      this.content = this.navParams.get('content') || 'Nội dung của thông báo';
    }

  ngOnInit() {
    this.dismissTimeOut = setTimeout(async ()=>{
      await this.modalCtrl.dismiss({},'','notification-popup');
    }, this.duration);
  }
  ionViewWillLeave(){
    if(this.dismissTimeOut){
      clearTimeout(this.dismissTimeOut)
      this.dismissTimeOut = null;
    }
  }

}
