import { Component } from '@angular/core';
import { Global } from '../../providers/appbase/appbase';
import { ApiProvider } from 'src/providers/api/api';
import { ModalController, AlertController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { UltilityProvider } from '../../providers/ultility/ultility';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

import { LoginPage } from '../personal/login/login.page';
import { UpdateProfilePage } from '../personal/update-profile/update-profile.page';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage {
  constructor(
    public global: Global,
    public modalCtrl: ModalController,
    public apiProvider: ApiProvider,
    public alertController: AlertController,
    public ultility: UltilityProvider,
    private storage: Storage,
    private appVersion: AppVersion,
    private emailComposer: EmailComposer,
    private loadingCtrl: LoadingController
  ) {
    console.log(this.emailComposer);
  }

  
  async showLogin() {
    const modal = await this.modalCtrl.create({
      component: LoginPage,
    });
    return await modal.present();
  }
  async showRegister() {
    const modal = await this.modalCtrl.create({
      component: LoginPage,
    });
    return await modal.present();
  }

  async showLogin1() {
    const alert = await this.alertController.create({
      header: 'Đăng nhập',
      message: 'Vui lòng nhập thông tin',
      inputs: [
        {
          name: 'username',
          placeholder: 'Email/Số điện thoại'
        },
        {
          name: 'password',
          placeholder: 'Mật khẩu',
          type: 'password'
        }
      ],
      cssClass: 'mihn-alert',
      buttons: [
        {
          text: 'Đồng ý',
          cssClass: 'dark',
          handler: (data) => {
            this.ultility.loginWithData(data.username, data.password, (u, p)=>{         
              this.storage.set('credentials', { username: u, password: p });
            });
          }
        },
        
        {
          text: 'Bỏ qua',
          cssClass: 'dark',
          handler: (blah) => {
          }
        }
      ]
    });

    await alert.present();
      
  }

  async showLogout() {
    const alert = await this.alertController.create({
      header: 'Đăng xuất',
      message: 'Bạn có muốn đăng xuất?',
      buttons: [
        {
          text: 'Đồng ý',
          cssClass: 'dark',
          handler: () => {
            this.storage.remove('credentials');
            this.global.isLogin = false;
            this.global.userInfo = null;
          }
        },
        {
          text: 'Bỏ qua',
          cssClass: 'dark',
          handler: () => {
          }
        }
      ]
    });

    await alert.present();
  }
  
  async showUpdate() {
    const modal = await this.modalCtrl.create({
      component: UpdateProfilePage,
    });
    return await modal.present();
  }

  about(){ 
    let aboutContent = 'Cổng Nông dân là hệ thống thông tin nhằm đáp ứng nhu cầu tra cứu thông tin, tư vấn, hỏi đáp của bà con nông dân; Kênh kết nối người nông dân với các nhà Khoa học trong lĩnh vực nông nghiệp, môi trường và với các luật sư khi cần tư vấn về chính sách, pháp luật; Tạo môi trường giao thương nông sản, hàng hóa, chế phẩm sinh học…giữa các tổ chức, cá nhân liên quan đến nông nghiệp, nông dân và nông thôn một cách nhanh chóng hiệu quả với độ tin cậy cao nhất.';
    this.showAlertPrivacy('Giới thiệu', aboutContent);
  }
  
  getPrivacyPolicy() {
    this.apiProvider.getPrivacyPolicy().then(data => {
      // console.log(JSON.stringify(data));
      this.showPostDetail(data.post[0].nid);
    }).catch(err => {

    });
  }

  showPostDetail(nid) {
    this.apiProvider.getPostDetail(nid)
      .then(data => {
        console.log(data.post[0].news_description);
        this.showAlertPrivacy('Điều khoản sử dụng', data.post[0].news_description);
      }).catch(err => {

      });

  }

  async showAlertPrivacy(title: string, message: string) {
    const alert = await this.alertController.create({
      header: title,
      message: message,
      buttons: [
        {
          text: 'Thoát',
          role: 'cancel',
        }
      ]
    });

    await alert.present();
  }

  openUrl() {
    var packageName = this.appVersion.getPackageName();;
    var link = "https://play.google.com/store/apps/details?id=" + packageName;
    this.ultility.openUrl(link);
  }

  support() {
    if (this.emailComposer.isAvailable())
    {
      this.emailComposer.isAvailable().then((available: boolean) => {
        if (available) {
          //Now we know we can send
          let email = {
            to: 'dieunguyen@inongdan.vn',
            subject: 'iNongDan Support',
            body: '',
            isHtml: true
          }
  
          // Send a text message using default options
          this.emailComposer.open(email);
        }
      });
    }

  }

  async openProfile() {
    const modal = await this.modalCtrl.create({
      component: UpdateProfilePage,
    });
    return await modal.present();
  }
}
