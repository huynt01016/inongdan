import { Component } from '@angular/core';
import { NavParams, PopoverController, ModalController } from '@ionic/angular';
import { UltilityProvider } from 'src/providers/ultility/ultility';

@Component({
  selector: 'app-dropdownlist',
  templateUrl: './dropdownlist.page.html',
  styleUrls: ['./dropdownlist.page.scss'],
})
export class DropdownlistPage {

  title = this.navParams.get('title');
  values = this.navParams.get('values');
  valuesFilter: Array<any>;
  value:string ='';
  name: string ='';
  isItemAvailable = false;
  constructor(
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public ulti: UltilityProvider
  ) {
    var ev = {
      target: {
        value: '',
      }
    }
    this.getItems(ev);
    this.isItemAvailable = true;
  }

  getItems(ev: any) {

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.isItemAvailable = true;
      this.valuesFilter = this.values.filter((value) => {
        return (this.ulti.xoa_dau(value.name.toLowerCase()).indexOf(this.ulti.xoa_dau(val.toLowerCase())) > -1);
      })
      // this.valuesFilter = this.valuesFilter.slice(0, 5);
    }
    else {
      this.valuesFilter = this.values;
    }
  }

  changeValue(item) {
    this.value = item.value;
    this.name= item.name;

    this.close();
  }

  close() {
    this.modalCtrl.dismiss({
      'dismissed': true,
      'value': this.value,
      'name': this.name,
    });
  }
}
