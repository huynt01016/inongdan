import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownlistPage } from './dropdownlist.page';

describe('DropdownlistPage', () => {
  let component: DropdownlistPage;
  let fixture: ComponentFixture<DropdownlistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownlistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownlistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
