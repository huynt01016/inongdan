import { Component } from '@angular/core';
import { ApiProvider } from 'src/providers/api/api';
import { NavController, ModalController } from '@ionic/angular';
import * as Enums from '../../providers/appbase/appbase';
import { SearchPage } from '../search/search.page';
import { Router } from '@angular/router';
import { CategoryPostsPage } from '../category-posts/category-posts.page';

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
})
export class CategoryPage {
  offlineMode: number = 0;
  text_noconnection: string = "";
  categories: any;
  constructor(public apiProvider: ApiProvider,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public router: Router) {
    this.getCategoryIndex();
  }

  getCategoryIndex(){
    this.apiProvider.getCategoryIndex()
      .then((data)=>{
        if(!data || !data.categories || data.categories.length <= 0){
          console.log('getRecentPost -> data is empty');
            this.text_noconnection = "CHƯA CÓ BÀI VIẾT";
            this.offlineMode = Enums.OfflineMode.NO_DATA;
        }
        else
        {
          //  console.log(JSON.stringify(data));
          this.offlineMode = Enums.OfflineMode.NORMAL
          this.categories = data.categories;
        }
        
       }).catch(err=>{
          this.text_noconnection = "KHÔNG TẢI ĐƯỢC DỮ LIỆU";
          this.offlineMode = Enums.OfflineMode.NO_INTERNET;
       });
  }

  async showCategoryPost(cid){
    const modal = await this.modalCtrl.create({
      component: CategoryPostsPage,
      componentProps: {
        'cid': cid,
      }
    });
    return await modal.present();
  }

  async showSearch() {
    const modal = await this.modalCtrl.create({
      component: SearchPage
    });
    return await modal.present();
  }

  
  tryAgain()
  {
    this.getCategoryIndex();
  }
}
