import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, ModalController, ToastController } from '@ionic/angular';
import { ApiProvider } from '../../../providers/api/api';
import { Global } from '../../../providers/appbase/appbase';
import { UltilityProvider } from '../../../providers/ultility/ultility';
import * as Enums from '../../../providers/appbase/appbase';

import { MarketDetailPage } from '../../market/market-detail/market-detail.page';
import { MarketAddPage } from '../../market/market-add/market-add.page';
import { SearchPage } from '../../search/search.page';
import { LoginPage } from '../../personal/login/login.page';

@Component({
  selector: 'app-store',
  templateUrl: './market.page.html',
  styleUrls: ['./market.page.scss'],
})
export class MarketPage {
  get_duration_interval: any;
  deltaTime: 0;
  currentType: number;
  offlineMode: number = 0;
  text_noconnection: string = "";
  currentPage: number = 1;
  category_name: string = '';
  tab: string;
  market_items_buy: any = [];
  market_items_sell: any = [];
  market_types: Array<any>;
  maxLength: number = 50;
  cid: string;

  constructor(
    public route: ActivatedRoute,
    public modalCtrl: ModalController,
    public apiProvider: ApiProvider,
    public navCtrl: NavController,
    public global: Global,
    public ultility: UltilityProvider,
    public toastController: ToastController) {
    this.cid = this.global.dataSender.category.id;
    this.category_name = this.global.dataSender.category.name;
    this.market_items_buy = [];
    this.market_items_sell = [];
    this.currentType = parseInt(this.cid)
    this.tab = '1';
    if (this.currentType == 0) {
      console.log('getMarketItemsUser');
      this.getMarketItemsUser();
    }
    else
      this.getMarketItemsCategory(this.currentType);

    this.get_duration_interval = setInterval(() => { this.update() }, 5000);
  }

  ionViewWillEnter() {
  }

  update(): any {
    this.deltaTime += 5;
    if (this.deltaTime >= 15) {
      this.currentPage = 1;
      this.market_items_buy = [];
      this.market_items_sell = [];
      this.getMarketItems();
      console.log("getMarketItems");
    }
  }

  getMarketItems(infiniteScroll: any = undefined) {
    this.currentType = 0;
    this.apiProvider.getMarketItems(this.currentPage, 10)
      .then(data => {
        if (!data || !data.market_items || data.market_items.length <= 0) {
          console.log('getMarketItems -> data is empty');
          if (this.currentPage == 1) {
            this.text_noconnection = "CHƯA CÓ BÀI VIẾT";
            this.offlineMode = Enums.OfflineMode.NO_DATA;
          }
        }
        else {
          // console.log(JSON.stringify(data));
          this.offlineMode = Enums.OfflineMode.NORMAL;
          if (this.currentPage == 1) {
            for (let index = 0; index < data.market_items.length; index++) {
              data.market_items[index].date = this.ultility.convertToDate(data.market_items[index].created, true);
              data.market_items[index].title = data.market_items[index].title;
              data.market_items[index].title_short = this.ultility.cropString(data.market_items[index].title, " ", this.maxLength, this.maxLength);

              if (data.market_items[index].status == 1) {
                if (data.market_items[index].type == 0)
                  this.market_items_buy.push(data.market_items[index]);
                else
                  this.market_items_sell.push(data.market_items[index]);
              }
            }
          }
          else {
            for (let index = 0; index < data.market_items.length; index++) {
              data.market_items[index].date = this.ultility.convertToDate(data.market_items[index].news_date, true);
              data.market_items[index].title = data.market_items[index].news_title;
              data.market_items[index].title_short = this.ultility.cropString(data.market_items[index].title, " ", this.maxLength, this.maxLength);

              if (data.market_items[index].status == 1) {
                if (data.market_items[index].type == 0)
                  this.market_items_buy.push(data.market_items[index]);
                else
                  this.market_items_sell.push(data.market_items[index]);
              }
            }
          }
          this.currentPage++;
        }
        if (infiniteScroll != undefined)
          infiniteScroll.complete();
      }).catch(err => {

        if (this.market_items_buy.length + this.market_items_sell.length <= 0) {
          this.text_noconnection = "KHÔNG TẢI ĐƯỢC DỮ LIỆU";
          this.offlineMode = Enums.OfflineMode.NO_INTERNET;
          if (infiniteScroll != undefined)
            infiniteScroll.complete();
        }
      });
  }

  getMarketItemsCategory(market_type_id: number, infiniteScroll: any = undefined) {

    this.apiProvider.getMarketItemsType(market_type_id, this.currentPage, 10)
      .then(data => {
        if (!data || !data.market_items || data.market_items.length <= 0) {
          console.log('getMarketItemsType -> data is empty');
          if (this.currentPage == 1) {
            this.text_noconnection = "CHƯA CÓ BÀI VIẾT";
            this.offlineMode = Enums.OfflineMode.NO_DATA;
          }
        }
        else {
          // console.log(JSON.stringify(data));
          this.offlineMode = Enums.OfflineMode.NORMAL;
          if (this.currentPage == 1) {
            for (let index = 0; index < data.market_items.length; index++) {
              data.market_items[index].date = this.ultility.convertToDate(data.market_items[index].created);
              data.market_items[index].title = data.market_items[index].title;
              data.market_items[index].title_short = this.ultility.cropString(data.market_items[index].title, " ", this.maxLength, this.maxLength);

              if (data.market_items[index].status == 1) {
                if (data.market_items[index].type == 0)
                  this.market_items_buy.push(data.market_items[index]);
                else
                  this.market_items_sell.push(data.market_items[index]);
              }
            }
          }
          else {
            for (let index = 0; index < data.market_items.length; index++) {
              data.market_items[index].date = this.ultility.convertToDate(data.market_items[index].created);
              data.market_items[index].title = data.market_items[index].title;
              data.market_items[index].title_short = this.ultility.cropString(data.market_items[index].title, " ", this.maxLength, this.maxLength);

              if (data.market_items[index].status == 1) {
                if (data.market_items[index].type == 0)
                  this.market_items_buy.push(data.market_items[index]);
                else
                  this.market_items_sell.push(data.market_items[index]);
              }
            }
          }
          this.currentPage++;
          console.log(this.market_items_sell);
          console.log(this.market_items_buy);
        }
        if (infiniteScroll != undefined)
          infiniteScroll.complete();
      }).catch(err => {

        if (this.market_items_buy.length + this.market_items_sell.length <= 0) {
          this.text_noconnection = "KHÔNG TẢI ĐƯỢC DỮ LIỆU";
          this.offlineMode = Enums.OfflineMode.NO_INTERNET;
          if (infiniteScroll != undefined)
            infiniteScroll.complete();
        }
      });
  }

  getMarketItemsUser(infiniteScroll: any = undefined) {
    if (this.global.isLogin == false)
      return;
    this.apiProvider.getMarketItemsUser(this.global.userInfo.phone, this.currentPage, 10)
      .then(data => {
        if (!data || !data.market_items || data.market_items.length <= 0) {
          console.log('getMarketItems -> data is empty');
          if (this.currentPage == 1) {
            this.text_noconnection = "CHƯA CÓ SẢN PHẨM";
            this.offlineMode = Enums.OfflineMode.NO_DATA;
          }
        }
        else {
          console.log(JSON.stringify(data));
          this.offlineMode = Enums.OfflineMode.NORMAL;
          if (this.currentPage == 1) {
            for (let index = 0; index < data.market_items.length; index++) {
              data.market_items[index].date = this.ultility.convertToDate(data.market_items[index].created);
              data.market_items[index].title = data.market_items[index].title;
              data.market_items[index].title_short = this.ultility.cropString(data.market_items[index].title, " ", this.maxLength, this.maxLength);

              if (data.market_items[index].status != 9) {
                if (data.market_items[index].type == 0)
                  this.market_items_buy.push(data.market_items[index]);
                else
                  this.market_items_sell.push(data.market_items[index]);
              }
            }
          }
          else {
            for (let index = 0; index < data.market_items.length; index++) {
              data.market_items[index].date = this.ultility.convertToDate(data.market_items[index].created);
              data.market_items[index].title = data.market_items[index].title;
              data.market_items[index].title_short = this.ultility.cropString(data.market_items[index].title, " ", this.maxLength, this.maxLength);

              if (data.market_items[index].status != 9) {
                if (data.market_items[index].type == 0)
                  this.market_items_buy.push(data.market_items[index]);
                else
                  this.market_items_sell.push(data.market_items[index]);
              }
            }
          }
          this.currentPage++;

          // console.log(this.market_items_sell);
          // console.log(this.market_items_buy);
          // this.market_items_buy.sort(function (a, b) {
          //   return a.status - b.status;
          // });
          // this.market_items_sell.sort(function (a, b) {
          //   return a.status - b.status;
          // });

        }
        if (infiniteScroll != undefined)
          infiniteScroll.complete();
      }).catch(err => {

        if (this.market_items_buy.length + this.market_items_sell.length <= 0) {
          this.text_noconnection = "KHÔNG TẢI ĐƯỢC DỮ LIỆU";
          this.offlineMode = Enums.OfflineMode.NO_INTERNET;
          if (infiniteScroll != undefined)
            infiniteScroll.complete();
        }
      });
  }

  doInfinite(event: any) {
    if (!this.global.isLogin) {
      return;
    }
    console.log("doinfinite")
    if (this.currentType == 0)
      this.getMarketItemsUser(event.target);
    else
      this.getMarketItemsCategory(this.currentType, event.target);

  }

  doInfinite2(event: any) {
    if (!this.global.isLogin) {
      return;
    }
    console.log("doinfinite2")
    this.getMarketItemsUser(event.target);
  }

  async showPostDetail(marketid: number) {
    var data: any;
    var all = [...this.market_items_buy, ...this.market_items_sell];
    for (let i = 0; i < all.length; i++) {
      if (all[i].market_id == marketid) {
        data = all[i];
        break;
      }
    }

    if (data != undefined) {
      const modal = await this.modalCtrl.create({
        component: MarketDetailPage,
        componentProps: {
          'data': data,
        }
      });
      modal.onDidDismiss().then((detail) => {
        if (detail !== null && detail.data != null && detail.data.isReload) {
          this.currentPage = 1;
          this.market_items_buy = [];
          this.market_items_sell = [];
          if (this.currentType == 0)
            this.getMarketItemsUser();
          else
            this.getMarketItems();
        }
      });
      return await modal.present();
    }
  }

  async showSearch() {
    const modal = await this.modalCtrl.create({
      component: SearchPage
    });
    return await modal.present();
  }

  tryAgain() {
    this.getMarketItems();
  }

  searchByKeyword(event: any) {
  }

  segmentChanged(event) {
    // console.log(event.detail.value);
  }

  categoryChange(value) {
    this.currentType = value;
    this.currentPage = 1;
    this.market_items_buy = [];
    this.market_items_sell = [];
    this.getMarketItemsCategory(this.currentType);
  }

  async showMarketAdd() {
    if (this.global.userInfo) {
      const modal = await this.modalCtrl.create({
        component: MarketAddPage,
        componentProps: {
          'isAdd': true,
        }
      });

      modal.onDidDismiss().then(() => {
        this.currentPage = 1;
        this.market_items_buy = [];
        this.market_items_sell = [];
        if (this.currentType == 0)
          this.getMarketItemsUser();
        else
          this.getMarketItems();
      });

      return await modal.present();
    }
    else {
      this.showAlertLogin();
    }
  }

  async showAlertLogin() {

    const toast = await this.toastController.create({
      message: 'Đăng nhập để thêm sản phẩm?',
      duration: 2000,
      position: 'top',
      color: 'dark',
      buttons: [
        {
          text: 'Đăng nhập',
          handler: () => {
            this.showLogin();
          }
        }
      ]
    });
    toast.present();
  }

  async showLogin() {
    const modal = await this.modalCtrl.create({
      component: LoginPage,
    });

    modal.onDidDismiss().then(() => {
      if (this.global.isLogin) {
        this.getMarketItemsUser();
      }
    });
    return await modal.present();
  }

  handleImgError(ev: any, item : any){
    item.image = "../../assets/default-cart.png";
    console.log("----> handleImgError: " + ev + "  " + JSON.stringify(item));
  }
}
