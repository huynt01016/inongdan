import { Component } from '@angular/core';
import { ApiProvider } from 'src/providers/api/api';
import { NavController, ModalController } from '@ionic/angular';
import * as Enums from '../../../providers/appbase/appbase';
import { Router } from '@angular/router';
import { UltilityProvider } from 'src/providers/ultility/ultility';
import { Global } from '../../../providers/appbase/appbase';
import { SearchPage } from '../../search/search.page';
import { MarketDetailPage } from '../../market/market-detail/market-detail.page';

@Component({
  selector: 'app-market-category',
  templateUrl: './market-category.page.html',
  styleUrls: ['./market-category.page.scss'],
})
export class MarketCategoryPage {

  get_duration_interval: any;
  tab: string;
  offlineMode: number = 0;
  text_noconnection: string = "";
  market_items: Array<any>;
  market_types: any;
  reloads: Array<any>;
  maxLength: number = 50;
  myCategory = {
    market_type_id: 0,
    name: 'Nông sản của tôi'
  };
  currentPage: number = 1;
  constructor(public apiProvider: ApiProvider,
    public navCtrl: NavController,
    public ultility: UltilityProvider,
    public router: Router,
    public global: Global,
    public modalCtrl: ModalController, ) {

    this.tab = '1';
    this.market_items = [];
    this.getMarketType();
    this.getMarketItems();
  }

  ionViewWillEnter() {
    this.get_duration_interval = setInterval(() => { 
      this.getMarketItems(undefined, true);
    }, 5000);
  }
  ionViewWillLeave(){
    clearInterval(this.get_duration_interval);
  }

  getMarketItems(infiniteScroll: any = undefined, autoReload: boolean = false) {
    var pageNum = this.currentPage;
    var count = 10;
    if(autoReload){
      this.currentPage--;
      if(this.currentPage <= 0)
        this.currentPage = 1;
      pageNum = 1;
      count = this.currentPage * 10;
      console.log("market-category -> autoReload at: " + pageNum + " count: " + count)
    }


    this.apiProvider.getMarketItems(pageNum, count)
      .then(data => {
        if (!data || !data.market_items || data.market_items.length <= 0) {
          console.log('getMarketItems -> data is empty');
          if (this.currentPage == 1) {
            this.text_noconnection = "CHƯA CÓ BÀI VIẾT";
            this.offlineMode = Enums.OfflineMode.NO_DATA;
          }
        }
        else {
          this.offlineMode = Enums.OfflineMode.NORMAL;
          if(autoReload){
            this.reloadData(data.answers, count);
          }
          else{
            for (let index = 0; index < data.market_items.length; index++) {
              var r = this.convertMarketItemData(data.market_items[index]);
              if(r.status == 1)
                this.market_items.push(r);
            }
          }
          this.currentPage++;
        }
      }).catch(err => {
        console.log(err);
        if (this.market_items.length <= 0 && this.tab == '1') {
          this.text_noconnection = "KHÔNG TẢI ĐƯỢC DỮ LIỆU";
          this.offlineMode = Enums.OfflineMode.NO_INTERNET;
        }
      });
      if (infiniteScroll != undefined)
        infiniteScroll.complete();
  }
  reloadData(items, count){
    var updated = false;
    this.reloads = [];
    if(items == null)
      return;
    for (let index = 0; index < items.length; index++) {
      var r = this.convertMarketItemData(items[index]);
      if(r.status == 1){
        if(this.reloads.length == 0){
          if(this.market_items.length == 0)
            updated = true;
          else{
            var oldFirst = this.market_items[0];
            if(oldFirst.ans_id != r.ans_id){
              updated =true;
            }
            else
              break;
          }
        }
        this.reloads.push(items[index]);
      }
    }
    if(updated && this.reloads.length >= count)
      this.market_items = this.reloads;
  }
  convertMarketItemData(market_item){
    var r = market_item;
    r.date = this.ultility.convertToDate(market_item.news_date || market_item.created, true);
    r.title = market_item.news_title || market_item.title;;
    r.title_short = this.ultility.cropString(market_item.title, " ", this.maxLength, this.maxLength);

    if (r.type == 0)
      r.color = "green";
    else
      r.color = "red";

    return r;
  }


  getMarketType() {
    this.apiProvider.getMarketType()
      .then(data => {
        this.market_types = data.market_types;
        this.market_types.forEach(element => {
          element.market_category_image = element.market_category_image || '../../../assets/logotext.png';
        });
      }).catch(err => {

      });
  }

  showCategoryPost(m) {
    if (m.market_type_id == 0 && !this.global.isLogin) {
      return;
    }
    this.global.dataSender.category = {
      id: m.market_type_id,
      name: m.name
    };
    this.router.navigateByUrl(`tabs/market/category`);
  }

  tryAgain() {
    this.getMarketItems();
  }

  async showSearch() {
    const modal = await this.modalCtrl.create({
      component: SearchPage,
      componentProps: {
        recentPost: this.market_items.length > 7 ? this.market_items.slice(0, 7) : this.market_items,
        category: 2,
      }
    });
    return await modal.present();
  }

  async showPostDetail(marketid: number) {
    var data: any;
    var all = this.market_items;
    for (let i = 0; i < all.length; i++) {
      if (all[i].market_id == marketid) {
        data = all[i];
        break;
      }
    }

    if (data != undefined) {
      const modal = await this.modalCtrl.create({
        component: MarketDetailPage,
        componentProps: {
          'data': data,
        }
      });
      modal.onDidDismiss().then((detail) => {
        if (detail !== null && detail.data != null && detail.data.isReload) {
          this.currentPage = 1;
          this.market_items = [];
          this.getMarketItems();
        }
      });
      return await modal.present();
    }
  }

  doInfinite(event: any) {
    this.getMarketItems(event.target);
  }
}
