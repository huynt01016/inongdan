import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketAddPage } from './market-add.page';

describe('MarketAddPage', () => {
  let component: MarketAddPage;
  let fixture: ComponentFixture<MarketAddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarketAddPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketAddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
