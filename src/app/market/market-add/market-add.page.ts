import { Component } from '@angular/core';
import { ModalController, NavParams, ActionSheetController, PopoverController, Platform, LoadingController } from '@ionic/angular';
import { ApiProvider } from '../../../providers/api/api';
import { Global } from '../../../providers/appbase/appbase';
import { File } from '@ionic-native/file';

import { UltilityProvider } from '../../../providers/ultility/ultility';
import { DropdownlistPage } from '../../dropdownlist/dropdownlist.page';
import { async } from '@angular/core/testing';
import { PopupSelecterPage } from 'src/app/popup-selecter/popup-selecter.page';

@Component({
  selector: 'app-market-add',
  templateUrl: './market-add.page.html',
  styleUrls: ['./market-add.page.scss'],
})
export class MarketAddPage {
  imageUri: any = [];
  defaultImage: any = null;
  defaultVideo: any = null;
  market_types: Array<any>;
  provinces: Array<any>;

  item: any = {};
  isEdit: boolean = true;
  fileToUpload: any = null;
  constructor(
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public apiProvider: ApiProvider,
    public popoverCtrl: PopoverController,
    public ultility: UltilityProvider,
    public actionSheetController: ActionSheetController,
    public global: Global,
    private platform: Platform,
    public loadingCtrl: LoadingController) {
    this.getMarketType();
    this.getProvince();
    this.imageUri = [];
    this.defaultImage = null;
    this.defaultVideo = null;
    this.fileToUpload = null;

    let data = this.navParams.get('data') || {};
    this.isEdit = this.navParams.get('isEdit') || false;

    this.item = {
      full_name: data.full_name || 'Ẩn danh',
      market_id: data.market_id || '',
      title: data.title || '',
      contact: data.contact || this.global.userInfo.phone || '',
      price: data.price || '',
      content: data.content || '',
      province_id: data.province_id || 361,
      province_name: data.province_name || "Toàn Quốc",
      typeSelected: data.type || 0,
      market_type_id: data.market_type_id || 0,
      market_type_name: data.market_type_name || '',
      date: data.date || '',
      status: data.status || '',
    };
    if (data.price == 'undefined')
      this.item.price = '';
    if (data.image && !data.image.includes("../../assets")) {
      if (data.image.includes(".mp4"))
        this.defaultVideo = data.image;
      else if (data.image.includes(".png") || data.image.includes(".jpg")){
        this.defaultImage = data.image; 
        if(this.isEdit){
          this.imageUri.push(data.image);
        }
      }
       
    }
  }

  close() {
    this.item.type = this.item.typeSelected || 0;
    this.item.province_name = this.item.province_name || "Toàn Quốc";
    this.modalCtrl.dismiss({
      'dismissed': true,
      'newItem': this.item,
      'isReload': this.isEdit
    });
  }

  changeType(t) {
    this.item.typeSelected = t;
  }

  getMarketType() {
    this.apiProvider.getMarketType()
      .then(data => {
        // console.log(JSON.stringify(data));
        this.market_types = data.market_types;
      }).catch(err => {

      });
  }

  getProvince() {
    this.apiProvider.getProvince()
      .then(data => {
        // console.log(JSON.stringify(data));
        this.provinces = data.provinces;
      }).catch(err => {

      });
  }

  async onSelectMarketType() {
      var allItems: any = [];
    
      for(var i = 0; i < this.market_types.length; i++){
        var r = this.market_types[i];
        var v = {
          id: r.market_type_id,
          name: this.ultility.capitalizeFirstLetter(r.name),
        };
        allItems.push(v);
      }
  
      const rowModal = await this.modalCtrl.create({
        component: PopupSelecterPage,
        id: 'popup-selecter',
        cssClass: 'popup-selecter',
        componentProps: { 
          placeholder: 'Nhập loại',
          allItems:allItems,
        }
      });
      await rowModal.present();
      var selected = await rowModal.onDidDismiss();
      if(selected && selected.data && selected.data.selectedItem != null){
        this.item.market_type_id = selected.data.selectedItem.id;
        this.item.market_type_name = selected.data.selectedItem.name;
      }
  }

  async onSelectProvince(item) {
      var allItems: any = [];
      var selectedItem = null;
    
      for(var i = 0; i < this.provinces.length; i++){
        var r = this.provinces[i];
        var v = {
          id: r.province_id,
          name: this.ultility.capitalizeFirstLetter(r.name),
          selected: r.province_id == item.province_id
        };
        if(v.selected)
          selectedItem = v;
        allItems.push(v);
      }
  
      const rowModal = await this.modalCtrl.create({
        component: PopupSelecterPage,
        id: 'popup-selecter',
        cssClass: 'popup-selecter',
        componentProps: { 
          placeholder: 'Nhập địa điểm',
          allItems:allItems,
          selectedItem:selectedItem
        }
      });
      await rowModal.present();
      var selected = await rowModal.onDidDismiss();
      if(selected && selected.data && selected.data.selectedItem != null){
        this.item.province_id = selected.data.selectedItem.id;
        this.item.province_name = selected.data.selectedItem.name;
      }
  }

  async showGetImg(isShow) {
    if (isShow == true) {
      const actionSheet = await this.actionSheetController.create({
        buttons: [{
          text: 'Camera',
          role: 'destructive',
          icon: 'camera',
          handler: () => {
            this.captureAndUpload(1);
          }
        }, {
          text: 'Thư viện',
          icon: 'image',
          handler: () => {
            this.captureAndUpload(0);
          }
        }, {
          text: 'Huỷ',
          icon: 'close',
          role: 'cancel',
          handler: () => {
          }
        }]
      });
      await actionSheet.present();
    }
  }

  clear() {
    setTimeout(() => {
      this.fileToUpload = null;
      this.defaultImage = null;
      this.defaultVideo = null;
      this.imageUri.pop();
    }, 100);
  }

  async captureAndUpload(sourceType: number) {
    let me = this;

    let loading = this.loadingCtrl.create({
      message: 'Đang đọc dữ liệu. Vui lòng đợi...'
    });
    (await loading).present();
    await this.ultility.captureAndUploadBase64(sourceType,
      async (imageData) => {     
        me.defaultImage = imageData;
        me.imageUri.push(imageData.replace('data:image/jpeg;base64,', ''));
        (await loading).dismiss();
      },
      async (err) => {
        this.ultility.presentNoti({
          title:'Thông báo hệ thống',
          content:"Đọc dữ liệu thất bại. Vui lòng thử lại."
        });
        console.log("Lấy ảnh thất bại. Vui lòng thử lại." + err);
        console.log(JSON.stringify(err));
        (await loading).dismiss();

      });
  }

  
  addMarket() {
    if (this.item.market_type_id == "") {
      this.ultility.presentNoti({
        title:'Thông báo hệ thống',
        content:"Vui lòng chọn 'Loại nông sản'."
      });
      return;
    }
    if (this.item.province_id == "") {
      this.ultility.presentNoti({
        title:'Thông báo hệ thống',
        content:"Vui lòng chọn 'Địa chỉ'."
      });
      return;
    }
    if (this.item.title == "") {
      this.ultility.presentNoti({
        title:'Thông báo hệ thống',
        content:"Vui lòng chọn 'Tiêu đề'."
      });
      return;
    }
    if (this.item.content == "") {
      this.ultility.presentNoti({
        title:'Thông báo hệ thống',
        content:"Vui lòng chọn 'Mô tả'."
      });
      return;
    }
    if (this.item.price == "" && this.item.typeSelected == 1) {
      this.ultility.presentNoti({
        title:'Thông báo hệ thống',
        content:"Vui lòng chọn 'Giá cả'."
      });
      return;
    }
    if (this.item.contact == "") {
      this.ultility.presentNoti({
        title:'Thông báo hệ thống',
        content:"Vui lòng chọn 'Liên hệ'."
      });
      return;
    }
    if (!this.isEdit) {
      if (this.imageUri.length <= 0) {
        if(this.item.typeSelected == 1)
        {
          this.ultility.presentNoti({
            title:'Thông báo hệ thống',
            content:"Vui lòng chọn 'Ảnh sản phẩm'."
          });
          return;
        }
        else{
          this.imageUri.push("null");
        }
      }

      this.apiProvider.addMarketItems(
        this.global.userInfo.user_id,
        this.item.market_type_id,
        this.item.province_id,
        this.item.title, this.item.content,
        this.item.typeSelected,
        this.item.price, this.global.userInfo.phone,
        this.item.contact, "img.png", this.imageUri[0]).then(data => {
          if (data.status = "ok") {
            this.ultility.presentNoti({
              title:'Thông báo hệ thống',
              content:"Thêm sản phẩm thành công"
            });
            this.close();
          }
          else {
            this.ultility.presentNoti({
              title:'Thông báo hệ thống',
              content:"Thêm sản phẩm thất bại. Vui lòng thử lại."
            });
            console.log('addMarketItems: ' + data.message);
          }

        }).catch(err => {
          this.ultility.presentNoti({
            title:'Thông báo hệ thống',
            content:"Thêm sản phẩm thất bại. Vui lòng thử lại."
          });
          console.log('addMarketItems: ' + err);
        });
    }
    else {
      if (this.imageUri.length <= 0 && this.item.typeSelected == 1) {
        this.ultility.presentNoti({
          title:'Thông báo hệ thống',
          content:"Vui lòng chọn 'Ảnh sản phẩm'."
        });
          return;
      }
      if (this.item.market_id == '') {
        this.ultility.presentNoti({
          title:'Thông báo hệ thống',
          content:"Máy chủ gặp sự cố. Vui lòng thử lại."
        });
        this.close();
        return;
      }
      
      var image = "img.png";
      var imgBase64 = "";
      if(this.imageUri.length > 0 && !this.imageUri[0].includes('.png') && !this.imageUri[0].includes('http')){
        imgBase64 = this.imageUri[0];
        this.item.image = 'data:image/jpeg;base64,' + imgBase64;
      }
      
      this.apiProvider.editMarketItems(
        this.item.market_type_id,
        this.item.province_id,
        this.item.title, this.item.content,
        this.item.typeSelected,
        this.item.price, this.global.userInfo.phone, this.item.contact,
        this.item.market_id,
        image,
        imgBase64).then(data => {
          if (data.status = "ok") {
            this.ultility.presentNoti({
              title:'Thông báo hệ thống',
              content:"Sửa sản phẩm thành công."
            });
          }
          else {
            this.ultility.presentNoti({
              title:'Thông báo hệ thống',
              content:"Sửa sản phẩm thất bại. Vui lòng thử lại."
            });
            console.log('addMarketItems: ' + data.message);
          }
          this.close();

        }).catch(err => {
          this.ultility.presentNoti({
            title:'Thông báo hệ thống',
            content:"Sửa sản phẩm thất bại. Vui lòng thử lại."
          });
          console.log('addMarketItems: ' + err);
          this.close();
        });
    }
  }

}
