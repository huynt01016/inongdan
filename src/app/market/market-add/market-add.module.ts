import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import {NgxMaskIonicModule} from 'ngx-mask-ionic'

import { IonicModule } from '@ionic/angular';

import { MarketAddPage } from './market-add.page';

const routes: Routes = [
  {
    path: '',
    component: MarketAddPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxMaskIonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MarketAddPage]
})
export class MarketAddPageModule {}
