import { Component } from '@angular/core';
import { ModalController, NavParams, AlertController } from '@ionic/angular';
import { Global } from '../../../providers/appbase/appbase';
import { MarketAddPage } from '../../market/market-add/market-add.page';
import { ApiProvider } from 'src/providers/api/api';
import { UltilityProvider } from 'src/providers/ultility/ultility';
import { Storage } from '@ionic/storage';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';


@Component({
  selector: 'app-market-detail',
  templateUrl: './market-detail.page.html',
  styleUrls: ['./market-detail.page.scss'],
})
export class MarketDetailPage {
  isMe: boolean;
  isReload: boolean;
  data = this.navParams.get('data');
  constructor(
    public global: Global,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public alertController: AlertController,
    public apiProvider: ApiProvider,
    public storage: Storage,
    public ultility: UltilityProvider,
    private photoViewer: PhotoViewer) {

    console.log(JSON.stringify(this.data));
    if (this.global.isLogin && this.data.creator == this.global.userInfo.phone)
      this.isMe = true;
    this.isReload = false;

    var priceN = Number(this.data.price);
    if(Number.isNaN(priceN)){
      this.data.priceN = this.data.price || 'Liên hệ';
    }
    else{
      this.data.priceN = this.ultility.numberWithDot(priceN) + ' đ/kg';
    }
  }

  close() {
    this.modalCtrl.dismiss({
      'dismissed': true,
      'isReload': this.isReload
    });
  }

  async editMarketItem() {
    const modal = await this.modalCtrl.create({
      component: MarketAddPage,
      componentProps: {
        'isEdit': true,
        'data': this.data
      }
    });

    let me = this;
    modal.onDidDismiss().then((detail) => {
      if (detail !== null && detail.data.isReload) {
        console.log(JSON.stringify(me.data));
        console.log(JSON.stringify(detail.data.newItem));

        me.isReload = true

        if(detail.data.newItem){
          me.data.title = detail.data.newItem.title;
          me.data.content = detail.data.newItem.content;
          me.data.contact = detail.data.newItem.contact;
          me.data.full_name = detail.data.newItem.full_name;
          me.data.market_type_id = detail.data.newItem.market_type_id;
          me.data.market_type_name = detail.data.newItem.market_type_name;
          me.data.province_id = detail.data.newItem.province_id;
          me.data.province_name = detail.data.newItem.province_name;
          me.data.price = detail.data.newItem.price;
          me.data.date = detail.data.newItem.date;
          if(detail.data.newItem.image)
            me.data.image = detail.data.newItem.image;


          var priceN = Number(me.data.price);
          if(Number.isNaN(priceN)){
            me.data.priceN = me.data.price || 'Liên hệ';
          }
          else{
            me.data.priceN = me.ultility.numberWithDot(priceN) + ' đ/kg';
          }
        }
        // me.close();
      }
    });

    return await modal.present();
  }

  async showAlertDelete() {
    const alert = await this.alertController.create({
      header: 'Thông báo!',
      message: 'Bạn có muốn xóa sản phẩm này?',
      buttons: [
        {
          text: 'Thoát',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Đồng ý',
          handler: () => {
            this.showAlertPass();
          }
        }
      ]
    });

    await alert.present();
  }

  async showAlertPass() {
    const alert = await this.alertController.create({
      header: 'Thông báo!',
      message: 'Nhập mật khẩu để xác nhận?',
      inputs: [
        {
          name: 'password',
          placeholder: 'Mật khẩu'
        }
      ],
      buttons: [
        {
          text: 'Thoát',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Đồng ý',
          handler: (data) => {
            this.storage.get('credentials').then((val) => {
              if (data.password == val.password) {
                this.deleteMarketItem();
              }
              else {
                this.ultility.presentToast("Sai mật khẩu! Vui lòng thử lại", "e");
              }

            });
          }
        }
      ]
    });

    await alert.present();
  }

  deleteMarketItem() {
    this.apiProvider.deleteMarketItems(this.data.market_id)
      .then(data => {
        // console.log(JSON.stringify(data));
        if (data.status == "ok") {
          this.ultility.presentNoti({
            title:'Thông báo hệ thống',
            content:"Sửa sản phẩm thành công."
          });
          this.isReload = true;
        }
        else {
          this.ultility.presentNoti({
            title:'Thông báo hệ thống',
            content:"Xóa sản phẩm thất bại. Vui lòng thử lại."
          });
        }
        this.close();
      }).catch(err => {
        this.ultility.presentNoti({
          title:'Thông báo hệ thống',
          content:"Xóa sản phẩm thất bại. Vui lòng thử lại."
        });
        this.close();
      });
  }

  photoClick(url, title){
    if(this.data.image.includes("../../assets"))
      console.log(url);
    else
      this.photoViewer.show(url, title);
  }
}
