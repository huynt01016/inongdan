import { Component } from '@angular/core';
import { ModalController, ActionSheetController, AlertController, ToastController } from '@ionic/angular';
import { ApiProvider } from '../../providers/api/api';
import { Global } from '../../providers/appbase/appbase';
import { NavParams } from '@ionic/angular';
import { UltilityProvider } from '../../providers/ultility/ultility';
import * as Enums from '../../providers/appbase/appbase';

import { LoginPage } from '../personal/login/login.page';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.page.html',
  styleUrls: ['./comment.page.scss'],
})
export class CommentPage {
  commentSucess = false;
  haveComment = false;
  offlineMode: number = 0;
  text_noconnection: string = "";
  nid = this.navParams.get('nid');
  title = this.navParams.get('title');
  comment: any;
  comments: Array<{ comment: string, avatar_url: string, user_name: string, created: string, date: string, user: number, isMe: boolean }>;
  constructor(
    public modalCtrl: ModalController,
    public apiProvider: ApiProvider,
    public navParams: NavParams,
    public global: Global,
    public ultility: UltilityProvider,
    public actionSheetController: ActionSheetController,
    public alertController: AlertController,
    public toastController: ToastController) {
    this.getComment();
  }


  close() {
    this.modalCtrl.dismiss({
      'dismissed': true,
      'commentSucess': this.commentSucess
    });
  }


  getComment() {
    console.log("getComment " + this.nid);
    this.apiProvider.getComment(this.nid)
      .then(data => {
        if (!data || !data.comments || data.comments.length <= 0) {
          this.text_noconnection = "CHƯA CÓ BÌNH LUẬN";
          this.offlineMode = Enums.OfflineMode.NO_DATA;
        }
        // console.log(JSON.stringify(data))
        this.offlineMode = Enums.OfflineMode.NORMAL;
        this.comments = data.comments;
        for (let i = 0; i < this.comments.length; i++) {
          this.comments[i].avatar_url = "https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1";
          this.comments[i].date = this.ultility.convertToDate(this.comments[i].created);

          if (this.global.userInfo && this.comments[i].user == this.global.userInfo.user_id)
            this.comments[i].isMe = true;
          else
            this.comments[i].isMe = false;
        }
        if (this.comments.length >= 1)
          this.haveComment = true;
      }).catch(err => {
        this.text_noconnection = "KHÔNG TẢI ĐƯỢC DỮ LIỆU";
        this.offlineMode = Enums.OfflineMode.NO_INTERNET;
      });
  }

  sendComment() {
    if (this.global.userInfo && this.global.userInfo.user_id) {
      if (this.comment == undefined || this.comment.trim().length <= 0)
        this.ultility.presentNoti({
          title:'Thông báo hệ thống',
          content:'Vui lòng nhập bình luận'
        });
      else {
        this.apiProvider.addComment(this.nid, this.global.userInfo.user_id, this.comment)
          .then(data => {
            // console.log(JSON.stringify(data));
            if (data.status == "ok") {
              this.ultility.presentNoti({
                title:'Thông báo hệ thống',
                content:'Bình luận thành công'
              });
              this.comment = "";
              this.getComment();
              this.commentSucess = true;
            }
            else {
              this.ultility.presentNoti({
                title:'Thông báo hệ thống',
                content:'Bình luận thất bại'
              });
            }
          }).catch(err => {

          });
      }
    }
    else {
      this.showAlertLogin();
    }

  }

  deleteComment(comid: string) {
    this.apiProvider.deleteComment(comid)
      .then(data => {
        if (data.status == "ok") {
          this.ultility.presentNoti({
            title:'Thông báo hệ thống',
            content:'Xoá bình luận thành công.'
          });
          this.getComment();
          this.commentSucess = true;
        }
        else {
          this.ultility.presentNoti({
            title:'Thông báo hệ thống',
            content:'Xoá bình luận thất bại.'
          });
        }
      }).catch(err => {

      });
  }

  editComment(comid: string, message: string) {
    if (message == undefined || message.trim().length <= 0){
      this.ultility.presentNoti({
        title:'Thông báo hệ thống',
        content:'Vui lòng nhập bình luận.'
      });
    }
    else {
      this.apiProvider.editComment(comid, message)
        .then(data => {
          if (data.status == "ok") {
            this.ultility.presentNoti({
              title:'Thông báo hệ thống',
              content:'Sửa bình luận thành công.'
            });
            this.getComment();
          }
          else {
            this.ultility.presentNoti({
              title:'Thông báo hệ thống',
              content:'Sửa bình luận thất bại.'
            });
          }
        }).catch(err => {

        });
    }
  }

  async showMore(comid: string, message: string) {

    const actionSheet = await this.actionSheetController.create({
      buttons: [{
        text: 'Xoá',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.showAlertDelete(comid);
        }
      }, {
        text: 'Sửa',
        icon: 'create',
        handler: () => {
          this.showAlertEdit(comid, message);
        }
      }, {
        text: 'Huỷ',
        icon: 'close',
        role: 'cancel',
        handler: () => {
        }
      }]
    });
    await actionSheet.present();
  }

  async showAlertDelete(comid: string) {
    const alert = await this.alertController.create({
      header: 'Thông báo!',
      message: 'Bạn có muốn xoá bình luận này?',
      buttons: [
        {
          text: 'Thoát',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Đồng ý',
          handler: () => {
            this.deleteComment(comid);
          }
        }
      ]
    });

    await alert.present();
  }

  async showAlertEdit(comid: string, message: string) {
    const alert = await this.alertController.create({
      header: 'Sửa bình luận',
      inputs: [
        {
          name: 'message',
          placeholder: 'Nhập bình luận',
          value: message,
        },
      ],
      buttons: [
        {
          text: 'Thoát',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Đồng ý',
          handler: (data) => {
            this.editComment(comid, data.message);
          }
        }
      ]
    });

    await alert.present();
  }

  async showAlertLogin() {

    const toast = await this.toastController.create({
      message: 'Đăng nhập để bình luận?',
      duration: 2000,
      position: 'bottom',
      cssClass: 'mihn-toast-wb',
      buttons: [
        {
          text: 'Đăng nhập',
          handler: () => {
            this.showLogin();
          }
        }
      ]
    });
    toast.present();
  }

  async showLogin() {
    const modal = await this.modalCtrl.create({
      component: LoginPage,
    });
    return await modal.present();
  }

  tryAgain() {
    this.getComment();
  }
}
