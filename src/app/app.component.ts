import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { UltilityProvider } from '../providers/ultility/ultility';

import { FCM } from '@ionic-native/fcm/ngx';
import { Global } from 'src/providers/appbase/appbase';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public ultility: UltilityProvider,
    public fcm: FCM,
    public global: Global,
    public ulti: UltilityProvider
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      setTimeout(() => {
        this.statusBar.styleLightContent();
        this.splashScreen.hide();
      }, 1000);

      this.fcm.getToken().then(token=>{
        console.log("========> token: " + token);
        this.global.fireBase_token = token;
      }).catch(err=>{
        console.log("========> token failed: " + JSON.stringify(err));
      });
      this.fcm.onTokenRefresh().subscribe(token=>{
        console.log("========> token refresh: " + token);
        this.global.fireBase_token = token;
      });
      this.fcm.onNotification().subscribe(data=>{
        if(!data.wasTapped){
          this.ulti.presentNoti({
            title: data.title,
            content: data.body
          });
        }
        else{
          console.log("========> onNotification: " + JSON.stringify(data));
        }
      });
    });
  }
}
