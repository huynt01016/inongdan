import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ApiProvider } from '../../providers/api/api';
import { Url } from 'url';
import { Global } from '../../providers/appbase/appbase';
import { Storage } from '@ionic/storage';

import { ModalController } from '@ionic/angular';
import { DetailPage } from '../detail/detail.page';
import { SearchPage } from '../search/search.page';
import { NotificationsPage } from '../notifications/notifications.page';
import { UltilityProvider } from '../../providers/ultility/ultility';
import * as Enums from '../../providers/appbase/appbase';
import { LoadingController } from '@ionic/angular';
import { HomeMenuPage } from '../home-menu/home-menu.page';
import { async } from '@angular/core/testing';
import { container } from '@angular/core/src/render3';
import * as moment from 'moment';

@Component({
  selector: 'app-tab1',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  
  commentSucess = false;
  offlineMode: number = 0;
  text_noconnection: string = "";
  get_duration_interval: any;
  currentPage: number = 1;
  postPerPage: number = 10;
  firstPost: any;
  posts: Array<{ cat_id: number; category_name: string; nid: number; news_title: string; news_date: string; date: string; news_image: Url; news_description: string }>;
  reloadPosts: Array<{ cat_id: number; category_name: string; nid: number; news_title: string; news_date: string; date: string; news_image: Url; news_description: string }>;
  categories: any = [];
  cid: number = -1;
  cname: string = '';
  isFirstGetData: boolean = false;
  constructor(
    public modalController: ModalController,
    public apiProvider: ApiProvider,
    public navCtrl: NavController,
    public global: Global,
    public ultility: UltilityProvider,
    public loadingCtrl: LoadingController,
    private storage: Storage) {
    this.firstPost = {
      news_title: "",
      news_image: "https://wallpapercave.com/wp/Hvtxo6m.jpg",
    };

    this.autoLoginCheck();
  }
  ionViewWillEnter(){
    this.getCategoryIndex();
    this.cid = -1;
    this.cname ='';
    this.currentPage = 1;
    this.posts = [];
    
    this.tryAgain();
    this.get_duration_interval = setInterval(()=>{
      if(!this.isFirstGetData)
        return;
      if(this.cid == -1)
        this.getRecentPostReload();
      else
        this.getCategoryPostReload();
    }, 5000);
  }
  ionViewWillLeave(){
    clearInterval(this.get_duration_interval);
  }

  getCategoryIndex(){
    this.apiProvider.getCategoryIndex()
      .then((data)=>{
        if(!data || !data.categories || data.categories.length <= 0){
        }
        else
        {
          this.categories = data.categories;
        }
        
       }).catch(err=>{
          this.text_noconnection = "KHÔNG TẢI ĐƯỢC DỮ LIỆU";
          this.offlineMode = Enums.OfflineMode.NO_INTERNET;
       });
  }
  async getCategoryPostReload(){
    var pageNum = 1;
    var count = this.currentPage * this.postPerPage;;
    this.apiProvider.getCategoryPost(this.cid + '', pageNum, count)
      .then(async data => {
          this.reloadData(data.posts, count);
      }).catch(async err => {
      });
  }
  async getCategoryPost(infiniteScroll: any = undefined){
    let loading = null;
    loading = this.loadingCtrl.create({
      message: 'Đang đọc dữ liệu. Vui lòng đợi...'
    });
    (await loading).present();
  
    if(infiniteScroll != undefined)
      this.currentPage++;

    var pageNum = this.currentPage;
    var count = this.postPerPage;
    let me = this;
    this.apiProvider.getCategoryPost(this.cid + '', pageNum, count)
      .then(async data => {
          
        if (!data || !data.posts || data.posts.length <= 0) {
          console.log('getCategoryPost -> data is empty');
          me.currentPage--;
          if (this.currentPage == 1) {
            this.text_noconnection = "CHƯA CÓ BÀI VIẾT";
            this.offlineMode = Enums.OfflineMode.NO_DATA;
          }
        }
        else {
          this.isFirstGetData = true;
          this.fillData(data.posts);
        }
        
        setTimeout(async () => {     
          if(loading)
            (await loading).dismiss();
        }, 100);
      }).catch(async err => {
        setTimeout(async () => {     
          if(loading)
            (await loading).dismiss();
        }, 100);
        if (this.posts.length <= 0) {
          this.text_noconnection = "KHÔNG TẢI ĐƯỢC DỮ LIỆU";
          this.offlineMode = Enums.OfflineMode.NO_INTERNET;
        }
      });

      if (infiniteScroll != undefined){
        infiniteScroll.complete();
      }
  }

  async getRecentPostReload(){
    var pageNum = 1;
    var count = this.currentPage * this.postPerPage;;
    this.apiProvider.getRecentPost(pageNum, count)
      .then(async data => {
          this.reloadData(data.posts, count);
      }).catch(async err => {
      });
  }
  async getRecentPost(infiniteScroll: any = undefined) {
    // console.log("GET RECENTPOST");
    let loading = null;
    loading = this.loadingCtrl.create({
      message: 'Đang đọc dữ liệu. Vui lòng đợi...'
    });
    (await loading).present();
  
    if(infiniteScroll != undefined)
      this.currentPage++;
    let me = this;
    this.apiProvider.getRecentPost(this.currentPage, this.postPerPage)
      .then(async data => {
        
        if (!data || !data.posts || data.posts.length <= 0) {
          me.currentPage --;
          console.log('getRecentPost -> data is empty');
          if (this.currentPage == 1) {
            this.text_noconnection = "CHƯA CÓ BÀI VIẾT";
            this.offlineMode = Enums.OfflineMode.NO_DATA;
          }
        }
        else {
          this.isFirstGetData = true;
          this.fillData(data.posts);
        }
        
        setTimeout(async () => {     
          if(loading)
            (await loading).dismiss();
        }, 100);
      }).catch(async err => {
        setTimeout(async () => {     
          if(loading)
            (await loading).dismiss();
        }, 100);
        if (this.posts.length <= 0) {
          this.text_noconnection = "KHÔNG TẢI ĐƯỢC DỮ LIỆU";
          this.offlineMode = Enums.OfflineMode.NO_INTERNET;
        }
        
      });

      if (infiniteScroll != undefined){
        infiniteScroll.complete();
      }
  }

  autoLoginCheck() {
    if (this.global.isLogin == true)
      return;

    this.storage.get('credentials').then((val) => {
      // console.log('Your credentials is', val);
      if (!val || !val.username || !val.password)
        return;
      this.apiProvider.login(val.username, val.password).then(data => {

        if (data.status == "nok") {
          this.storage.remove('credentials');
          if (data.message.includes("user") && data.message.includes("password")) {
            this.ultility.presentNoti({
              title:'Đăng nhập thất bại', 
              content:"'Tên đăng nhập' hoặc 'Mật khẩu' không đúng."
            });
          }
          else if (data.message.includes("User") && data.message.includes("exists")) {
            this.ultility.presentNoti({
              title:'Đăng nhập thất bại', 
              content:"'Tài khoản' không tồn tại."
            });
          }
          else{
            this.ultility.presentNoti({
              title:'Đăng nhập thất bại', 
              content:"Có lỗi xảy ra. (" + data.message + ")"
            });
          }
        }
        else if (data.status == "ok") {
          console.log("userinfo: " + JSON.stringify(data.user_info));
          this.global.isLogin = true;
          this.global.userInfo = data.user_info[0];
          this.ultility.presentNoti({
            title:'Đăng nhập thành công', 
            content:`Chào mừng bạn '${this.global.userInfo.full_name}'`
          });

          if (this.global.userInfo.avatar && this.global.userInfo.avatar.length > 1)
            this.global.avatar = this.global.avalink + this.global.userInfo.avatar;
        }

      }).catch(err => {
        this.ultility.presentNoti({
          title:'Đăng nhập thất bại', 
          content:"Có lỗi xảy ra."
        });
      });
    });
  }

  doInfinite(event: any) {
    console.log("doinfinite: " + event.target);
    if(this.cid == -1)
      this.getRecentPost(event.target);
    else
      this.getCategoryPost(event.target);
  }

  async showPostDetail(nid: number) {
    const modal = await this.modalController.create({
      component: DetailPage,
      componentProps: {
        'nid': nid,
      }
    });

    modal.onDidDismiss().then((detail) => {
      if (detail !== null && detail.data && detail.data.commentSucess !== undefined) {
        this.commentSucess = detail.data.commentSucess
        if (this.commentSucess) {
          this.currentPage = 1;
          this.posts = [];
          this.tryAgain();
        }
      }
    });
    return await modal.present();
  }

  async showSearch() {
    const modal = await this.modalController.create({
      component: SearchPage,
      componentProps: {
        recentPost: this.posts.length > 7 ? this.posts.slice(0, 7) : this.posts,
        category: 1,
      }
    });
    await modal.present();

  }

  backToHome(){
    this.cid = -1;  
    this.cname = '';  
    this.tryAgain();
  }
  async showNotification() {
    const modal = await this.modalController.create({
      component: NotificationsPage
    });
    return await modal.present();
  }
  async showMenu(){
    const modal = await this.modalController.create({
      component: HomeMenuPage,
      id: 'home-menu',
      componentProps: {
        categories: this.categories,
      }
    });
    await modal.present();
    
    var d = await modal.onDidDismiss();
    if(d && d.data && d.data.selected){ 
      if(this.cid != d.data.selected.cid){
        this.cid = d.data.selected.cid;  
        this.cname = d.data.selected.category_name;  
        this.tryAgain();
      }
    }
  }

  tryAgain() {
    if(this.firstPost == null){
      this.firstPost = {
        news_title: "",
        news_image: "https://wallpapercave.com/wp/Hvtxo6m.jpg",
      };
    }
    this.isFirstGetData = false;
    this.currentPage = 1;
    this.posts = [];

    console.log("tryagain: " + this.currentPage);
    if(this.cid == -1)
      this.getRecentPost();
    else
      this.getCategoryPost();
  }

  reloadData(posts, count){
    var updated = false;
    this.reloadPosts =[];
    for (let index = 0; index < posts.length; index++) {
      posts[index].date = this.ultility.convertToDate(posts[index].news_date);
      posts[index].title = this.ultility.cropString(posts[index].news_title, " ", 90, 95);
      posts[index].isVideo = posts[index].content_type === "VIDEO";
      if(index == 0){
        if(posts[index].nid != this.firstPost.nid)
          updated = true;
        else
          break;
      }
      this.reloadPosts.push(posts[index]);
    }
    if(updated && this.reloadPosts.length >= count){
      console.log("home reload: " + posts.length + "/" + count);
      this.firstPost = this.reloadPosts.shift();
      this.firstPost.date = this.ultility.convertToDate(this.firstPost.news_date, true);   
      this.firstPost.short_desc = this.getShortDesc(this.firstPost.news_description);
      this.posts = this.reloadPosts;
    }
  }
  fillData(posts){
    if(posts != null && posts.length > 0){
      console.log(`current page: ${this.currentPage}`)
      this.offlineMode = Enums.OfflineMode.NORMAL;
      if (this.currentPage == 1) {
        this.firstPost = posts[0];
        this.firstPost.date = this.ultility.convertToDate(this.firstPost.news_date, true);
        this.firstPost.title = this.firstPost.news_title;
        this.firstPost.short_desc = this.getShortDesc(this.firstPost.news_description);
    
        this.posts = [];
        for (let index = 1; index < posts.length; index++) {
          posts[index].date = this.ultility.convertToDate(posts[index].news_date);
          posts[index].title = this.ultility.cropString(posts[index].news_title, " ", 90, 95);
          posts[index].isVideo = posts[index].content_type === "VIDEO";

          this.posts.push(posts[index]);
        }
      }
      else {
        for (let index = 0; index < posts.length; index++) {
          const contains = this.posts.filter(p => p.nid == posts[index].nid);
          if(contains.length > 0){
            console.log("trùng: " + contains[0].nid)
            continue;
          }
          if(posts[index].nid == this.firstPost.nid)
            continue;

          posts[index].date = this.ultility.convertToDate(posts[index].news_date);
          posts[index].title = this.ultility.cropString(posts[index].news_title, " ", 90, 95);
          posts[index].isVideo = posts[index].content_type === "VIDEO";
          this.posts.push(posts[index]);
        }
      }
      if(this.posts.length > 0){
        this.posts.sort(function(a, b){
          var da = moment(a.date, 'DD/MM/YYYY HH:mm:ss').valueOf();
          var db = moment(b.date, 'DD/MM/YYYY HH:mm:ss').valueOf();
          return db-da;
        });
      }
    }
  }
  getShortDesc(description){
    var des: string = description.replace(/<[^>]*>?/gm, '').replace(/[&]nbsp[;]/gi, " ");
    var posdot = des.indexOf('.');
    var posbreak = des.indexOf('\n');
    var c = des.substring(posdot + 1, des.length - 1);
    if (c >= '0' && c <= '9') {
      posdot = c.indexOf('.');
    }
    var pos = Math.min(posdot, posbreak, 250);
    return unescape(des.substring(0, pos)) + ".";
  }

}
